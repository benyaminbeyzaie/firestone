# Fire Stone
AP Project Readme file. I'm not a native English speaker be ready for some possible language flaws :)


## Resource and library 
- **Java Version: 13.0.1** 

_Make sure that you build project correctly based on gradle_
- **library and modules**
    - jackson-annotations-2.10.3
    - jackson-core-2.10.3
    - jackson-databind-2.10.3
    - commons-io-2.6
    - stax-1.0.1
    - hibernate-core,-5.4.14.Final
    - sqlite-jdbc-3.32.3.2
- **Image and sounds**
    - I upload all Images and sounds [phase02 resources](https://drive.google.com/file/d/1lN20uwKL323aYYdQC1bKl3ldEJirADL8/view?usp=sharing),
    [phase03 resources](https://drive.google.com/file/d/1tXxM5u7abDHZA-OuhGMF1P97PbVW5qBa/view?usp=sharing). phase03 resourses also works fo phase04. so you can download it and replace it with resource folder! It would works prefectlly fine!
- **Resources**
    - [Hashing password](https://dzone.com/articles/storing-passwords-java-web)
    - [Jackon guide](http://tutorials.jenkov.com/java-json/jackson-objectmapper.html)
    - [Write a markdown file](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
    - [Work and build with gradle](https://www.lynda.com/Developer-tutorials/Gradle-Java-Developers/585247-2.html)
    - [Markdown help in vscode](https://code.visualstudio.com/docs/languages/markdown)
    - [Work with git](https://faradars.org/courses/fvgit9609-managed-distributed-edition-using-git)
    - [How to avoid java.util.ConcurrentModificationException when iterating through and removing elements from an ArrayList](https://stackoverflow.com/questions/8104692/how-to-avoid-java-util-concurrentmodificationexception-when-iterating-through-an)
    - [Playing background sound in java](https://stackoverflow.com/questions/13573281/how-to-play-a-background-music-when-the-program-run-in-java)
    - [Musics](https://www.blizzard.com/en-us/games/music/hearthstone.html)
    - [Images](https://hearthstone.gamepedia.com/)
    - [How to avoid java.util.ConcurrentModificationException when iterating through and removing elements from an ArrayList](https://stackoverflow.com/questions/8104692/how-to-avoid-java-util-concurrentmodificationexception-when-iterating-through-an)
    - [State design pattern](https://en.wikipedia.org/wiki/State_pattern)
    - [Beginner 2D game development in java](https://www.youtube.com/watch?v=dEKs-3GhVKQ&list=PLah6faXAgguMnTBs3JnEJY0shAc18XYQZ&index=1) helps me a lot :))
    - [Multi-thread programming](https://javacup.ir/javacup-training-videos/)
    - I use some files that my fried Erfan Mousavian provided! Thanks to him. :))
    - I have update some hard-coded setting in project based on XML file reading. I write a little article for that purpose. You can read it from my virgool in [this link](https://virgool.io/javacup/%D8%A7%D8%B3%D8%AA%D9%81%D8%A7%D8%AF%D9%87-%D8%A7%D8%B2-xml-%D8%AF%D8%B1-%D8%AC%D8%A7%D9%88%D8%A7-r4jfshjoo5eo) and find some good resources for learning usage of XML in java!
    - Thanks to SAM Sadat to give me some advice about adding database to my project. I may use some code from his project on github [hearthstone](https://github.com/samssh/Hearthstone)
    - Dr. Ostovari mentioned a course [hibernate](www.p30download.com)
    

    

    

    
## Solved one big problem in phase1! ;)
- **All of methods were void** because of cli based app that I developed in phase one, I could just print the result but in graphical user interface we must be able to track the method results some how! I tried returning int and enum some times. for example sign in methods returns 5 diffrent values in diffrent situations. usage is simple as the saving that value in temp in. like that: 
``` 
int temp = signIn.signIn(user, pass);
        if (temp == 0){
            // user is empty
            // showing warning massage
            warning.setMessage("Username is empty!");
            warning.setImage(Assets.warning);
            warning.setBackgroundColor(Constants.WARNING_COLOR);
            warning.setShow(true);
        }
```
## Khown disadvantage of code and issues
- **I don't have any animations** of course my project has timer and it is not so hard to add some animations to it but I don't have time to work on that!
- **I don't have class for card and heros** It can be advantage and disadvantage in same time. My project is based on Minion and Spell class and it takes time to change it to divided classes! also I have some useful methods that maybe useless if I have make class fo all cards. like ``makeMinionFromJson()``.
- **Big play class** I may break some SOLID rules in this class. :(
- **Don't have quest and reward**
- **files address don't works on unix base OS**
- some features may have bugs I don't test all of the logic!

## How it works? 
#### Running
``` 
        Game game = Game.getInstance();
        game.run();
```

This project has simply a Application class in src folder and when you run this class, it creates new Game class (which is the main class) and call the run method from that class. Game class in view package is a completely divided class wich is created to handle the graphical user interface and it has no connection to the game logic and other data models. (of course some the logical things are handled in view package. I explained the reasons in short video that I uploaded)
#### Cards
A card is neutral or class card. in practice a card is a spell or minion so we must have these two classes. Of course some of the fields and method are common so we need a card super class which is ``Card.java``

You may notice that I don't have any weapon class because weapon works like minion class in next phase I may create divided class for that too but in this phase it works perfectly fine!
#### CommandLineInterface
Command line interface is totally gone! I just commented out the all functionality!
#### Heroes
like Cards I read fields from a Json file and convert it to an object. As far as each hero may have different fields I create a method called ``createHero()`` which takes a string for name of the hero that we want to create; Heros in the game are **completely floated**. if game producer wants to add a new hero to the game it is enough to add hero's Json file and also hero's class cards in cardjson package. Program reads all Json files through a _for on files_ in an specific directory. but for showing hero's image developer must change the ``getHeroImage`` method in ``Assets.java``.

#### Log
My first idea to track a player activity was use a library to log all actions but I realize that i don't need all actions in log file so i just create two simple class which extends ``Logger`` and ``Formatter`` in java.util package.

I had a challenge for writing log in the way that doc says for having header and body. for adding DELETE line to the header i used a simple trick... When user wants to delete account I read all lines of log file using ``LineIterator`` class in org.apache.commons.io package then add DELETE line to the Strings that I read from log file then create a new log file with that additional line and replace it via this methods: 
```
FileUtils.deleteQuietly(logFile);
FileUtils.moveFile(tempFile, logFile);
``` 
each log file has a unique name! player id for each player is a unique String which is crated based on the time of signing up I use this DateFormat for create unique id:
```
final DateFormat dff = new SimpleDateFormat("yyMMddhhmmss");
```
#### Player
each player that signs up calls ``Player()`` constructor which sets default player info for example which heros are open at the first or which one is selected! also which cards are open and so on...

**One of the best feature** in this code is a method that saves all actions that player dose in the game we must update player's Json file on each action so I create a method called ``saveSignedPlayerInfo()`` that method converts player object to the Json file and replace it! so after each action that must have effect on the Json file I called that method!
```
//buying a card
shop.buy(signedPlayer, cardName);
signedPlayer.saveSignedPlayerInfo();
```


#### Security
Security class is a simple class which has just two ``static`` method for hashing password for create a really strong hash I add a string called SALT so if some one knows how to get the password from hash code SALT prevents this action.

I don's have any method for unhash the password each time player wants to sign in I convert entered password to hash code then check it from Json file via this method on Player class:
```
public boolean checkPassword(String pass) {
    return Security.hashPassword(pass).equals(this.getPassword());
    }
```
#### Shop
Designed so simple and works completely fine!

#### Play and half  of game logic
The ``Play.java`` class handle the logic part of the game state. it is simple, ``Play.java`` constructor takes two player and randomly gives them turn one or two. every time a player press the end turn button some functions are called and change turn and set gems and all sort of things.
```
    public int endTurn() {
        if (currentPlayer.getInfoPassive() != null){
            changeTurn();
            increaseOneMaxMana(currentPlayer);
            increaseCurrentMana(currentPlayer);
            return 0; // success
        }else {
            return 1; // select an info passive
        }
    }
```
Info passive is handled by just a simple enum class I may change it later but it works pretty well in this phase. at the start of the game player must choose an among three random passives that are shown on the screen. player not allowed to do anything before select one. passive logic part is not complete. just works with some if/else things. randoms are generated based on shuffle method on Collections.

#### Logic 
Actions works as an object! every logical action on play field like summon a card or attack to hero is an ``Action.java`` object.
every logical action has ``act()`` methods which works perfectly fine and handles every possible situations.

**battlecry and spell actions** are a little different. I use a ``boolean`` in every minion to khow that if this  minion has battlecry or not. if battlecry action works on minion then player must select enemy or friendly minion to call the ``battlecry(card)``. turn base battlecrys and spells handele on some other methods in play and every time player presses end turn button this methods runs. I have two different int named ``battlecryActionCallerID`` and ``spellActionCallerID`` which specify which card is played and action must be acted. 

All logical things of the game are implied taut Color is ``Color.BLUE`` and shield color is ``Color.YELLOW``.

Turns are random and you can change your cards by clicking on them before select info passive.

I would like to mention that the difference among playingDeck and currentDeck. when you play with a deck you may lose a card or your hero may lose blood. if this changes saved at your next game your card are not the same and your hero may be damaged. so when you enter play state I get a copy of your current deck and name it playingDeck so this problem would not occurred.

#### Timer

Works like main game timer but on other thread! game time starts when player prees the end turn button and monitor the time on the other Thread. Timer has access to the ``handler`` of the game and when time of player is come automatically calls `` handler.getGame().getPlayState().endThings();``. 

#### Deck Reader
A strong json file reader! DeckReder.java is s singleton class. I have three modes for play class.
- 0 is for play with selected deck.
- 1 for play with deck reader.
- 2 for play with computer. (It is not available).
```
            // deck reader mode
            this.playerTwo = handler.getGame().getPlayState().getImaginaryPlayer();
            DeckReader deckReader = DeckReader.newInstance();
            deckReader.readFrom(Constants.DECK_FILE_ADDRESS, playerOne, playerTwo);
            currentPlayer = playerOne;
            opponentPlayer = playerTwo;
```

``DeckReader.java`` class has two methods. ``readFrom()`` read the Deck.json file and create DeckFromJson object which has name of cards in json. ``makeDeckCards()`` create actual cards from strings.


## graphics

#### State design pattern! 
I use a design pattern called "state design pattern" for main idea of my graphics! one small different: I use abstract main class State and these abstract method instead of interface. This design pattern make my works pretty easy and clean! I mentioned it again on view part!
```
    public abstract void tick();
    public abstract void render(Graphics2D g);
```
Swing utils and objects could be used of course! but I prefer to write complete original graphical utils by my self. I called them BObject! so everything that extends BObject is a graphical util thing that can be rendered on the screen! I use swing just for create new frame. all graphical things are drawn by Graphics2D. BufferStrategy and Canvas are used for showing things on frame (which is a JFrame).

#### Load resources in simple and fast way!

because of loading an image 60 times per second is so hard for CPU, I prefer to load all images and sounds resource just one time and call them when ever I needed them! to handle resources I create Assets package with ``Assets`` and ``ImageLoader`` classes.
 Every variable in ``Assets.java`` class is public and static. ``ImageLoade.java`` class is a simple class just for loading images!

 All resource files are in the resource folder of project.

#### BObject, useful graphical util abstract class

I thought by myself that every graphical object must have render and tick method and every Object must have an method for tracking the mouse and keyboard commands. 
```
    public abstract void tick();
    public abstract void render(Graphics2D g);
    public abstract void onClick();
    void onMouseMove(MouseEvent e){
        hovering = bounds.contains(e.getX(), e.getY());
    }
    void onMouseRelease(MouseEvent e){
        if(hovering)
            onClick();
        else if (this instanceof BTextBox){
            ((BTextBox) this).setUnderType(false);
        }
    }
    void onKeyTyped(KeyEvent e){
        if (this instanceof BTextBox){
            ((BTextBox) this).type(e);
        }
    }
```
Every class that extends BObject must receive an interface which is called ClickListener. ClickListener have ``onClick()`` method so abstract ``onClick()`` on BObject just have to call that method on each graphic util object. Like in the BButton:
```
    @Override
    public void onClick() {
        // I play sound when a button is clicked!
        Game.playSound(Assets.click);
        clickListener.onClick();
    }
```

#### Accessing whole game anywhere with handler!
for access different part of game from any states I created ``handler.java`` class and this class takes Game object as far as game is a singleton class there is no way to change different object values rather than what we want! ``handler.java`` is clean and simple. passing one handler over the whole classes is much better than pass the game object itself!

for example if I wanted to access the signed player over the MainState I could just call that:
```
Player signedPlayer = handler.getGame().getSignedPlayer(); //this player is the one that signed in the game and saved in the game
```
or accessing CollectionState for setting up the pages of the cards from the MainState is simple as that:
```
    private void callCollectionsState() {
        handler.getGame().getCollectionsState().setCurrentDeckPage(null);
        handler.getGame().getCollectionsState().setSelectedDeck(null);
        try {
            handler.getGame().getCollectionsState().setUpPages(signedPlayer, -1, PageType.COLLECTIONS);
            handler.getGame().getCollectionsState().setUpDecks(handler.getGame().getSignedPlayer());
        } catch (IOException e) {
            e.printStackTrace();
        }
        StateManager.setCurrentState(handler.getGame().getCollectionsState());
    }
``` 

#### Scanning user commands
do to scan mouse and keyboard commands I create two classes one for mouse which is called ``MouseManger.java`` and one for keyboard ``KeyBoardManager.java`` I make instance of both in game object and set them to the different ``BObjecctManajer.java`` so each time one manager can scan the commands!

``BObjectManger.java`` takes an ArrayList of BObjects. with that manager I could control all objects simply with one for loop over that list! Like for rendering and ticking: 
```
    public void tick(){
        for(BObject o : objects)
            o.tick();
    }
    public void render(Graphics2D g){
        for (BObject object : objects) {
            object.render(g);
        }
    }
```


#### View
Views (render) and tick are based on game loop and frame.

for rendering different states (switching,...) and pages and also object I use one simple trick that. I have ``StateManager.java`` class which keeps just one simple ``State.java`` class, currentState. In Game.java main rendering method I just call this state's rendering method and that's it! when ever I want to change view or switch among states I just have to set the currentState to a new state and my main rendering method automatically calls the currentState ``render(Graphics2D)`` method: (part of render method)
```
    private void render(){
        //Clear Screen
        graphics2D.clearRect(0, 0, display.getFrame().getWidth(), display.getFrame().getHeight());

        //Draw Here!
        if(StateManager.getCurrentState() != null)
            StateManager.getCurrentState().render(graphics2D);

        // Cursorr Icon
        graphics2D.drawImage(Assets.mouseIcon, MouseManager.mouseX - 7, MouseManager.mouseY - 10, 40,40, null);
    }
```

## Database (ORM makes it easy!)

in phase 04 I added database to my project. I use hibernate and SQLite to manage data, table and mapping java objects to the tables. I use hibernate annotations to map the objects to the tables. tables created automaticially through hibernate intelligance! SAM Sadat helps me a lot with woriking on database.

SQLite is easy embeded database wich is pretty enough for us to manage all data in a game like hearthstone. Dr. Ostovari introduce a complete course about database with hibernate and it was so helpful I create ``HibernateUtil.java`` class based on this course.

creating players and getting them is a diffrent process also adding cards and hero to their table was preety handy but rest of the work was easy because I had a poweful function which is called ``saveSignedPlayerInfo()`` and I just edit this fuction based on ORM:
```
public synchronized void saveSignedPlayerInfo() {
        Session session = 
        HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.update(this);
        session.getTransaction().commit();
        session.close();
        }
 ``` 

