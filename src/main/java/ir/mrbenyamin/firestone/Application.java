package ir.mrbenyamin.firestone;

import ir.mrbenyamin.firestone.cli.CLI;
import ir.mrbenyamin.firestone.setting.Setting;
import ir.mrbenyamin.firestone.setting.SettingLoader;
import ir.mrbenyamin.firestone.view.Game;

import javax.xml.stream.XMLStreamException;
import java.io.IOException;

public class Application {
    public static void main(String[] args) throws IOException {
//        CLI cli = CLI.getInstance();
//        cli.run();
        Game game = null;
        try {
            game = Game.getInstance();
        } catch (XMLStreamException e) {
            e.printStackTrace();
        }
        assert game != null;
        game.run();
    }
}
