package ir.mrbenyamin.firestone.setting;

import java.awt.*;

public class Setting {
    private String TITLE;
    private int STANDARD_STATES_WIDTH;
    private int STANDARD_STATES_HEIGHT;
    private int PLAY_WIDTH;
    private int PLAY_HEIGHT;
    private String DECK_FILE_ADDRESS;

    private Color BUTTON_COLOR_1;
    private Color BUTTON_COLOR_2;
    private Color BUTTON_COLOR_3;
    private Color BUTTON_COLOR_4;
    private Color WARNING_COLOR;
    private Color WARNING_FONT_COLOR;
    private Color LABEL_WHITE;
    private Color HEADER_TEXT;

    private Font CARD_TEXT_FONT;
    private Font COLLECTIONS_CARD_TEXT_FONT;


    void setColor(String name, Color color){
        switch (name){
            case "Constants.BUTTON_COLOR_1": BUTTON_COLOR_1 = color;
                break;
            case "BUTTON_COLOR_2": BUTTON_COLOR_2 = color;
                break;
            case "BUTTON_COLOR_3": BUTTON_COLOR_3 = color;
                break;
            case "BUTTON_COLOR_4": BUTTON_COLOR_4 = color;
                break;
            case "WARNING_COLOR": WARNING_COLOR = color;
                break;
            case "WARNING_FONT_COLOR": WARNING_FONT_COLOR = color;
                break;
            case "LABEL_WHITE": LABEL_WHITE = color;
                break;
            case "HEADER_TEXT": HEADER_TEXT = color;
                break;
        }
    }

    void setFont(String name, String font, int type, int size){
        switch (name){
            case "CARD_TEXT_FONT" : CARD_TEXT_FONT = new Font(font,type ,size);
            case "COLLECTIONS_CARD_TEXT_FONT" : COLLECTIONS_CARD_TEXT_FONT = new Font(font,type ,size);
        }
    }

    public String getTITLE() {
        return TITLE;
    }

    public void setTITLE(String TITLE) {
        this.TITLE = TITLE;
    }

    public int getSTANDARD_STATES_WIDTH() {
        return STANDARD_STATES_WIDTH;
    }

    public void setSTANDARD_STATES_WIDTH(int STANDARD_STATES_WIDTH) {
        this.STANDARD_STATES_WIDTH = STANDARD_STATES_WIDTH;
    }

    public int getSTANDARD_STATES_HEIGHT() {
        return STANDARD_STATES_HEIGHT;
    }

    public void setSTANDARD_STATES_HEIGHT(int STANDARD_STATES_HEIGHT) {
        this.STANDARD_STATES_HEIGHT = STANDARD_STATES_HEIGHT;
    }

    public int getPLAY_WIDTH() {
        return PLAY_WIDTH;
    }

    public void setPLAY_WIDTH(int PLAY_WIDTH) {
        this.PLAY_WIDTH = PLAY_WIDTH;
    }

    public int getPLAY_HEIGHT() {
        return PLAY_HEIGHT;
    }

    public void setPLAY_HEIGHT(int PLAY_HEIGHT) {
        this.PLAY_HEIGHT = PLAY_HEIGHT;
    }

    public Color getBUTTON_COLOR_1() {
        return BUTTON_COLOR_1;
    }

    public void setBUTTON_COLOR_1(Color BUTTON_COLOR_1) {
        this.BUTTON_COLOR_1 = BUTTON_COLOR_1;
    }

    public Color getBUTTON_COLOR_2() {
        return BUTTON_COLOR_2;
    }

    public void setBUTTON_COLOR_2(Color BUTTON_COLOR_2) {
        this.BUTTON_COLOR_2 = BUTTON_COLOR_2;
    }

    public Color getBUTTON_COLOR_3() {
        return BUTTON_COLOR_3;
    }

    public void setBUTTON_COLOR_3(Color BUTTON_COLOR_3) {
        this.BUTTON_COLOR_3 = BUTTON_COLOR_3;
    }

    public Color getBUTTON_COLOR_4() {
        return BUTTON_COLOR_4;
    }

    public void setBUTTON_COLOR_4(Color BUTTON_COLOR_4) {
        this.BUTTON_COLOR_4 = BUTTON_COLOR_4;
    }

    public Color getWARNING_COLOR() {
        return WARNING_COLOR;
    }

    public void setWARNING_COLOR(Color WARNING_COLOR) {
        this.WARNING_COLOR = WARNING_COLOR;
    }

    public Color getWARNING_FONT_COLOR() {
        return WARNING_FONT_COLOR;
    }

    public void setWARNING_FONT_COLOR(Color WARNING_FONT_COLOR) {
        this.WARNING_FONT_COLOR = WARNING_FONT_COLOR;
    }

    public Color getLABEL_WHITE() {
        return LABEL_WHITE;
    }

    public void setLABEL_WHITE(Color LABEL_WHITE) {
        this.LABEL_WHITE = LABEL_WHITE;
    }

    public Color getHEADER_TEXT() {
        return HEADER_TEXT;
    }

    public void setHEADER_TEXT(Color HEADER_TEXT) {
        this.HEADER_TEXT = HEADER_TEXT;
    }

    public Font getCARD_TEXT_FONT() {
        return CARD_TEXT_FONT;
    }

    public void setCARD_TEXT_FONT(Font CARD_TEXT_FONTS) {
        this.CARD_TEXT_FONT = CARD_TEXT_FONTS;
    }

    public Font getCOLLECTIONS_CARD_TEXT_FONT() {
        return COLLECTIONS_CARD_TEXT_FONT;
    }

    public void setCOLLECTIONS_CARD_TEXT_FONT(Font COLLECTIONS_CARD_TEXT_FONT) {
        this.COLLECTIONS_CARD_TEXT_FONT = COLLECTIONS_CARD_TEXT_FONT;
    }

    public String getDECK_FILE_ADDRESS() {
        return DECK_FILE_ADDRESS;
    }

    public void setDECK_FILE_ADDRESS(String DECK_FILE_ADDRESS) {
        this.DECK_FILE_ADDRESS = DECK_FILE_ADDRESS;
    }

    @Override
    public String toString() {
        return "Setting{" +
                "TITLE='" + TITLE + '\'' +
                ", STANDARD_STATES_WIDTH=" + STANDARD_STATES_WIDTH +
                ", STANDARD_STATES_HEIGHT=" + STANDARD_STATES_HEIGHT +
                ", PLAY_WIDTH=" + PLAY_WIDTH +
                ", PLAY_HEIGHT=" + PLAY_HEIGHT +
                ", Constants.BUTTON_COLOR_1=" + BUTTON_COLOR_1 +
                ", BUTTON_COLOR_2=" + BUTTON_COLOR_2 +
                ", BUTTON_COLOR_3=" + BUTTON_COLOR_3 +
                ", BUTTON_COLOR_4=" + BUTTON_COLOR_4 +
                ", WARNING_COLOR=" + WARNING_COLOR +
                ", WARNING_FONT_COLOR=" + WARNING_FONT_COLOR +
                ", LABEL_WHITE=" + LABEL_WHITE +
                ", HEADER_TEXT=" + HEADER_TEXT +
                '}';
    }
}
