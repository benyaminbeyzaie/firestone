package ir.mrbenyamin.firestone.card;

import com.fasterxml.jackson.databind.ObjectMapper;
import ir.mrbenyamin.firestone.view.Constants;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.io.File;
import java.io.IOException;

@Entity
public class Minion extends Card {
    @Column
    private int health;
    @Column
    private int attack;
    @Column
    private String subType;
    @Column
    private boolean canAttack;
    @Column
    private int timeOnField;
    @Column
    private int attackInOnce;
    @Column
    private boolean taut;
    @Column
    private boolean hasBattlecry;
    @Column
    private boolean rush;
    @Column
    private boolean charge;
    @Column
    private boolean shield;
    @Column
    private boolean windfury;
    @Column
    private boolean lifesteal;
    @Column
    private boolean poisonous;

    public static Minion createAMinionFromJson(String name) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        Minion minion =  objectMapper.readValue(new File(Constants.CARD_PATH + name + ".json"), Minion.class);
        return minion;
    }

    public boolean isTaut() {
        return taut;
    }

    public void setTaut(boolean taut) {
        this.taut = taut;
    }

    public int getAttackInOnce() {
        return attackInOnce;
    }

    public void setAttackInOnce(int attackInOnce) {
        this.attackInOnce = attackInOnce;
    }

    public int getTimeOnField() {
        return timeOnField;
    }

    public void setTimeOnField(int timeOnField) {
        this.timeOnField = timeOnField;
    }

    public boolean canAttack() {
        return canAttack;
    }

    public void setCanAttack(boolean canAttack) {
        this.canAttack = canAttack;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getAttack() {
        return attack;
    }

    public void setAttack(int attack) {
        this.attack = attack;
    }

    public String getSubType() {
        return subType;
    }

    public void setSubType(String subType) {
        this.subType = subType;
    }

    public boolean hasBattlecry() {
        return hasBattlecry;
    }

    public void setHasBattlecry(boolean hasBattlecry) {
        this.hasBattlecry = hasBattlecry;
    }

    public boolean isCanAttack() {
        return canAttack;
    }

    public boolean isHasBattlecry() {
        return hasBattlecry;
    }

    public boolean isRush() {
        return rush;
    }

    public void setRush(boolean rush) {
        this.rush = rush;
    }

    public boolean isCharge() {
        return charge;
    }

    public void setCharge(boolean charge) {
        this.charge = charge;
    }

    public boolean isShield() {
        return shield;
    }

    public void setShield(boolean shield) {
        this.shield = shield;
    }

    public boolean isWindfury() {
        return windfury;
    }

    public void setWindfury(boolean windfury) {
        this.windfury = windfury;
    }

    public boolean isLifesteal() {
        return lifesteal;
    }

    public void setLifesteal(boolean lifesteal) {
        this.lifesteal = lifesteal;
    }

    public boolean isPoisonous() {
        return poisonous;
    }

    public void setPoisonous(boolean poisonous) {
        this.poisonous = poisonous;
    }
}

