package ir.mrbenyamin.firestone.card;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import ir.mrbenyamin.firestone.view.Constants;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.io.File;
import java.io.IOException;

@Entity
public class Weapon extends Card {
    @Column
    private int durability;
    @Column
    private int attack;
    @Column
    private boolean canAttack;
    @Column
    private int timeOnField;
    @Column
    private int attackInOnce;
    @Column
    private boolean hasBattlecry;

    public static Weapon createWeaponWithName(String name) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        File file = new File(Constants.CARD_PATH + name + ".json");
        return objectMapper.readValue(file, Weapon.class);
    }

    public int getDurability() {
        return durability;
    }

    public void setDurability(int durability) {
        this.durability = durability;
    }

    public int getAttack() {
        return attack;
    }

    public void setAttack(int attack) {
        this.attack = attack;
    }

    public boolean isCanAttack() {
        return canAttack;
    }

    public void setCanAttack(boolean canAttack) {
        this.canAttack = canAttack;
    }

    public int getTimeOnField() {
        return timeOnField;
    }

    public void setTimeOnField(int timeOnField) {
        this.timeOnField = timeOnField;
    }

    public int getAttackInOnce() {
        return attackInOnce;
    }

    public void setAttackInOnce(int attackInOnce) {
        this.attackInOnce = attackInOnce;
    }

    public boolean isHasBattlecry() {
        return hasBattlecry;
    }

    public void setHasBattlecry(boolean hasBattlecry) {
        this.hasBattlecry = hasBattlecry;
    }
}
