package ir.mrbenyamin.firestone.card;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import ir.mrbenyamin.firestone.view.Constants;
import ir.mrbenyamin.firestone.view.assets.Assets;
import ir.mrbenyamin.firestone.view.assets.ImageLoader;

import javax.persistence.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Arrays;

@Entity
public class Card {
    @Id
    private String name;
    @Column
    private int manaCost;
    @Column
    private String type;
    @Column
    private String heroClass;
    @Column
    private String rarity;
    @Column
    private String description;
    @Column
    private int price;
    @Column
    private String cardImageAddress;
    @Column
    private boolean isSummon = false;
    transient private static final int CARD_WIDTH_ON_FIELD = 75;
    transient private static final int CARD_HEIGHT_ON_FIELD = 106;
    transient public static final String CARDS_PATH = "src\\main\\java\\ir\\mrbenyamin\\firestone\\card\\cardsjsonfiles";

    // getter and setters


    public String getCardImageAddress() {
        return cardImageAddress;
    }

    public void setCardImageAddress(String cardImageAddress) {
        this.cardImageAddress = cardImageAddress;
    }

    public boolean isSummon() {
        return isSummon;
    }

    public void setSummon(boolean summon) {
        isSummon = summon;
    }

    public static int getCardWidthOnField() {
        return CARD_WIDTH_ON_FIELD;
    }

    public static int getCardHeightOnField() {
        return CARD_HEIGHT_ON_FIELD;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getManaCost() {
        return manaCost;
    }

    public void setManaCost(int manaCost) {
        this.manaCost = manaCost;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getHeroClass() {
        return heroClass;
    }

    public void setHeroClass(String heroClass) {
        this.heroClass = heroClass;
    }

    public String getRarity() {
        return rarity;
    }

    public void setRarity(String rarity) {
        this.rarity = rarity;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void renderOnField(Graphics g, int x, int y){
        g.drawImage(Assets.card, x, y, CARD_WIDTH_ON_FIELD, CARD_HEIGHT_ON_FIELD, null);
    }

    public static Card createCardWithName(String name) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        File file = new File(Constants.CARD_PATH + name + ".json");
        return objectMapper.readValue(file, Card.class);
    }

    public static ArrayList<Card> getAllCards() throws IOException {
        final ArrayList<Card> allCards = new ArrayList<>();
        ArrayList<File> allCardsJsons = new ArrayList<>();
        File[] cardsFile = new File(CARDS_PATH).listFiles();
        allCardsJsons.addAll(Arrays.asList(cardsFile));

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        for (File file :
                allCardsJsons) {
            Card card = objectMapper.readValue(file, Card.class);
            if (card.getType().equals("Spell")){
                Spell spell = objectMapper.readValue(file, Spell.class);
                allCards.add(spell);
            }else if(card.getType().equals("Minion")){
                Minion minion = objectMapper.readValue(file, Minion.class);
                allCards.add(minion);
            }else if (card.getType().equals("Weapon")){
                Weapon weapon = objectMapper.readValue(file, Weapon.class);
                allCards.add(weapon);
            }
        }
        return allCards;
    }
    public static ArrayList<String> getNameOfAllCards() throws IOException {
        ArrayList<String> allCardsName = new ArrayList<>();
        ArrayList<Card> allCards = getAllCards();
        for (Card card :
                allCards) {
            allCardsName.add(card.getName());
        }
        return allCardsName;
    }

    public static Card getCardOfName(String name) throws IOException {
        final ArrayList<Card> allCards = getAllCards();
        for (Card card :
                allCards) {
            if (card.getName().equals(name)) {
                return card;
            }
        }
        return null;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Card) {
            return this.getName().equals(((Card) obj).getName());
        }else {
            return false;
        }
    }
}
