package ir.mrbenyamin.firestone.card;

import com.fasterxml.jackson.databind.ObjectMapper;
import ir.mrbenyamin.firestone.view.Constants;

import javax.persistence.Entity;
import java.io.File;
import java.io.IOException;

@Entity
public class Spell extends Card {
    public static Spell createSpellFromJson(String name) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        Spell spell =  objectMapper.readValue(new File(Constants.CARD_PATH + name + ".json"), Spell.class);
        return spell;
    }
}
