package ir.mrbenyamin.firestone.card;

import ir.mrbenyamin.firestone.card.Card;
import ir.mrbenyamin.firestone.heroes.Hero;
import ir.mrbenyamin.firestone.log.MyLogger;
import ir.mrbenyamin.firestone.player.Player;
import ir.mrbenyamin.firestone.view.Constants;
import ir.mrbenyamin.firestone.view.Handler;
import ir.mrbenyamin.firestone.view.state.states.MainState;
import org.hibernate.annotations.GeneratorType;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.io.IOException;
import java.util.*;
import java.util.logging.Level;

@Entity
public class Deck implements Comparable<Deck> {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @Column
    private String name;
    @Column
    private int winsCount;
    @Column
    private int allGamesCount;
    @Column
    private float winToAll;
    @Column
    private int sumOfMana;
    @Column
    private int countOfCards;
    @ManyToOne
    private Hero hero;

    @ManyToMany
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Card> cards;

    @ElementCollection
    @LazyCollection(LazyCollectionOption.FALSE)
    private Map<String , Integer> useCountsOfCards;
    @ManyToOne
    private Card mostUsedCard;

    public Deck(){

    }

    public Deck(String name, Hero hero, ArrayList<Card> cards) {
        this.hero = hero;
        this.cards = cards;
        this.name = name;
        useCountsOfCards =  new HashMap<>();
        calculate();
    }

    public void calculate(){

        winToAll = calculateWinToAll();
        sumOfMana = calculateSumOfMana();
        countOfCards = calculateCountOfCards();
        try {
            mostUsedCard = calculateMostUsedCard();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private int calculateSumOfMana(){
        int sum = 0;
        for (Card card:
             cards) {
            sum += card.getManaCost();
        }
        return sum;
    }

    private int calculateCountOfCards(){
        int sum = 0;
        for (Card card:
                cards) {
            sum += 1;
        }
        return sum;
    }

    private Card calculateMostUsedCard() throws IOException {
        int max = 0;
        int maxManaCost = 0;
        ArrayList<Card> cards =  new ArrayList<>();
        Card card;
        for(String key: useCountsOfCards.keySet()){
            int temp = useCountsOfCards.get(key);
            if (temp > max) max = temp;
        }
        // TODO fucking KAMYABI
        for(String key: useCountsOfCards.keySet()){
            if (useCountsOfCards.get(key) == max){
               cards.add(Card.getCardOfName(key));
            }
        }
        if (cards.size() > 1){
            // check the mana
            for (Card c :
                    cards) {
                if (c.getManaCost() > maxManaCost) maxManaCost = c.getManaCost();
            }
            // avoiding exception
            for (Iterator<Card> iterator = cards.iterator(); iterator.hasNext(); ) {
                Card value = iterator.next();
                if (value.getManaCost() < maxManaCost) {
                    iterator.remove();
                }
            }

            if (cards.size() > 1){
                boolean hasMinion = false;
                for (Card c :
                        cards) {
                    if (c.getType().equals("Minion")) {
                        hasMinion = true;
                        break;
                    }
                }
                if (hasMinion){
                    for (Card c :
                            cards) {
                        if (!c.getType().equals("Minion")) cards.remove(c);
                    }
                    if (cards.size() > 1){
                        // random
                        Random random = new Random();
                        int rand = random.nextInt(cards.size());
                        card = cards.get(rand);
                    }else{
                        card = cards.get(0);
                    }
                }else {
                    Random random = new Random();
                    int rand = random.nextInt(cards.size());
                    card = cards.get(rand);
                }
            }else {
                card = cards.get(0);
            }
        }else {
            if (cards.size() == 1){
                card = cards.get(0);
            }else {
                card = null;
            }
        }
        return card;
    }

    public Hero getHero() {
        return hero;
    }

    public void setHero(Hero hero) {

        this.hero = hero;
    }

    public List<Card> getCards() {
        return cards;
    }

    public void setCards(ArrayList<Card> cards) {
        this.cards = cards;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWinsCount() {
        return winsCount;
    }

    public void setWinsCount(int winsCount) {
        this.winsCount = winsCount;
    }

    public int getAllGamesCount() {
        return allGamesCount;
    }

    public void setAllGamesCount(int allGamesCount) {
        this.allGamesCount = allGamesCount;
    }

    public int getSumOfMana() {
        return sumOfMana;
    }

    public void setSumOfMana(int sumOfMana) {
        this.sumOfMana = sumOfMana;
    }

    public int getCountOfCards() {
        return countOfCards;
    }

    public void setCountOfCards(int countOfCards) {
        this.countOfCards = countOfCards;
    }

    public Map<String, Integer> getUseCountsOfCards() {
        return useCountsOfCards;
    }

    public void setUseCountsOfCards(Map<String, Integer> useCountsOfCards) {
        this.useCountsOfCards = useCountsOfCards;
    }

    public Card getMostUsedCard() {
        return mostUsedCard;
    }

    public void setMostUsedCard(Card mostUsedCard) {
        this.mostUsedCard = mostUsedCard;
    }

    private float calculateWinToAll() {
        if (allGamesCount == 0) return 0;
        return (float)winsCount / (float)allGamesCount;
    }

    public float getWinToAll() {
        return winToAll;
    }

    public void setWinToAll(float winToAll) {
        this.winToAll = winToAll;
    }

    @Override
    public int compareTo(Deck o) {
        if (this.winToAll > o.winToAll){
            return 1;
        }
        else if (this.winToAll < o.winToAll){
            return -1;
        }
        else {
            if (this.winsCount > o.winsCount) return 1;
            else if (this.winsCount < o.winsCount) return -1;
            else {
                if (this.allGamesCount > o.allGamesCount) return 1;
                else if (this.allGamesCount < o.allGamesCount) return -1;
                else  {
                    if (this.cards.size() == 0){
                        return -1;
                    }else {
                        if (this.getManaPerCard() < o.getManaPerCard()){
                            return 1;
                        }else if (this.getManaPerCard() > o.getManaPerCard()) return -1;
                        else {
                            Random random = new Random(2);
                            if (random.nextInt() == 1) return 1;
                            else return -1;
                        }
                    }
                }
            }
        }
    }

    public float getManaPerCard() {
        if (cards.size() == 0) return -1;
        else return (float) calculateSumOfMana() / (float) cards.size();
    }

    public void resetStatus(){
        winsCount = 0;
        allGamesCount = 0;
        winToAll = 0;
        useCountsOfCards = new HashMap<>();
        Card mostUsedCard = null;
        try {
            MyLogger.writeLog(MainState.LOG_NAME,  this.getName() + " status was removed", Level.INFO);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Deck){
            return ((Deck) obj).getName().equals(this.getName());
        }else {
            return false;
        }
    }

    @Override
    public String toString() {
        return "Deck{" +
                "name='" + name + '\'' +
                '}';
    }

}
