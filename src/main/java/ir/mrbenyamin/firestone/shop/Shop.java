package ir.mrbenyamin.firestone.shop;


import ir.mrbenyamin.firestone.card.Card;
import ir.mrbenyamin.firestone.card.Deck;
import ir.mrbenyamin.firestone.log.MyLogger;
import ir.mrbenyamin.firestone.player.Player;

import java.io.IOException;
import java.util.logging.Level;

public class Shop {
    public int sell(Player signedPlayer, String cardToSellName) throws IOException {
        Card cardToSell = Card.getCardOfName(cardToSellName);
        if (cardToSell != null){
            if (signedPlayer.getAllAvailableCards().contains(cardToSell)){
                boolean isInDeck = false;
                for (Deck deck:
                     signedPlayer.getDecks()) {
                    if (deck.getCards().contains(cardToSell))
                        isInDeck = true;
                }
                if (!isInDeck){
                    signedPlayer.getAllAvailableCards().remove(cardToSell);
                    signedPlayer.setGems(signedPlayer.getGems() + cardToSell.getPrice());
                    MyLogger.writeLog(signedPlayer.getUsername() + "-" + signedPlayer.getStringID(),  "player sold " + cardToSellName, Level.INFO);
                    return 3; // "You sold " + cardToSellName + " Successfully. Your gems has raised " + cardToSell.getPrice()
                }else {
                    isInDeck = false;
                    return 2; // You already have this card on your battle deck! Remove it first!
                }
            }else {
                return 1; // you don't have this
            }
        }else {
            return 0; // card is invalid
        }
    }

    public int buy(Player signedPlayer, String cardToBuyName) throws IOException {
        Card cardToBuy = Card.getCardOfName(cardToBuyName);
        if (cardToBuy == null){
            return 0; // Card is null
        }else {
            if (!signedPlayer.getAllAvailableCards().contains(cardToBuy)){
                if (signedPlayer.getGems() >= cardToBuy.getPrice()){
                    signedPlayer.setGems(signedPlayer.getGems() - cardToBuy.getPrice());
                    signedPlayer.getAllAvailableCards().add(cardToBuy);
                    MyLogger.writeLog(signedPlayer.getUsername() + "-" + signedPlayer.getStringID(),  "player buy " + cardToBuyName, Level.INFO);
                    return 3;
                }else{
                    return 2;
                }
            }else {
                return 1;
            }
        }
    }
}

