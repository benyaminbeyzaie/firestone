package ir.mrbenyamin.firestone.view.graphicutils;

import java.io.IOException;

public interface ClickListener {
    void onClick() throws IOException;
}
