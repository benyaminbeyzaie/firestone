package ir.mrbenyamin.firestone.view.graphicutils.cardview;


import ir.mrbenyamin.firestone.card.Card;
import ir.mrbenyamin.firestone.card.Minion;
import ir.mrbenyamin.firestone.card.Weapon;
import ir.mrbenyamin.firestone.player.Player;
import ir.mrbenyamin.firestone.view.Constants;
import ir.mrbenyamin.firestone.view.assets.Assets;
import ir.mrbenyamin.firestone.view.graphicutils.ClickListener;

import java.awt.*;
import java.io.IOException;

public class OnFieldWeaponView extends CardView {
    public OnFieldWeaponView(Weapon weapon, Player signedPlayer, float x, float y, int width, int height, ClickListener clickListener) {
        super(weapon, signedPlayer, x, y, width, height, clickListener);
    }

    @Override
    public void tick() {

    }

    @Override
    public void render(Graphics2D g) {
        if (Assets.getWeaponImage(getCard().getName()) != null){
            g.setColor(Color.WHITE);
            g.drawImage(Assets.getWeaponImage(getCard().getName())[0],(int) x, (int) y, width, height, null);
            g.setFont(Constants.CARDS_TEXT_FONT);
            g.drawString(String.valueOf(((Weapon) getCard()).getDurability()), x + width - 20 , y + height - 20);
            g.drawString(String.valueOf(((Weapon) getCard()).getAttack()), x + 15 , y + height - 20);
            // g.drawString(String.valueOf(getCard().getManaCost()), x+ 8, y + 18);
            g.setFont(new Font("Arial", Font.BOLD, 14));
        }
    }

    @Override
    public void onClick() throws IOException {
        getClickListener().onClick();
    }
}
