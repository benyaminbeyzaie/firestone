package ir.mrbenyamin.firestone.view.graphicutils.button;

import ir.mrbenyamin.firestone.view.Constants;
import ir.mrbenyamin.firestone.view.graphicutils.ClickListener;

import java.awt.*;

public class BTextButton extends BButton {
    private String buttonText;
    private Font buttonTextFont;
    private int buttonTextSize = 14;
    private Color buttonColor = Constants.BUTTON_COLOR_2;
    private Color buttonHoverColor = Constants.BUTTON_COLOR_3;
    private Color stringColor = Color.WHITE;

    public BTextButton(String buttonText, float x, float y, int width, int height, ClickListener clickListener) {
        super(x, y, width, height, clickListener);
        this.buttonText = buttonText;
    }

    @Override
    public void tick() {

    }

    @Override
    public void render(Graphics2D g) {
        if (hovering){
            g.setColor(buttonHoverColor);
        }else g.setColor(buttonColor);
        g.fillRect((int)x, (int)y, width, height);
        g.setColor(stringColor);
        buttonTextFont = new Font("Arial", Font.BOLD, buttonTextSize);
        g.setFont(buttonTextFont);
        FontMetrics fontMetrics = g.getFontMetrics(buttonTextFont);
        int width = fontMetrics.stringWidth(buttonText);
        g.drawString(buttonText, (int)x + this.width / 2 - width / 2, (int)y + height / 2 + buttonTextSize / 4);
    }

    // Getters and setters


    public String getButtonText() {
        return buttonText;
    }

    public void setButtonText(String buttonText) {
        this.buttonText = buttonText;
    }

    public Font getButtonTextFont() {
        return buttonTextFont;
    }

    public void setButtonTextFont(Font buttonTextFont) {
        this.buttonTextFont = buttonTextFont;
    }

    public int getButtonTextSize() {
        return buttonTextSize;
    }

    public void setButtonTextSize(int buttonTextSize) {
        this.buttonTextSize = buttonTextSize;
    }

    public Color getButtonColor() {
        return buttonColor;
    }

    public void setButtonColor(Color buttonColor) {
        this.buttonColor = buttonColor;
    }

    public Color getButtonHoverColor() {
        return buttonHoverColor;
    }

    public void setButtonHoverColor(Color buttonHoverColor) {
        this.buttonHoverColor = buttonHoverColor;
    }

    public Color getStringColor() {
        return stringColor;
    }

    public void setStringColor(Color stringColor) {
        this.stringColor = stringColor;
    }
}
