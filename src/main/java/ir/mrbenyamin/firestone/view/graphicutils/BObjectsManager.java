package ir.mrbenyamin.firestone.view.graphicutils;

import ir.mrbenyamin.firestone.view.Handler;
import ir.mrbenyamin.firestone.view.graphicutils.textbox.BTextBox;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.Iterator;

public class BObjectsManager {
    private Handler handler;
    private ArrayList<BObject> objects;

    public BObjectsManager(Handler handler){
        this.handler = handler;
        objects = new ArrayList<>();
    }

    public void tick(){
        // avoiding ConcurrentModificationException
        for (int i = 0; i < objects.size(); i++) {
            objects.get(i).tick();
        }
    }

    public void render(Graphics2D g){
        // for avoiding exceptions I don't use foreach loop
        for (int i = 0; i < objects.size(); i++) {
            objects.get(i).render(g);
        }
    }

    public void onMouseMove(MouseEvent e){
        for(BObject o : objects)
            o.onMouseMove(e);
    }

    public void onMouseRelease(MouseEvent e) throws IOException {
        for (int i = 0; i < objects.size(); i++) {
            objects.get(i).onMouseRelease(e);
        }
    }

    public void onKeyTyped(KeyEvent e){
        for(BObject o : objects)
            if (o instanceof BTextBox){
                o.onKeyTyped(e);
            }
    }

    public void addObject(BObject o){
        objects.add(o);
    }

    public void removeObject(BObject o){
        objects.remove(o);
    }

    public Handler getHandler() {
        return handler;
    }

    public void setHandler(Handler handler) {
        this.handler = handler;
    }

    public ArrayList<BObject> getObjects() {
        return objects;
    }

    public void setObjects(ArrayList<BObject> objects) {
        this.objects = objects;
    }

    public int size(){
        return objects.size();
    }
}
