package ir.mrbenyamin.firestone.view.graphicutils.textbox;

import ir.mrbenyamin.firestone.view.graphicutils.BObject;
import ir.mrbenyamin.firestone.view.graphicutils.ClickListener;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;

public class BTextBox extends BGeneralTextBox {
    boolean isUnderType;
    private ClickListener clickListener;
    String typedString = "";
    Font font;

    public BTextBox(float x, float y, int width, int height, ClickListener clickListener) {
        super(x, y, width, height);
        this.clickListener = clickListener;
        font = new Font("Arial", Font.PLAIN, 22);
    }

    @Override
    public void tick() {

    }

    @Override
    public void render(Graphics2D g) {
        renderString(g, isUnderType, font);
        g.drawString(typedString, x + 5, y + height / 2 + font.getSize() / 4);
    }

    @Override
    public void onClick() throws IOException {
        clickListener.onClick();
    }

    public void type(KeyEvent e){
        if (isUnderType){
            if (e.getKeyChar() == '\b' && typedString.length() > 0){
                typedString = typedString.substring(0, typedString.length() - 1);
            }else  if (!(e.getKeyChar() == '\b')) {
                if (typedString.length() <= 10) {
                    typedString += e.getKeyChar();
                }
            }
        }
    }

    // Getter and setters


    public boolean isUnderType() {
        return isUnderType;
    }

    public void setUnderType(boolean underType) {
        isUnderType = underType;
    }

    public ClickListener getClickListener() {
        return clickListener;
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public String getTypedString() {
        return typedString;
    }

    public void setTypedString(String typedString) {
        this.typedString = typedString;
    }

    public Font getFont() {
        return font;
    }

    public void setFont(Font font) {
        this.font = font;
    }
}
