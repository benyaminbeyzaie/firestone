package ir.mrbenyamin.firestone.view.graphicutils.textbox;

import ir.mrbenyamin.firestone.view.graphicutils.BObject;

import java.awt.*;

abstract class BGeneralTextBox extends BObject {
    BGeneralTextBox(float x, float y, int width, int height) {
        super(x, y, width, height);
    }

    void renderString(Graphics2D g, boolean isUnderType, Font font){
        g.setColor(Color.LIGHT_GRAY);
        g.fillRect((int) x,(int) y, width, height);
        if (isUnderType){
            g.setColor(Color.ORANGE);
            g.fillRect((int) x, (int) y + height - (height / 8), width, height / 8);
        }
        g.setFont(font);
        g.setColor(Color.DARK_GRAY);
    }
}
