package ir.mrbenyamin.firestone.view.graphicutils;

import ir.mrbenyamin.firestone.view.graphicutils.textbox.BTextBox;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.io.IOException;

public abstract class BObject {
    protected float x, y;
    protected int width, height;
    private Rectangle bounds;
    protected boolean hovering = false;

    public BObject(float x, float y, int width, int height){
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        bounds = new Rectangle((int) x, (int) y, width, height);
    }

    public abstract void tick();

    public abstract void render(Graphics2D g);

    public abstract void onClick() throws IOException;

    void onMouseMove(MouseEvent e){
        hovering = bounds.contains(e.getX(), e.getY());
    }

    void onMouseRelease(MouseEvent e) throws IOException {
        if(hovering)
            onClick();
        else if (this instanceof BTextBox){
            ((BTextBox) this).setUnderType(false);
        }
    }

    void onKeyTyped(KeyEvent e){
        if (this instanceof BTextBox){
            ((BTextBox) this).type(e);
        }
    }
}
