package ir.mrbenyamin.firestone.view.graphicutils.textbox;

import ir.mrbenyamin.firestone.view.graphicutils.ClickListener;

import java.awt.*;
import java.awt.event.KeyEvent;

public class BPasswordTextBox extends BTextBox {
    private String renderedPass = "";
    public BPasswordTextBox(float x, float y, int width, int height, ClickListener clickListener) {
        super(x, y, width, height, clickListener);
    }

    public void render(Graphics2D g) {
        renderString(g, isUnderType, font);
        g.drawString(renderedPass, x + 5, y + height / 2 + font.getSize() / 2);
    }

    @Override
    public void type(KeyEvent e){
        if (isUnderType){
            if (e.getKeyChar() == '\b' && typedString.length() > 0){
                renderedPass = renderedPass.substring(0, typedString.length() - 1);
                typedString = typedString.substring(0, typedString.length() - 1);
            }else if (!(e.getKeyChar() == '\b')) {
                renderedPass += "*";
                typedString += e.getKeyChar();
            }
        }
    }

    public String getTypedString() {
        return typedString;
    }

    public void setTypedString(String typedString) {
        this.typedString = typedString;
    }

    public String getRenderedPass() {
        return renderedPass;
    }

    public void setRenderedPass(String renderedPass) {
        this.renderedPass = renderedPass;
    }
}
