package ir.mrbenyamin.firestone.view.graphicutils.heroview;


import ir.mrbenyamin.firestone.heroes.Hero;
import ir.mrbenyamin.firestone.view.assets.Assets;
import ir.mrbenyamin.firestone.view.graphicutils.BObject;
import ir.mrbenyamin.firestone.view.graphicutils.ClickListener;
import ir.mrbenyamin.firestone.view.graphicutils.button.BImageButton;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class HeroView extends BObject {
    private Hero hero;
    private ClickListener clickListener;

    public HeroView(Hero hero, int x, int y, int width, int height, ClickListener clickListener) {
        super(x, y, width, height);
        this.hero = hero;
        this.clickListener = clickListener;
    }

    @Override
    public void tick() {

    }

    @Override
    public void render(Graphics2D g) {
        if (hovering){
            g.fillRect((int)x, (int)y, width, height);
        }
        if(Assets.getHeroImage(hero.getName()) != null){
            g.drawImage(Assets.getHeroImage(hero.getName()), (int)x, (int)y, width, height, null);
            g.drawString(String.valueOf(hero.getBlood()), x + 75, y+ 92);
        }

    }

    @Override
    public void onClick() throws IOException {
        clickListener.onClick();
    }
}
