package ir.mrbenyamin.firestone.view.graphicutils.cardview;

import ir.mrbenyamin.firestone.card.Card;
import ir.mrbenyamin.firestone.player.Player;
import ir.mrbenyamin.firestone.view.assets.Assets;
import ir.mrbenyamin.firestone.view.graphicutils.ClickListener;

import java.awt.*;

public class OnBackCardView extends CardView {
    public OnBackCardView(Card card, Player signedPlayer, float x, float y, int width, int height, ClickListener clickListener) {
        super(card, signedPlayer, x, y, width, height, clickListener);
    }

    @Override
    public void tick() {

    }

    @Override
    public void render(Graphics2D g) {
        if (Assets.getBackCardImage(getSignedPlayer().getCardBackImageName()) != null){
            g.drawImage(Assets.getBackCardImage(getSignedPlayer().getCardBackImageName()),(int) x, (int) y, width, height, null);
        }
    }

    @Override
    public void onClick() {

    }
}
