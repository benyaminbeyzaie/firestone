package ir.mrbenyamin.firestone.view.graphicutils.cardview;

import ir.mrbenyamin.firestone.card.Card;
import ir.mrbenyamin.firestone.player.Player;
import ir.mrbenyamin.firestone.view.graphicutils.BObject;
import ir.mrbenyamin.firestone.view.graphicutils.ClickListener;

public abstract class CardView extends BObject {
    private Card card;
    private ClickListener clickListener;
    private Player signedPlayer;
    CardView(Card card, Player signedPlayer, float x, float y, int width, int height, ClickListener clickListener) {
        super(x, y, width, height);
        this.card = card;
        this.signedPlayer = signedPlayer;
        this.clickListener = clickListener;
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    public ClickListener getClickListener() {
        return clickListener;
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public Player getSignedPlayer() {
        return signedPlayer;
    }

    public void setSignedPlayer(Player signedPlayer) {
        this.signedPlayer = signedPlayer;
    }
}
