package ir.mrbenyamin.firestone.view.graphicutils.cardview;

import ir.mrbenyamin.firestone.card.Card;
import ir.mrbenyamin.firestone.player.Player;
import ir.mrbenyamin.firestone.view.graphicutils.ClickListener;

import java.awt.*;
import java.io.IOException;

public class OnFieldCardView extends CardView {
    public OnFieldCardView(Card card, Player signedPlayer, float x, float y, int width, int height, ClickListener clickListener) {
        super(card, signedPlayer, x, y, width, height, clickListener);
    }

    @Override
    public void tick() {

    }

    @Override
    public void render(Graphics2D g) {

    }

    @Override
    public void onClick() throws IOException {
        getClickListener().onClick();
    }
}
