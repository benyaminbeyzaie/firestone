package ir.mrbenyamin.firestone.view.graphicutils.button;

import ir.mrbenyamin.firestone.view.Game;
import ir.mrbenyamin.firestone.view.assets.Assets;
import ir.mrbenyamin.firestone.view.graphicutils.BObject;
import ir.mrbenyamin.firestone.view.graphicutils.ClickListener;

import java.io.IOException;

public abstract class BButton extends BObject {

    private ClickListener clickListener;

    public BButton(float x, float y, int width, int height, ClickListener clickListener) {
        super(x, y, width, height);
        this.clickListener = clickListener;
    }

    @Override
    public void onClick() throws IOException {
        Game.playSound(Assets.click);
            clickListener.onClick();
    }

}
