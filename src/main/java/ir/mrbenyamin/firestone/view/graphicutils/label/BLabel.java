package ir.mrbenyamin.firestone.view.graphicutils.label;

import ir.mrbenyamin.firestone.view.Constants;
import ir.mrbenyamin.firestone.view.graphicutils.BObject;

import java.awt.*;

public class BLabel extends BObject {
    private String string;
    private int size;
    private Font font;
    private Color stringColor = Constants.LABEL_WHITE;
    public BLabel(String string, float x, float y, int width, int height, int size) {
        super(x, y, width, height);
        this.string = string;
        this.size = size;
        font = new Font("Arial", Font.BOLD, size);
    }

    @Override
    public void tick() {

    }

    @Override
    public void render(Graphics2D g) {
        g.setColor(stringColor);
        g.setFont(font);
        g.drawString(string, x, y);
    }

    @Override
    public void onClick() {

    }

    public String getString() {
        return string;
    }

    public void setString(String string) {
        this.string = string;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public Font getFont() {
        return font;
    }

    public void setFont(Font font) {
        this.font = font;
    }

    public Color getStringColor() {
        return stringColor;
    }

    public void setStringColor(Color stringColor) {
        this.stringColor = stringColor;
    }
}
