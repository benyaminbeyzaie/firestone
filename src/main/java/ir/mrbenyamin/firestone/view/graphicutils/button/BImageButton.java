package ir.mrbenyamin.firestone.view.graphicutils.button;

import ir.mrbenyamin.firestone.view.graphicutils.ClickListener;

import java.awt.*;
import java.awt.image.BufferedImage;

public class BImageButton extends BButton {
    private BufferedImage[] images = new BufferedImage[2];
    private String string;
    private Font font;

    public BImageButton(float x, float y, int width, int height, BufferedImage[] images, ClickListener clickListener) {
        super(x, y, width, height, clickListener);
        this.images = images;
    }

    public BImageButton(float x, float y, int width, int height, BufferedImage image, ClickListener clickListener) {
        super(x, y, width, height, clickListener);
        this.images[0] = image;
        this.images[1] = image;
    }

    @Override
    public void tick() {

    }

    @Override
    public void render(Graphics2D g) {
        if(hovering)
            g.drawImage(images[1], (int) x, (int) y, width, height, null);
        else
            g.drawImage(images[0], (int) x, (int) y, width, height, null);
        if (string != null) {
            font = new Font("Arial", Font.BOLD, 14);
            g.setFont(font);
            FontMetrics fontMetrics = g.getFontMetrics(font);
            int width = fontMetrics.stringWidth(string);
            g.drawString(string, (int)x + this.width / 2 - width / 2, (int)y + height / 2 + font.getSize() / 4 + 2);
        }
    }

    public BufferedImage[] getImages() {
        return images;
    }

    public void setImages(BufferedImage[] images) {
        this.images = images;
    }

    public String getString() {
        return string;
    }

    public void setString(String string) {
        this.string = string;
    }
}
