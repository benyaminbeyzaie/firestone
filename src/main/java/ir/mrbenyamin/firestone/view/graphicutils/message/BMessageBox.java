package ir.mrbenyamin.firestone.view.graphicutils.message;

import ir.mrbenyamin.firestone.view.Constants;
import ir.mrbenyamin.firestone.view.graphicutils.BObject;

import java.awt.*;
import java.awt.image.BufferedImage;

public abstract class BMessageBox extends BObject {
    String message;
    Font stringFont;
    int fontSize = 12;
    Color backgroundColor;
    Color stringColor = Constants.WARNING_FONT_COLOR;
    BufferedImage image;
    boolean show = false;
    long showTime;
    long startShow;
    public BMessageBox(String message,BufferedImage image, float x, float y, int width, int height, long showTime) {
        super(x, y, width, height);
        this.message = message;
        this.showTime = showTime;
        this.image = image;
    }


    void renderBase(Graphics2D g){
        g.setColor(backgroundColor);
        g.fillRect((int)x, (int)y, width, height);
        g.setColor(stringColor);
        stringFont = new Font("Arial", Font.BOLD, fontSize);
        g.setFont(stringFont);
        FontMetrics fontMetrics = g.getFontMetrics(stringFont);
        int width = fontMetrics.stringWidth(message);
        g.setColor(stringColor);
        if (this.image != null){
            g.drawImage(image , (int) x + 5 , (int)y + height / 2 - image.getHeight() / 2, null);
        }
        g.drawString(this.message, (int)x + this.width / 2 - width / 2, (int)y + height / 2 + fontSize / 4);
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Font getStringFont() {
        return stringFont;
    }

    public void setStringFont(Font stringFont) {
        this.stringFont = stringFont;
    }

    public int getFontSize() {
        return fontSize;
    }

    public void setFontSize(int fontSize) {
        this.fontSize = fontSize;
    }

    public Color getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(Color backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public Color getStringColor() {
        return stringColor;
    }

    public void setStringColor(Color stringColor) {
        this.stringColor = stringColor;
    }

    public BufferedImage getImage() {
        return image;
    }

    public void setImage(BufferedImage image) {
        this.image = image;
    }

    public boolean isShow() {
        return show;
    }

    public void setShow(boolean show) {
        if (show){
            startShow = System.nanoTime();
        }
        this.show = show;
    }

    public long getShowTime() {
        return showTime;
    }

    public void setShowTime(long showTime) {
        this.showTime = showTime;
    }

    public long getStartShow() {
        return startShow;
    }

    public void setStartShow(long startShow) {
        this.startShow = startShow;
    }
}
