package ir.mrbenyamin.firestone.view.graphicutils.cardview;

import ir.mrbenyamin.firestone.card.Card;
import ir.mrbenyamin.firestone.card.Minion;
import ir.mrbenyamin.firestone.player.Player;
import ir.mrbenyamin.firestone.view.Constants;
import ir.mrbenyamin.firestone.view.assets.Assets;
import ir.mrbenyamin.firestone.view.graphicutils.ClickListener;

import java.awt.*;
import java.io.IOException;

public class HandCardView extends CardView {
    public HandCardView(Card card, Player signedPlayer, float x, float y, int width, int height, ClickListener clickListener) {
        super(card, signedPlayer, x, y, width, height, clickListener);
    }

    @Override
    public void tick() {

    }

    @Override
    public void render(Graphics2D g) {
        if (Assets.getImage(getCard().getName()) != null){
            if (getCard().getType().equals("Minion") && ((Minion) getCard()).isTaut()){
                // drawing taut
                g.setColor(Color.BLUE);
                g.fillRect((int)x - 1, (int)y + width - 10, (int)width+2 , (int)20);
            }
            if (getCard().getType().equals("Minion") && ((Minion) getCard()).isShield()){
                // drawing shield
                g.setColor(Color.YELLOW);
                g.fillRect((int)x - 1, (int)y + width - 50, (int)width+2 , (int)20);
            }
            g.setColor(Color.WHITE);
            g.drawImage(Assets.getImage(getCard().getName())[0],(int) x, (int) y, width, height, null);
            g.setFont(Constants.CARDS_TEXT_FONT);
            if (getCard().getType().equals("Minion")){
                g.drawString(String.valueOf(((Minion)getCard()).getAttack()), x + 7, y + height - 3);
                g.drawString(String.valueOf(((Minion)getCard()).getHealth()), x + width - 12, y + height - 3);

            }
            g.drawString(String.valueOf(getCard().getManaCost()), x+ 8, y + 18);
            if(hovering){
                g.drawImage(Assets.getImage(getCard().getName())[0],(int) x - 10, (int) y - 200,
                        5/2 * width, 5/2 * height, null);
                g.setFont(new Font("Arial", Font.BOLD, 22));
                g.drawString(String.valueOf(getCard().getManaCost()), x + 5, y - 165);
                if (getCard().getType().equals("Minion")){
                    g.drawString(String.valueOf(((Minion)getCard()).getAttack()), x + 5, y - 210 + (5/2) * height);
                    g.drawString(String.valueOf(((Minion)getCard()).getHealth()), x + (5/2) * width - 35, y - 210 + (5/2) * height);
                }
            }
            g.setFont(new Font("Arial", Font.BOLD, 14));
        }
    }

    @Override
    public void onClick() throws IOException {
        getClickListener().onClick();
    }
}
