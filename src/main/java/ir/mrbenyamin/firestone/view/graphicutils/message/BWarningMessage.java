package ir.mrbenyamin.firestone.view.graphicutils.message;

import ir.mrbenyamin.firestone.view.Constants;
import ir.mrbenyamin.firestone.view.assets.Assets;

import java.awt.*;
import java.awt.image.BufferedImage;

public class BWarningMessage extends BMessageBox {
    public BWarningMessage(String message, BufferedImage image, float x, float y, int width, int height, long showTime) {
        super(message,image, x, y, width, height, showTime);
        backgroundColor = Constants.WARNING_COLOR;
    }

    @Override
    public void tick() {

    }

    @Override
    public void render(Graphics2D g) {
        if (show){
            renderBase(g);
            if (System.nanoTime() >= startShow + showTime) show = false;

        }
    }

    @Override
    public void onClick() {

    }
}
