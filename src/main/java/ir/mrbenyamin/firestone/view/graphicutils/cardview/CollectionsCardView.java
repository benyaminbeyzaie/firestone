package ir.mrbenyamin.firestone.view.graphicutils.cardview;

import ir.mrbenyamin.firestone.card.Card;
import ir.mrbenyamin.firestone.card.Minion;
import ir.mrbenyamin.firestone.player.Player;
import ir.mrbenyamin.firestone.view.Constants;
import ir.mrbenyamin.firestone.view.assets.Assets;
import ir.mrbenyamin.firestone.view.assets.ImageLoader;
import ir.mrbenyamin.firestone.view.graphicutils.BObject;
import ir.mrbenyamin.firestone.view.graphicutils.ClickListener;

import java.awt.*;
import java.io.IOException;

public class CollectionsCardView extends CardView {

    public CollectionsCardView(Card card, Player signedPlayer, float x, float y, int width, int height, ClickListener clickListener) {
        super(card, signedPlayer, x, y, width, height, clickListener);
    }


    @Override
    public void tick() {

    }

    @Override
    public void render(Graphics2D g) {
        if (Assets.getImage(getCard().getName()) != null){
            if (getSignedPlayer().getAllAvailableCards().contains(getCard())){
                g.drawImage(Assets.getImage(getCard().getName())[0],(int) x, (int) y, width, height, null);
            }else {
                g.drawImage(Assets.getImage(getCard().getName())[1],(int) x, (int) y, width, height, null);
            }
            g.setFont(Constants.COLLECTIONS_CARDS_TEXT_FONT);
            g.setColor(Color.white);
            if (getCard().getType().equals("Minion")){
                g.drawString(String.valueOf(((Minion)getCard()).getHealth()), x + 12, y + height - 8);
                g.drawString(String.valueOf(((Minion)getCard()).getAttack()), x + width - 22, y + height - 8);
            }
            g.drawString(String.valueOf(getCard().getManaCost()), x+ 12, y + 28);
        }
    }

    @Override
    public void onClick() throws IOException {
        getClickListener().onClick();
    }
}
