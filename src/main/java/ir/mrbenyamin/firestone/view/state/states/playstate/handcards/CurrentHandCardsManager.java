package ir.mrbenyamin.firestone.view.state.states.playstate.handcards;

import ir.mrbenyamin.firestone.card.Card;
import ir.mrbenyamin.firestone.card.Minion;
import ir.mrbenyamin.firestone.card.Weapon;
import ir.mrbenyamin.firestone.heroes.Hero;
import ir.mrbenyamin.firestone.log.MyLogger;
import ir.mrbenyamin.firestone.play.InfoPassiveType;
import ir.mrbenyamin.firestone.play.Play;
import ir.mrbenyamin.firestone.player.Player;
import ir.mrbenyamin.firestone.view.Constants;
import ir.mrbenyamin.firestone.view.Handler;
import ir.mrbenyamin.firestone.view.assets.Assets;
import ir.mrbenyamin.firestone.view.graphicutils.ClickListener;
import ir.mrbenyamin.firestone.view.graphicutils.button.BButton;
import ir.mrbenyamin.firestone.view.graphicutils.button.BTextButton;
import ir.mrbenyamin.firestone.view.graphicutils.cardview.HandCardView;
import ir.mrbenyamin.firestone.view.graphicutils.cardview.OnFieldWeaponView;
import ir.mrbenyamin.firestone.view.graphicutils.heroview.HeroView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.logging.Level;

public class CurrentHandCardsManager extends HandCardsManager {

    private ArrayList<Integer> types;
    private ArrayList<Integer> one;
    private ArrayList<Integer> two;

    public CurrentHandCardsManager(Handler handler, Play play) {
        super(handler, play);
        types = new ArrayList<>();
        for (int i = 0; i < InfoPassiveType.values().length; i++) {
            types.add(i);
        }
        Collections.shuffle(types);
        one = copy(types);
        Collections.shuffle(types);
        two = copy(types);
    }

    private ArrayList<Integer> copy(ArrayList<Integer> list){
        return new ArrayList<>(list);
    }

    public void addHero(Hero hero) {
        HeroView heroView = new HeroView(hero, 900, 430,
                Assets.getHeroImage(hero.getName()).getWidth()/3,
                Assets.getHeroImage(hero.getName()).getHeight()/3, () -> {

                });
        manager.addObject(heroView);
    }

    private void addWeaponView(Weapon weapon){
        if (weapon != null){
            OnFieldWeaponView weaponView = new OnFieldWeaponView(weapon, play.getCurrentPlayer(), 810, 410,
                    Assets.getWeaponImage(weapon.getName())[0].getWidth() / 3,
                    Assets.getWeaponImage(weapon.getName())[0].getHeight() / 3,
                    new ClickListener() {
                        @Override
                        public void onClick() {
                            // weapon function!!
                            if (play.getCurrentPlayer().getInfoPassive() != null){
                                play.setOnFieldSelectedCard(weapon);
                                handler.getGame().getPlayState().setSelectedCardX(815);
                                handler.getGame().getPlayState().setSelectedCardY(400);
                            }
                        }
                    });
            manager.addObject(weaponView);
        }
    }

    public void updateCardViews() {
        Player currentPlayer = play.getCurrentPlayer();
        manager.getObjects().removeAll(manager.getObjects());
        addSummonButton();
        addWeaponView(currentPlayer.getActiveWeapon());
        if (currentPlayer.getInfoPassive() == null){
            addInfoPassiveSelectButton();
        }
        addHero(currentPlayer.getPlayingDeck().getHero());
        for (int i = 0; i < currentPlayer.getHandCards().size(); i++) {
            int finalI = i;
            HandCardView handCardView = new HandCardView(currentPlayer.getHandCards().get(i),
                    currentPlayer,
                    5 + i * 2 + i * (3 * Constants.CARD_VIEW_WIDTH / 5),
                    600 - 10 - 3 * Constants.CARD_VIEW_HEIGHTS / 5,
                    3 * Constants.CARD_VIEW_WIDTH / 5, 3 * Constants.CARD_VIEW_HEIGHTS / 5,
                    () -> {
                        if (play.getCurrentPlayer().getInfoPassive() != null){
                            play.setOnHandSelectedCard(currentPlayer.getHandCards().get(finalI));
                            handler.getGame().getPlayState().setSelectedCardX(5 + finalI * 2 + finalI * (3 * Constants.CARD_VIEW_WIDTH / 5));
                            handler.getGame().getPlayState().setSelectedCardY(600 - 10 - 3 * Constants.CARD_VIEW_HEIGHTS / 5);
                        }else {
                            if (!play.getCurrentPlayer().getChangedCardFinalI().contains(finalI)){
                                currentPlayer.getPlayingDeck().getCards().add(currentPlayer.getHandCards().get(finalI));
                                currentPlayer.getHandCards().remove(finalI);
                                play.addACardFromDeckToHand();
                                updateCardViews();
                                play.getCurrentPlayer().getChangedCardFinalI().add(finalI);
                            }
                        }
                    });
            manager.addObject(handCardView);
        }

    }

    private void addInfoPassiveSelectButton(){
        BTextButton[] infoPassiveButtons = new BTextButton[3];
        for (int i = 0; i < 3; i++) {
            int finalI = i;
            if (play.getCurrentPlayer() == play.getPlayerOne()){
                infoPassiveButtons[i] = new BTextButton(InfoPassiveType.getInfoPassive(one.get(finalI)).name(), 350 + finalI * 105, 460, 100, 20, () -> addPassive(finalI));
            }else {
                infoPassiveButtons[i] = new BTextButton(InfoPassiveType.getInfoPassive(two.get(finalI)).name(), 350 + finalI * 105, 460, 100, 20, () -> addPassive(finalI));
            }
            manager.addObject(infoPassiveButtons[i]);
        }
    }

    private void addPassive(int i) {
        ArrayList<Integer> list;
        if (play.getCurrentPlayer() == play.getPlayerOne()) list = one;
        else list = two;
        handler.getGame().getPlayState().getPlay().getCurrentPlayer().setInfoPassive(InfoPassiveType.getInfoPassive(list.get(i)));
        actionInfoPassive(InfoPassiveType.getInfoPassive(list.get(i)));
        updateCardViews();
        try {
            MyLogger.writeLog(play.getCurrentPlayer().getUsername() + "-" + play.getCurrentPlayer().getStringID(),  "select " + InfoPassiveType.getInfoPassive(list.get(i)) + " info passive", Level.INFO);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void actionInfoPassive(InfoPassiveType infoPassive) {
        if (infoPassive.equals(InfoPassiveType.OFF_CARDS)){
            for (Card card :
                    play.getCurrentPlayer().getPlayingDeck().getCards()) {
                if (card.getManaCost()  != 0){
                    card.setManaCost(card.getManaCost() - 1);
                }
            }
            for (Card card :
                    play.getCurrentPlayer().getHandCards()) {
                card.setManaCost(card.getManaCost() - 1);
            }
        }else if (infoPassive.equals(InfoPassiveType.MANA_JUMP)){
            play.getCurrentPlayer().setMaxMana(play.getCurrentPlayer().getMaxMana() + 1);
        }
    }

    private void addSummonButton(){
        BButton summonButton = new BTextButton("Summon", 240, 460, 100, 20, () -> {
            int temp = play.summonAction(play.getOnHandSelectedCard());
            if (temp == 1){
                if (play.getOnHandSelectedCard().getType().equals("Spell")){
                    try {
                        play.spellAction(play.getOnHandSelectedCard());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                // battlecry
                if (play.getOnHandSelectedCard().getType().equals("Minion")){
                    for (int i = 0; i < play.getCurrentPlayer().getOnFieldMinions().size(); i++) {
                        if (play.getCurrentPlayer().getOnFieldMinions().get(i).getName().equals("High Priest Amet")){
                            ((Minion) play.getOnHandSelectedCard()).
                                    setHealth(play.getCurrentPlayer().getOnFieldMinions().get(i).getHealth());
                        }
                    }
                    // call the battlecry
                    if (((Minion) play.getOnHandSelectedCard()).hasBattlecry()){
                        try {
                            play.battlecry(play.getOnHandSelectedCard());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
                if (play.getOnHandSelectedCard().getType().equals("Weapon")){
                    // call the battlecry
                    if (((Weapon) play.getOnHandSelectedCard()).isHasBattlecry()){
                        try {
                            play.battlecry(play.getOnHandSelectedCard());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            if (temp == 4){
                handler.getGame().getPlayState().getWarningMessage().setMessage(
                        "Select Info passive first"
                );
                handler.getGame().getPlayState().getWarningMessage().setBackgroundColor(Constants.WARNING_COLOR);
                handler.getGame().getPlayState().getWarningMessage().setImage(Assets.warning);
                handler.getGame().getPlayState().getWarningMessage().setShow(true);
            }
            else if (temp == 0) {
                handler.getGame().getPlayState().getWarningMessage().setMessage(
                        "Select a card First"
                );
                handler.getGame().getPlayState().getWarningMessage().setBackgroundColor(Constants.WARNING_COLOR);
                handler.getGame().getPlayState().getWarningMessage().setImage(Assets.warning);
                handler.getGame().getPlayState().getWarningMessage().setShow(true);
            } else if (temp == 2) {
                handler.getGame().getPlayState().getWarningMessage().setMessage(
                        "Your Mana is not enough!"
                );
                handler.getGame().getPlayState().getWarningMessage().setBackgroundColor(Constants.WARNING_COLOR);
                handler.getGame().getPlayState().getWarningMessage().setImage(Assets.warning);
                handler.getGame().getPlayState().getWarningMessage().setShow(true);
            } else if (temp == 3) {
                handler.getGame().getPlayState().getWarningMessage().setMessage(
                        "Field is full!"
                );
                handler.getGame().getPlayState().getWarningMessage().setBackgroundColor(Constants.WARNING_COLOR);
                handler.getGame().getPlayState().getWarningMessage().setImage(Assets.warning);
                handler.getGame().getPlayState().getWarningMessage().setShow(true);
            }else if (temp == 5) {
                handler.getGame().getPlayState().getWarningMessage().setMessage(
                        "Play Spell or Battlecry First. Select Friendly or enemy minion"
                );
                handler.getGame().getPlayState().getWarningMessage().setBackgroundColor(Constants.WARNING_COLOR);
                handler.getGame().getPlayState().getWarningMessage().setImage(Assets.warning);
                handler.getGame().getPlayState().getWarningMessage().setShow(true);
            }
            play.setOnHandSelectedCard(null);
            handler.getGame().getPlayState().setSelectedCardX(1001);
            updateCardViews();
            handler.getGame().getPlayState().updateCardViews();
        });
        manager.addObject(summonButton);
    }

}
