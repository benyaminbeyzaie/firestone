package ir.mrbenyamin.firestone.view.state.states.playstate;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import ir.mrbenyamin.firestone.card.Card;
import ir.mrbenyamin.firestone.play.InfoPassiveType;
import ir.mrbenyamin.firestone.play.Play;
import ir.mrbenyamin.firestone.play.action.attack.AttackToHero;
import ir.mrbenyamin.firestone.play.action.attack.AttackToMinion;
import ir.mrbenyamin.firestone.play.action.summon.SummonSpell;
import ir.mrbenyamin.firestone.player.Player;
import ir.mrbenyamin.firestone.player.sign.De;
import ir.mrbenyamin.firestone.player.sign.SignIn;
import ir.mrbenyamin.firestone.util.HibernateUtil;
import ir.mrbenyamin.firestone.view.Constants;
import ir.mrbenyamin.firestone.view.Game;
import ir.mrbenyamin.firestone.view.Handler;
import ir.mrbenyamin.firestone.view.assets.Assets;
import ir.mrbenyamin.firestone.view.display.MouseManager;
import ir.mrbenyamin.firestone.view.graphicutils.BObjectsManager;
import ir.mrbenyamin.firestone.view.graphicutils.button.BImageButton;
import ir.mrbenyamin.firestone.view.graphicutils.button.BTextButton;
import ir.mrbenyamin.firestone.view.graphicutils.message.BWarningMessage;
import ir.mrbenyamin.firestone.view.state.State;
import ir.mrbenyamin.firestone.view.state.StateManager;
import ir.mrbenyamin.firestone.view.state.states.playstate.fieldcards.CurrentFieldCardsManager;
import ir.mrbenyamin.firestone.view.state.states.playstate.fieldcards.OpponentFieldCardsManager;
import ir.mrbenyamin.firestone.view.state.states.playstate.handcards.CurrentHandCardsManager;
import ir.mrbenyamin.firestone.view.state.states.playstate.handcards.OpponentHandCardsManager;
import org.hibernate.Session;

import java.awt.*;
import java.io.File;
import java.io.IOException;

public class PlayState extends State {
    private Player signedPlayer;
    private Player imaginaryPlayer;
    private Play play;
    private BTextButton exitGame;
    private CurrentHandCardsManager currentHandCardsManager;
    private OpponentHandCardsManager opponentHandCardsManager;
    private CurrentFieldCardsManager currentFieldCardsManager;
    private OpponentFieldCardsManager opponentFieldCardsManager;
    private BWarningMessage warningMessage;
    private int selectedCardX = 1001;
    private int selectedCardY = 0;
    private int timeToEnd = 10;

    public PlayState(Handler handler){
        super(handler);
        setName("Play");
        signedPlayer = handler.getGame().getSignedPlayer();
        // Set up imaginary Player
        setUpImaginaryPlayer();


        manager = new BObjectsManager(handler);

        warningMessage = new BWarningMessage("", Assets.warning, 250, 290, 500, 30, 3000000000L);

        manager.addObject(warningMessage);
        exitGame = new BTextButton("exit Game", 10, 5, 100, 20, () -> {
            handler.getGame().getSignedPlayer().saveSignedPlayerInfo();
            play.getTimer().stop();
            play.setTimer(null);
            Game.stopBackSound();
            handler.getGame().playBackSound("all");
            handler.getGame().getDisplay().changeDisplaySize(Constants.STANDARD_STATES_WIDTH, Constants.STANDARD_STATES_HEIGHT);
            StateManager.setCurrentState(handler.getGame().getMainState());
        });
        BImageButton endTurnButton = new BImageButton(1000 - Assets.endTurn[0].getWidth() - 10, 600 / 2 - (Assets.endTurn[0].getHeight() / 2), Assets.endTurn[0].getWidth(), Assets.endTurn[1].getHeight(), Assets.endTurn, () -> endThings());
        manager.addObject(endTurnButton);
        manager.addObject(exitGame);
    }

    public void endThings() throws IOException {
        play.checkForTurnBaseActionCards();
        int end = play.endTurn();
        if (end == 0){
            play.setOnHandSelectedCard(null);
            play.setOnFieldSelectedCard(null);
            handler.getGame().getPlayState().setSelectedCardX(1001);
            handler.getGame().getPlayState().updateCardViews();
            if (play.getCurrentPlayer().getInfoPassive() != null && play.getCurrentPlayer().getInfoPassive().equals(InfoPassiveType.TWICE_DRAW)){
                play.addACardFromDeckToHand();
            }
            int temp = play.addACardFromDeckToHand();
            if (temp == 0){
                handler.getGame().getPlayState().getWarningMessage().setMessage(
                        "You don't have any card on your deck!"
                );
                handler.getGame().getPlayState().getWarningMessage().setBackgroundColor(Constants.WARNING_COLOR);
                handler.getGame().getPlayState().getWarningMessage().setImage(Assets.warning);
                handler.getGame().getPlayState().getWarningMessage().setShow(true);
            }else if (temp == 1){
                handler.getGame().getPlayState().getWarningMessage().setMessage(
                        "Hand is full!"
                );
                handler.getGame().getPlayState().getWarningMessage().setBackgroundColor(Constants.WARNING_COLOR);
                handler.getGame().getPlayState().getWarningMessage().setImage(Assets.warning);
                handler.getGame().getPlayState().getWarningMessage().setShow(true);
            }
            currentHandCardsManager.updateCardViews();
        }else {
            handler.getGame().getPlayState().getWarningMessage().setMessage(
                    "Select info passive first"
            );
            handler.getGame().getPlayState().getWarningMessage().setBackgroundColor(Constants.WARNING_COLOR);
            handler.getGame().getPlayState().getWarningMessage().setImage(Assets.warning);
            handler.getGame().getPlayState().getWarningMessage().setShow(true);
        }
    }

    private void setUpImaginaryPlayer() {
        ObjectMapper objectMapper = new ObjectMapper();
        SimpleModule simpleModule = new SimpleModule();
        simpleModule.addKeyDeserializer(Card.class, new De());
        objectMapper.registerModule(simpleModule);
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        File imaginaryPlayerJsonFile = new File(SignIn.PLAYER_FILE_PATH + "imaginary" + ".json");
        try {
            imaginaryPlayer = objectMapper.readValue(imaginaryPlayerJsonFile, Player.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void tick() {
        manager.tick();
        currentHandCardsManager.tick();
        opponentHandCardsManager.tick();
        currentFieldCardsManager.tick();
        opponentFieldCardsManager.tick();
        handler.getGame().getMouseManager().setBObjectManager(manager);
        //System.out.println("MouseX: " + MouseManager.mouseX + ", MouseY: " + MouseManager.mouseY);
        if (MouseManager.mouseY >= 448 && MouseManager.mouseX < 925){
            handler.getGame().getMouseManager().setBObjectManager(currentHandCardsManager.getManager());
        }else if(MouseManager.mouseY >= 300 && MouseManager.mouseX < 925){
            handler.getGame().getMouseManager().setBObjectManager(currentFieldCardsManager.getManager());
        }else if(MouseManager.mouseY >= 153 && MouseManager.mouseX < 925){
            handler.getGame().getMouseManager().setBObjectManager(opponentFieldCardsManager.getManager());
        }else if (MouseManager.mouseY >= 30 && MouseManager.mouseY < 153 && MouseManager.mouseX < Constants.PLAY_WIDTH){
            handler.getGame().getMouseManager().setBObjectManager(opponentHandCardsManager.getManager());
        }

    }

    @Override
    public void render(Graphics2D g) {
        g.setFont(new Font("Arial", Font.BOLD, 10));
        g.drawImage(Assets.getGameBackgroundImage(play.getCurrentPlayer().getGameBackImageName()), 0, 0, null);
        g.drawString("Card in deck: " + play.getCurrentPlayer().getPlayingDeck().getCards().size() , 900, 590);
        renderSelectedCardBack(g);
        renderGems(play.getCurrentPlayer().getCurrentMana(), g);
        manager.render(g);

        opponentHandCardsManager.render(g);
        currentFieldCardsManager.render(g);
        opponentFieldCardsManager.render(g);
        currentHandCardsManager.render(g);
        renderActions(g);
        if (timeToEnd <= 5){
            g.setFont(new Font("Arial", Font.BOLD, 22));
            g.setColor(Color.RED);
            g.drawString(timeToEnd + "" , 10, 300);
        }

    }

    public void createManagers(){
        currentHandCardsManager = new CurrentHandCardsManager(handler, play);
        opponentHandCardsManager = new OpponentHandCardsManager(handler, play);
        currentFieldCardsManager = new CurrentFieldCardsManager(handler, play);
        opponentFieldCardsManager = new OpponentFieldCardsManager(handler, play);
    }

    private void renderSelectedCardBack(Graphics2D g) {
        g.setColor(Constants.BUTTON_COLOR_3);
        g.fillRect(selectedCardX,
                selectedCardY,
                3 * Constants.CARD_VIEW_WIDTH / 5 + 2,
                3 * Constants.CARD_VIEW_HEIGHTS / 5 + 2);
    }

    private void renderActions(Graphics2D g) {
        final int size = 10;
        int counter;
        int minus;
        if (play.getActions().size() < size){
            counter = 0;
            minus = 0;
        }
        else{
            counter = play.getActions().size() - size;
            minus = play.getActions().size() - size;
        }
        for (int i = play.getActions().size() - 1; i >= counter ; i--) {
                if (play.getActions().get(i).isDone()){
                    g.setFont(new Font("Arial", Font.PLAIN, 10));
                    g.setColor(Constants.WARNING_FONT_COLOR);
                    if (play.getActions().get(i) instanceof AttackToMinion){
                        g.drawString(((AttackToMinion) play.getActions().get(i)).getAttacker().getName() +
                                " attacked to " +
                                ((AttackToMinion) play.getActions().get(i)).getTarget().getName(), 5 , 170 + (i - minus) * 15);
                    }else if (play.getActions().get(i) instanceof AttackToHero){
                        g.drawString(((AttackToHero) play.getActions().get(i)).getCardToPlay().getName() +
                                        " attacked to " +
                                        ((AttackToHero)play.getActions().get(i)).getPlayer().getPlayingDeck().getHero().getName(), 5, 170 + (i - minus) * 15);
                    }else if (play.getActions().get(i) instanceof SummonSpell){
                        g.drawString(play.getActions().get(i).getPlayer().getUsername() +
                                " summoned " +
                                play.getActions().get(i).getCardToPlay().getName(), 5 , 170 + (i - minus) * 15);
                    }else {
                        g.drawString(play.getActions().get(i).getPlayer().getUsername() +
                                " summoned " +
                                play.getActions().get(i).getCardToPlay().getName(), 5 , 170 + (i - minus) * 15);
                    }
                }
        }
    }

    private void renderGems(int currentMana, Graphics2D g) {
        for (int i = 0; i < currentMana; i++) {
            g.drawImage(Assets.redGem[0], 10 + (i * 22), 460, 20, 22, null);
        }
    }

    public void updateCardViews(){
        currentFieldCardsManager.updateCardViews();
        opponentFieldCardsManager.updateCardViews();
        currentHandCardsManager.updateCardViews();
        opponentHandCardsManager.updateCardViews();
    }


    public Player getSignedPlayer() {
        return signedPlayer;
    }

    public void setSignedPlayer(Player signedPlayer) {
        this.signedPlayer = signedPlayer;
    }

    public Player getImaginaryPlayer() {
        return imaginaryPlayer;
    }

    public void setImaginaryPlayer(Player imaginaryPlayer) {
        this.imaginaryPlayer = imaginaryPlayer;
    }

    public Play getPlay() {
        return play;
    }

    public void setPlay(Play play) {
        this.play = play;
    }

    public BTextButton getExitGame() {
        return exitGame;
    }

    public void setExitGame(BTextButton exitGame) {
        this.exitGame = exitGame;
    }

    public CurrentHandCardsManager getCurrentHandCardsManager() {
        return currentHandCardsManager;
    }

    public void setCurrentHandCardsManager(CurrentHandCardsManager currentHandCardsManager) {
        this.currentHandCardsManager = currentHandCardsManager;
    }

    public CurrentFieldCardsManager getCurrentFieldCardsManager() {
        return currentFieldCardsManager;
    }

    public void setCurrentFieldCardsManager(CurrentFieldCardsManager currentFieldCardsManager) {
        this.currentFieldCardsManager = currentFieldCardsManager;
    }

    public int getSelectedCardX() {
        return selectedCardX;
    }

    public void setSelectedCardX(int selectedCardX) {
        this.selectedCardX = selectedCardX;
    }

    public BWarningMessage getWarningMessage() {
        return warningMessage;
    }

    public void setWarningMessage(BWarningMessage warningMessage) {
        this.warningMessage = warningMessage;
    }

    public OpponentFieldCardsManager getOpponentFieldCardsManager() {
        return opponentFieldCardsManager;
    }

    public void setOpponentFieldCardsManager(OpponentFieldCardsManager opponentFieldCardsManager) {
        this.opponentFieldCardsManager = opponentFieldCardsManager;
    }

    public OpponentHandCardsManager getOpponentHandCardsManager() {
        return opponentHandCardsManager;
    }

    public void setOpponentHandCardsManager(OpponentHandCardsManager opponentHandCardsManager) {
        this.opponentHandCardsManager = opponentHandCardsManager;
    }

    public int getSelectedCardY() {
        return selectedCardY;
    }

    public void setSelectedCardY(int selectedCardY) {
        this.selectedCardY = selectedCardY;
    }

    public int getTimeToEnd() {
        return timeToEnd;
    }

    public void setTimeToEnd(int timeToEnd) {
        this.timeToEnd = timeToEnd;
    }

}
