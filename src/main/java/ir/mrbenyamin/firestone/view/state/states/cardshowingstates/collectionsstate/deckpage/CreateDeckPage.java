package ir.mrbenyamin.firestone.view.state.states.cardshowingstates.collectionsstate.deckpage;

import ir.mrbenyamin.firestone.card.Deck;
import ir.mrbenyamin.firestone.heroes.Hero;
import ir.mrbenyamin.firestone.log.MyLogger;
import ir.mrbenyamin.firestone.view.Constants;
import ir.mrbenyamin.firestone.view.Handler;
import ir.mrbenyamin.firestone.view.assets.Assets;
import ir.mrbenyamin.firestone.view.graphicutils.BObject;
import ir.mrbenyamin.firestone.view.graphicutils.BObjectsManager;
import ir.mrbenyamin.firestone.view.graphicutils.button.BTextButton;
import ir.mrbenyamin.firestone.view.graphicutils.label.BLabel;
import ir.mrbenyamin.firestone.view.graphicutils.textbox.BTextBox;

import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;

public class CreateDeckPage {
    private BTextBox deckName;
    private ArrayList<BTextButton> heroesButton;
    private BObjectsManager manager;
    private Handler handler;
    private BTextButton done;
    private BLabel deckNameLabel, selectHeroLabel;

    CreateDeckPage(Handler handler) {
        this.handler = handler;
        heroesButton = new ArrayList<>();
        manager = new BObjectsManager(handler);
        int i = 0;
        deckNameLabel = new BLabel("Deck name: ", 605, 80, 100, 20, 12);
        deckNameLabel.setStringColor(Constants.WARNING_FONT_COLOR);
        selectHeroLabel = new BLabel("Select Hero: ", 605, 130, 100, 20, 12);
        selectHeroLabel.setStringColor(Constants.WARNING_FONT_COLOR);
        deckName = new BTextBox(605, 90, 100, 20, () -> {
            handler.getGame().getKeyBoardManager().setBObjectManager(manager);
            for (BObject o :
                    manager.getObjects()) {
                if (o instanceof BTextBox){
                    ((BTextBox) o).setUnderType(false);
                }
            }
            deckName.setUnderType(true);
            deckName.setFont(new Font("Arial", Font.PLAIN, 12));
        });
        for (Hero hero:
                handler.getGame().getSignedPlayer().getAvailableHeroes()) {
            heroesButton.add(new BTextButton(hero.getName(), 605, 140 + i * 20, 100, 20, () -> {
                if (handler.getGame().getSignedPlayer().getDecks().contains(new Deck(deckName.getTypedString(), hero, new ArrayList<>()))) {
                    handler.getGame().getCollectionsState().getPageManager().getMessage().setMessage(
                            "Change name"
                    );
                    handler.getGame().getCollectionsState().getPageManager().getMessage().setBackgroundColor(Constants.WARNING_COLOR);
                    handler.getGame().getCollectionsState().getPageManager().getMessage().setImage(Assets.warning);
                    handler.getGame().getCollectionsState().getPageManager().getMessage().setShow(true);
                } else {
                    handler.getGame().getSignedPlayer().getDecks().add(new Deck(deckName.getTypedString(), hero, new ArrayList<>()));
                    try {
                        MyLogger.writeLog(handler.getGame().getSignedPlayer().getUsername() + "-" + handler.getGame().getSignedPlayer().getStringID(), "Created new deck with name " + deckName.getTypedString(), Level.INFO);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    handler.getGame().getCollectionsState().setUpDecks(handler.getGame().getSignedPlayer());
                    handler.getGame().getCollectionsState().setCreateDeckPage(null);
                }
            }));
            i++;
        }

        done = new BTextButton("Back", 605, 450, 100, 30, () -> handler.getGame().getCollectionsState().setCreateDeckPage(null));

        for (BTextButton b :
                heroesButton) {
            manager.addObject(b);
        }
        manager.addObject(deckName);
        manager.addObject(deckNameLabel);
        manager.addObject(selectHeroLabel);
        manager.addObject(done);
    }

    public void tick(){
        manager.tick();
    }
    public void render(Graphics2D g){
        manager.render(g);
    }

    public BTextBox getDeckName() {
        return deckName;
    }

    public void setDeckName(BTextBox deckName) {
        this.deckName = deckName;
    }

    public ArrayList<BTextButton> getHeroesButton() {
        return heroesButton;
    }

    public void setHeroesButton(ArrayList<BTextButton> heroesButton) {
        this.heroesButton = heroesButton;
    }

    public BObjectsManager getManager() {
        return manager;
    }

    public void setManager(BObjectsManager manager) {
        this.manager = manager;
    }

    public Handler getHandler() {
        return handler;
    }

    public void setHandler(Handler handler) {
        this.handler = handler;
    }
}
