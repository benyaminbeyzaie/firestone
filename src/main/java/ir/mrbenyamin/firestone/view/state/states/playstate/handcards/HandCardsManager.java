package ir.mrbenyamin.firestone.view.state.states.playstate.handcards;

import ir.mrbenyamin.firestone.heroes.Hero;
import ir.mrbenyamin.firestone.play.Play;
import ir.mrbenyamin.firestone.view.Constants;
import ir.mrbenyamin.firestone.view.Handler;
import ir.mrbenyamin.firestone.view.assets.Assets;
import ir.mrbenyamin.firestone.view.graphicutils.BObjectsManager;
import ir.mrbenyamin.firestone.view.graphicutils.ClickListener;
import ir.mrbenyamin.firestone.view.graphicutils.button.BButton;
import ir.mrbenyamin.firestone.view.graphicutils.button.BTextButton;

import java.awt.*;

public abstract class HandCardsManager {
     Handler handler;
     BObjectsManager manager;
     Play play;

    HandCardsManager(Handler handler, Play play) {
        this.handler = handler;
        this.play = play;
        manager = new BObjectsManager(handler);
        //addHero();
    }

    public abstract void updateCardViews();

    public abstract void addHero(Hero hero);

    public void tick(){
        manager.tick();
    }

    public void render(Graphics2D g){

        manager.render(g);

    }

    public Handler getHandler() {
        return handler;
    }

    public void setHandler(Handler handler) {
        this.handler = handler;
    }

    public BObjectsManager getManager() {
        return manager;
    }

    public void setManager(BObjectsManager manager) {
        this.manager = manager;
    }

    public Play getPlay() {
        return play;
    }

    public void setPlay(Play play) {
        this.play = play;
    }
}
