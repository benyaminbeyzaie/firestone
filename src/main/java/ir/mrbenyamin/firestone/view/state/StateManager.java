package ir.mrbenyamin.firestone.view.state;

import ir.mrbenyamin.firestone.log.MyLogger;
import ir.mrbenyamin.firestone.view.display.MouseManager;
import ir.mrbenyamin.firestone.view.graphicutils.BObjectsManager;
import ir.mrbenyamin.firestone.view.state.states.MainState;

import java.io.IOException;
import java.util.logging.Level;

public class StateManager {
    private static State currentState;

    public static State getCurrentState() {
        return currentState;
    }

    public static void setCurrentState(State currentState) {
        StateManager.currentState = currentState;
        currentState.handler.getGame().getMouseManager().setBObjectManager(currentState.getManager());
        currentState.handler.getGame().getKeyBoardManager().setBObjectManager(currentState.getManager());
        try {
            if (currentState.getName().equals("Sign in")){
                MyLogger.writeLog(MainState.LOG_NAME,  "Player logged out", Level.INFO);
            }else {
                MyLogger.writeLog(MainState.LOG_NAME,  "Moved to " + currentState.getName(), Level.INFO);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
