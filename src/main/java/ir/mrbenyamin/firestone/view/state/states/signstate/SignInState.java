package ir.mrbenyamin.firestone.view.state.states.signstate;

import ir.mrbenyamin.firestone.player.sign.SignIn;
import ir.mrbenyamin.firestone.view.Constants;
import ir.mrbenyamin.firestone.view.Handler;
import ir.mrbenyamin.firestone.view.assets.Assets;
import ir.mrbenyamin.firestone.view.graphicutils.BObject;
import ir.mrbenyamin.firestone.view.graphicutils.BObjectsManager;
import ir.mrbenyamin.firestone.view.graphicutils.ClickListener;
import ir.mrbenyamin.firestone.view.graphicutils.button.BTextButton;
import ir.mrbenyamin.firestone.view.graphicutils.label.BLabel;
import ir.mrbenyamin.firestone.view.graphicutils.message.BWarningMessage;
import ir.mrbenyamin.firestone.view.graphicutils.textbox.BPasswordTextBox;
import ir.mrbenyamin.firestone.view.graphicutils.textbox.BTextBox;
import ir.mrbenyamin.firestone.view.state.StateManager;

import java.awt.*;
import java.io.IOException;

public class SignInState extends SignState {
    private static SignInState signInState;

    private BTextButton goToSignUpState;
    private BObjectsManager manager;
    private BTextBox userNameTextBox;
    private BPasswordTextBox passwordTextBox;
    private BLabel userLabel, passLabel;
    private BTextButton signInButton;
    private BWarningMessage warning;
    private BTextButton exit;

    private SignIn signIn;

    public static SignInState getInstance(Handler handler){
        if (signInState == null) signInState = new SignInState(handler);
        return signInState;
    }
    private SignInState(Handler handler) {
        super(handler);
        setName("Sign in");
        manager = new BObjectsManager(handler);
        signIn = SignIn.getInstance();


        int widthIn4 = (Constants.STANDARD_STATES_WIDTH / 4);
        goToSignUpState =  new BTextButton("Sign up", 3 * widthIn4 - 125, 260, 120, 40, () -> {
            handler.getGame().getSignUpState().clearDate();
            StateManager.setCurrentState(handler.getGame().getSignUpState());
        });
        // goToSignUpState.setButtonColor(Constants.BUTTON_COLOR_2);

        userLabel = new BLabel("Username: ", 439, 75, 100, 555, 12);
        userNameTextBox = new BTextBox(3 * widthIn4 - 125,  80, 250, 40, () -> {
            for (BObject o :
                    manager.getObjects()) {
                if (o instanceof BTextBox){
                    ((BTextBox) o).setUnderType(false);
                }
            }
            userNameTextBox.setUnderType(true);
        });
        passLabel = new BLabel("Password: ", 439, 145, 100, 555, 12);
        passwordTextBox = new BPasswordTextBox(3 * widthIn4 - 125, 150, 250, 40, () -> {
            for (BObject o :
                    manager.getObjects()) {
                if (o instanceof BTextBox){
                    ((BTextBox) o).setUnderType(false);
                }
            }
            passwordTextBox.setUnderType(true);
        });

        signInButton = new BTextButton("Sign in", 3 * widthIn4 - 125, 200, 250, 50, () -> {
            try {
                callSignIn(userNameTextBox.getTypedString(), passwordTextBox.getTypedString());
                //callSignIn("pp", "pp");
            } catch (IOException e) {
                e.printStackTrace();
            }

        });
        signInButton.setButtonTextSize(20);
        signInButton.setButtonColor(Constants.BUTTON_COLOR_2);

        exit =  new BTextButton("Exit", 3 * widthIn4 + 5, 260, 120, 40, () -> System.exit(0));
        exit.setButtonColor(Constants.BUTTON_COLOR_4);
        warning = new BWarningMessage("", Assets.warning, 3 * widthIn4 - 125, 300, 250, 30, 2000000000l);


        manager.addObject(userNameTextBox);
        manager.addObject(passwordTextBox);
        manager.addObject(userLabel);
        manager.addObject(passLabel);
        manager.addObject(signInButton);
        manager.addObject(warning);
        manager.addObject(exit);
        manager.addObject(goToSignUpState);

    }

    @Override
    public void clearDate() {
        this.getUserNameTextBox().setTypedString("");
        this.getPasswordTextBox().setTypedString("");
        this.getPasswordTextBox().setRenderedPass("");
    }

    private void callSignIn(String user, String pass) throws IOException {
        int temp = signIn.signIn(user, pass);
        if (temp == 0){
            // user is empty
            warning.setMessage("Username is empty!");
            warning.setImage(Assets.warning);
            warning.setBackgroundColor(Constants.WARNING_COLOR);
            warning.setShow(true);
        }else if(temp == 1){
            // pass is empty
            warning.setMessage("Password is empty!");
            warning.setImage(Assets.warning);
            warning.setBackgroundColor(Constants.WARNING_COLOR);
            warning.setShow(true);
        }else if(temp == 2){
            // user dose not exists
            warning.setMessage("This user dose not exists!");
            warning.setBackgroundColor(Constants.WARNING_COLOR);
            warning.setImage(Assets.error);
            warning.setShow(true);
        }else if(temp == 3){
            // pass is wrong
            warning.setMessage("Password is incorrect!");
            warning.setBackgroundColor(Constants.WARNING_COLOR);
            warning.setImage(Assets.error);
            warning.setShow(true);
        }else if(temp == 4){

            handler.getGame().setSignedPlayer(signIn.getSignedPlayer());
            handler.getGame().setUpMustSignedStates();
            StateManager.setCurrentState(handler.getGame().getMainState());
            handler.getGame().playBackSound("all");
        }
    }


    @Override
    public void tick() {
        manager.tick();
    }

    @Override
    public void render(Graphics2D g) {
        renderBack(g);
        manager.render(g);
    }

    public BObjectsManager getManager() {
        return manager;
    }

    public void setManager(BObjectsManager manager) {
        this.manager = manager;
    }

    public static SignInState getSignInState() {
        return signInState;
    }

    public static void setSignInState(SignInState signInState) {
        SignInState.signInState = signInState;
    }

    public BTextButton getGoToSignUpState() {
        return goToSignUpState;
    }

    public void setGoToSignUpState(BTextButton goToSignUpState) {
        this.goToSignUpState = goToSignUpState;
    }

    public BTextBox getUserNameTextBox() {
        return userNameTextBox;
    }

    public void setUserNameTextBox(BTextBox userNameTextBox) {
        this.userNameTextBox = userNameTextBox;
    }

    public BPasswordTextBox getPasswordTextBox() {
        return passwordTextBox;
    }

    public void setPasswordTextBox(BPasswordTextBox passwordTextBox) {
        this.passwordTextBox = passwordTextBox;
    }

    public BLabel getUserLabel() {
        return userLabel;
    }

    public void setUserLabel(BLabel userLabel) {
        this.userLabel = userLabel;
    }

    public BLabel getPassLabel() {
        return passLabel;
    }

    public void setPassLabel(BLabel passLabel) {
        this.passLabel = passLabel;
    }

    public BTextButton getSignInButton() {
        return signInButton;
    }

    public void setSignInButton(BTextButton signInButton) {
        this.signInButton = signInButton;
    }

    public BWarningMessage getWarning() {
        return warning;
    }

    public void setWarning(BWarningMessage warning) {
        this.warning = warning;
    }

    public BTextButton getExit() {
        return exit;
    }

    public void setExit(BTextButton exit) {
        this.exit = exit;
    }

    public SignIn getSignIn() {
        return signIn;
    }

    public void setSignIn(SignIn signIn) {
        this.signIn = signIn;
    }
}
