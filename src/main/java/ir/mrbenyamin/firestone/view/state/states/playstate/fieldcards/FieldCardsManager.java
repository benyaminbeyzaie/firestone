package ir.mrbenyamin.firestone.view.state.states.playstate.fieldcards;

import ir.mrbenyamin.firestone.card.Minion;
import ir.mrbenyamin.firestone.play.Play;
import ir.mrbenyamin.firestone.player.Player;
import ir.mrbenyamin.firestone.view.Handler;
import ir.mrbenyamin.firestone.view.graphicutils.BObjectsManager;

import java.awt.*;
import java.io.IOException;

public abstract class FieldCardsManager {
     Handler handler;
     BObjectsManager manager;
     Play play;

    FieldCardsManager(Handler handler, Play play) {
        this.handler = handler;
        this.play = play;
        manager = new BObjectsManager(handler);
    }

    public void executeNeedSelectCardBattlecryOrSpell(Player player, int finalI){
        if (play.getSpellActionCallerID() != -1){
            try {
                play.actionSpellWithID(play.getSpellActionCallerID(),
                        player.getOnFieldMinions().get(finalI));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (play.getBattlecryActionCallerID() != -1){
            // execute battlecry
            try {
                play.battlecryWithCallerID(play.getBattlecryActionCallerID(),
                        player.getOnFieldMinions().get(finalI));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        play.setMustSelectEnemyCard(false);
        play.setMustSelectFriendlyCard(false);
    }

    public abstract void updateCardViews();

    public void tick(){
        manager.tick();
    }

    public void render(Graphics2D g){
        manager.render(g);
    }

    public Handler getHandler() {
        return handler;
    }

    public void setHandler(Handler handler) {
        this.handler = handler;
    }

    public BObjectsManager getManager() {
        return manager;
    }

    public void setManager(BObjectsManager manager) {
        this.manager = manager;
    }

    public Play getPlay() {
        return play;
    }

    public void setPlay(Play play) {
        this.play = play;
    }
}
