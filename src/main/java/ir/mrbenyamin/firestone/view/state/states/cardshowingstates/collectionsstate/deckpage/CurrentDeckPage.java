package ir.mrbenyamin.firestone.view.state.states.cardshowingstates.collectionsstate.deckpage;

import ir.mrbenyamin.firestone.card.Deck;
import ir.mrbenyamin.firestone.log.MyLogger;
import ir.mrbenyamin.firestone.util.HibernateUtil;
import ir.mrbenyamin.firestone.view.Constants;
import ir.mrbenyamin.firestone.view.Handler;
import ir.mrbenyamin.firestone.view.graphicutils.BObjectsManager;
import ir.mrbenyamin.firestone.view.graphicutils.ClickListener;
import ir.mrbenyamin.firestone.view.graphicutils.button.BTextButton;
import ir.mrbenyamin.firestone.view.state.states.MainState;
import org.hibernate.Session;

import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;

public class CurrentDeckPage {
    private Handler handler;
    private ArrayList<BTextButton> cardButtons;
    private BObjectsManager manager;
    private Deck selectedDeck;
    private BTextButton doneButton, editButton;

    public CurrentDeckPage(Handler handler) {
        this.handler = handler;
        selectedDeck = handler.getGame().getCollectionsState().getSelectedDeck();

        manager = new BObjectsManager(handler);
        doneButton = new BTextButton("Done", 605, 460, 100, 20, () -> {
            handler.getGame().getCollectionsState().setSelectedDeck(null);
            handler.getGame().getCollectionsState().setCurrentDeckPage(null);
            handler.getGame().getSignedPlayer().saveSignedPlayerInfo();
            handler.getGame().getCollectionsState().setUpDecks(handler.getGame().getSignedPlayer());
        });
        editButton = new BTextButton("Edit", 605, 435, 100, 20, () -> handler.getGame().getCollectionsState().setEditDeckPage(new EditDeckPage(handler)));
        manager.addObject(editButton);
        manager.addObject(doneButton);

        cardButtons = new ArrayList<>();

        for (int i = 0; i < selectedDeck.getCards().size(); i++) {
            int finalI = i;
            BTextButton temp = new BTextButton(selectedDeck.getCards().get(i).getName(), 600, i * 20 + 70, 110, 18, () -> {
                try {
                    MyLogger.writeLog(MainState.LOG_NAME,  "Player removed " + selectedDeck.getCards().get(finalI).getName() + " from " + selectedDeck.getName(), Level.INFO);
                    handler.getGame().getSignedPlayer().saveSignedPlayerInfo();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (selectedDeck.getName().equals(handler.getGame().getSignedPlayer().getCurrentDeck().getName())) {
                    handler.getGame().getSignedPlayer().setCurrentDeck(null);
                }
                selectedDeck.resetStatus();
                selectedDeck.getCards().remove(finalI);


                handler.getGame().getCollectionsState().setCurrentDeckPage(new CurrentDeckPage(handler));
            });
            temp.setButtonTextSize(10);
            temp.setButtonColor(new Color(0xA85E00));
            temp.setButtonHoverColor(new Color(0xDE3512));
            cardButtons.add(temp);
            manager.addObject(temp);
        }
    }

    private void calculateDecks(){

    }

    public void tick(){
        manager.tick();
    }

    public void render(Graphics2D g){
        g.setColor(Constants.LABEL_WHITE);
        g.drawString(selectedDeck.getHero().getName(), 630 , 50);
        manager.render(g);

    }

    public Handler getHandler() {
        return handler;
    }

    public void setHandler(Handler handler) {
        this.handler = handler;
    }

    public ArrayList<BTextButton> getDeckButtons() {
        return cardButtons;
    }

    public void setDeckButtons(ArrayList<BTextButton> deckButtons) {
        this.cardButtons = deckButtons;
    }

    public BObjectsManager getManager() {
        return manager;
    }

    public void setManager(BObjectsManager manager) {
        this.manager = manager;
    }
}
