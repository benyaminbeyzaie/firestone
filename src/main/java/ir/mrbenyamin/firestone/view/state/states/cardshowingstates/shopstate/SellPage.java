package ir.mrbenyamin.firestone.view.state.states.cardshowingstates.shopstate;

import ir.mrbenyamin.firestone.card.Card;
import ir.mrbenyamin.firestone.shop.Shop;
import ir.mrbenyamin.firestone.view.Constants;
import ir.mrbenyamin.firestone.view.Handler;
import ir.mrbenyamin.firestone.view.assets.Assets;
import ir.mrbenyamin.firestone.view.graphicutils.BObjectsManager;
import ir.mrbenyamin.firestone.view.graphicutils.ClickListener;
import ir.mrbenyamin.firestone.view.graphicutils.button.BTextButton;

import java.awt.*;
import java.io.IOException;

public class SellPage {
    private Card card;
    private BTextButton button;
    private Handler handler;
    private BObjectsManager manager;
    private Shop shop;

    public SellPage(Card card, Handler handler) {
        this.card = card;
        this.handler = handler;
        shop = new Shop();
        manager = new BObjectsManager(handler);
        if (handler.getGame().getSignedPlayer().getAllAvailableCards().contains(card)){
            // sell
            button = new BTextButton("Sell", 600, 450, 100, 20, () -> {
                try {
                    sellCard(card);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        }else {
            // buy
            button = new BTextButton("Buy", 605, 450, 100, 20, () -> {
                try {
                    buyCard(card);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        }
        manager.addObject(button);
    }

    private void buyCard(Card card) throws IOException {
        int temp = shop.buy(handler.getGame().getSignedPlayer(), card.getName());
        if (temp == 0){
            handler.getGame().getShopState().getCardPageManager().getMessage().setMessage("Card is invalid!");
            handler.getGame().getShopState().getCardPageManager().getMessage().setBackgroundColor(Constants.WARNING_COLOR);
            handler.getGame().getShopState().getCardPageManager().getMessage().setImage(Assets.warning);
            handler.getGame().getShopState().getCardPageManager().getMessage().setShow(true);
        }else if (temp == 1){
            handler.getGame().getShopState().getCardPageManager().getMessage().setMessage("You already have this card!");
            handler.getGame().getShopState().getCardPageManager().getMessage().setBackgroundColor(Constants.WARNING_COLOR);
            handler.getGame().getShopState().getCardPageManager().getMessage().setImage(Assets.warning);
            handler.getGame().getShopState().getCardPageManager().getMessage().setShow(true);
        }else if (temp == 2){
            handler.getGame().getShopState().getCardPageManager().getMessage().setMessage("you dont have gems!");
            handler.getGame().getShopState().getCardPageManager().getMessage().setBackgroundColor(Constants.WARNING_COLOR);
            handler.getGame().getShopState().getCardPageManager().getMessage().setImage(Assets.warning);
            handler.getGame().getShopState().getCardPageManager().getMessage().setShow(true);
        }else if (temp == 3){
            handler.getGame().getShopState().getCardPageManager().getMessage().setMessage("You buy " + card.getName() + " successfully! Your gem now: " +
                    handler.getGame().getSignedPlayer().getGems());
            handler.getGame().getShopState().getCardPageManager().getMessage().setBackgroundColor(Constants.BUTTON_COLOR_3);
            handler.getGame().getShopState().getCardPageManager().getMessage().setImage(null);
            handler.getGame().getShopState().getCardPageManager().getMessage().setShow(true);
        }
    }

    private void sellCard(Card card) throws IOException {
        int temp = shop.sell(handler.getGame().getSignedPlayer(), card.getName());
        if (temp == 0){
            handler.getGame().getShopState().getCardPageManager().getMessage().setMessage("Card is invalid!");
            handler.getGame().getShopState().getCardPageManager().getMessage().setBackgroundColor(Constants.WARNING_COLOR);
            handler.getGame().getShopState().getCardPageManager().getMessage().setImage(Assets.warning);
            handler.getGame().getShopState().getCardPageManager().getMessage().setShow(true);
        }else if (temp == 1){
            handler.getGame().getShopState().getCardPageManager().getMessage().setMessage("You don't have this");
            handler.getGame().getShopState().getCardPageManager().getMessage().setBackgroundColor(Constants.WARNING_COLOR);
            handler.getGame().getShopState().getCardPageManager().getMessage().setImage(Assets.warning);
            handler.getGame().getShopState().getCardPageManager().getMessage().setShow(true);
        }else if (temp == 2){
            handler.getGame().getShopState().getCardPageManager().getMessage().setMessage("You already have this card on your battle deck! Remove it first!");
            handler.getGame().getShopState().getCardPageManager().getMessage().setBackgroundColor(Constants.WARNING_COLOR);
            handler.getGame().getShopState().getCardPageManager().getMessage().setImage(Assets.warning);
            handler.getGame().getShopState().getCardPageManager().getMessage().setShow(true);
        }else if (temp == 3){
            handler.getGame().getShopState().getCardPageManager().getMessage().setMessage("You sold " + card.getName() + " Successfully. Your gems has raised " + card.getPrice());
            handler.getGame().getShopState().getCardPageManager().getMessage().setBackgroundColor(Constants.BUTTON_COLOR_3);
            handler.getGame().getShopState().getCardPageManager().getMessage().setImage(null);
            handler.getGame().getShopState().getCardPageManager().getMessage().setShow(true);
        }
    }

    public void tick(){
        manager.tick();
    }

    public void render(Graphics2D g){
        g.drawString("Price: " + card.getPrice() , 605, 95);
        g.drawImage(Assets.getImage(card.getName())[0],600, 120, Constants.CARD_VIEW_WIDTH, Constants.CARD_VIEW_HEIGHTS, null);
        manager.render(g);
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    public BTextButton getButton() {
        return button;
    }

    public void setButton(BTextButton button) {
        this.button = button;
    }

    public Handler getHandler() {
        return handler;
    }

    public void setHandler(Handler handler) {
        this.handler = handler;
    }

    public BObjectsManager getManager() {
        return manager;
    }

    public void setManager(BObjectsManager manager) {
        this.manager = manager;
    }

    public Shop getShop() {
        return shop;
    }

    public void setShop(Shop shop) {
        this.shop = shop;
    }
}
