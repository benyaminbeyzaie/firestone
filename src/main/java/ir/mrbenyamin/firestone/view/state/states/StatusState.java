package ir.mrbenyamin.firestone.view.state.states;

import ir.mrbenyamin.firestone.card.Deck;
import ir.mrbenyamin.firestone.player.Player;
import ir.mrbenyamin.firestone.view.Constants;
import ir.mrbenyamin.firestone.view.Handler;
import ir.mrbenyamin.firestone.view.assets.Assets;
import ir.mrbenyamin.firestone.view.graphicutils.BObjectsManager;
import ir.mrbenyamin.firestone.view.graphicutils.ClickListener;
import ir.mrbenyamin.firestone.view.graphicutils.button.BImageButton;
import ir.mrbenyamin.firestone.view.state.State;
import ir.mrbenyamin.firestone.view.state.StateManager;


import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class StatusState extends State {
    private Player signedPlayer;
    private List<Deck> decks;

    public StatusState(Handler handler) {
        super(handler);
        setName("Status");
        this.signedPlayer = handler.getGame().getSignedPlayer();
        decks = handler.getGame().getSignedPlayer().getDecks();
        manager = new BObjectsManager(handler);
        BImageButton backButton = new BImageButton(10, 10, Assets.back[0].getWidth(), Assets.back[0].getHeight(), Assets.back, () -> StateManager.setCurrentState(handler.getGame().getMainState()));

        manager.addObject(backButton);
    }

    @Override
    public void tick() {

    }

    @Override
    public void render(Graphics2D g) {
        manager.render(g);
        g.drawString("Deck name", 30, 40);
        g.drawLine(100, 0, 100, Constants.STANDARD_STATES_HEIGHT);
        g.drawString("wins/all game", 110, 40);
        g.drawLine(200, 0, 200, Constants.STANDARD_STATES_HEIGHT);
        g.drawString("wins", 220, 40);
        g.drawLine(260, 0, 260, Constants.STANDARD_STATES_HEIGHT);
        g.drawString("all Game", 270, 40);
        g.drawLine(330, 0, 330, Constants.STANDARD_STATES_HEIGHT);
        g.drawString("Mana/card", 350, 40);
        g.drawLine(430, 0, 430, Constants.STANDARD_STATES_HEIGHT);
        g.drawString("hero name", 460, 40);
        g.drawLine(550, 0, 550, Constants.STANDARD_STATES_HEIGHT);
        g.drawString("most used card", 580, 40);
        g.drawImage(Assets.line, 0 , 45, null);
        for (int i = 0; i < decks.size(); i++) {
            g.drawString(decks.get(i).getName(), 35 , 80 + i * 20);
        }
        for (int i = 0; i < decks.size(); i++) {
            g.drawString(decks.get(i).getWinToAll() + "", 115 , 80 + i * 20);
        }
        for (int i = 0; i < decks.size(); i++) {
            g.drawString(decks.get(i).getWinsCount() + "", 225 , 80 + i * 20);
        }
        for (int i = 0; i < decks.size(); i++) {
            g.drawString(decks.get(i).getAllGamesCount() + "", 275 , 80 + i * 20);
        }
        for (int i = 0; i < decks.size(); i++) {
            String s = String.format("%.2f", decks.get(i).getManaPerCard());
            g.drawString(s, 355 , 80 + i * 20);
        }
        for (int i = 0; i < decks.size(); i++) {
            System.out.println(decks.get(i).getHero());
            g.drawString(decks.get(i).getHero().getName() + "", 465 , 80 + i * 20);
        }
        for (int i = 0; i < decks.size(); i++) {
            if (decks.get(i).getMostUsedCard() != null){
                g.drawString(decks.get(i).getMostUsedCard().getName()+ "", 585 , 80 + i * 20);
            }
        }
    }

    public Player getSignedPlayer() {
        return signedPlayer;
    }

    public void setSignedPlayer(Player signedPlayer) {
        this.signedPlayer = signedPlayer;
    }

    public List<Deck> getDecks() {
        return decks;
    }

    public void setDecks(ArrayList<Deck> decks) {
        this.decks = decks;
    }
}
