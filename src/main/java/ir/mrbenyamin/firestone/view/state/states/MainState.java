package ir.mrbenyamin.firestone.view.state.states;

import ir.mrbenyamin.firestone.card.Deck;
import ir.mrbenyamin.firestone.play.Play;
import ir.mrbenyamin.firestone.player.Player;
import ir.mrbenyamin.firestone.view.Constants;
import ir.mrbenyamin.firestone.view.Game;
import ir.mrbenyamin.firestone.view.Handler;
import ir.mrbenyamin.firestone.view.assets.Assets;
import ir.mrbenyamin.firestone.view.graphicutils.BObjectsManager;
import ir.mrbenyamin.firestone.view.graphicutils.button.BImageButton;
import ir.mrbenyamin.firestone.view.graphicutils.button.BTextButton;
import ir.mrbenyamin.firestone.view.graphicutils.message.BWarningMessage;
import ir.mrbenyamin.firestone.view.state.State;
import ir.mrbenyamin.firestone.view.state.StateManager;
import ir.mrbenyamin.firestone.view.state.states.cardshowingstates.collectionsstate.cardpage.PageType;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Collections;

public class MainState extends State {
    private BImageButton playButton, collectionsButton, shopButton, settingButton,
            exitButton, logOutButton, statusButton;
    private BTextButton defaultMode, deckReaderMode, playWithComputerMode;
    private BufferedImage playerName, gemBackground;
    private Player signedPlayer;
    private BWarningMessage warningMessage;
    public static String LOG_NAME;
    private int mode = 1;



    public MainState(Handler handler) {
        super(handler);
        setName("Main");
        signedPlayer = handler.getGame().getSignedPlayer();
        LOG_NAME = signedPlayer.getUsername() + "-" + signedPlayer.getStringID();
        manager = new BObjectsManager(handler);
        playerName = Assets.playerNameBack;
        gemBackground = Assets.gemBack;
        int BigButtonsX = Constants.STANDARD_STATES_WIDTH / 2 - Assets.playButton[0].getWidth() / 2;
        warningMessage = new BWarningMessage("", Assets.warning, Constants.STANDARD_STATES_WIDTH / 2 - 200, Constants.STANDARD_STATES_HEIGHT / 2 - 15, 400, 30, 3000000000L);


        defaultMode = new BTextButton("Default", 70, 30, 40, 10, () -> {
            mode = 0; // default
        });
        deckReaderMode = new BTextButton("Deck Reader", 120, 30, 50, 10, () -> {
            mode = 1; // deck
        });
        playWithComputerMode = new BTextButton("Play with computer", 180, 30, 80, 10, () -> {
            mode = 2; // AR
        });
        defaultMode.setButtonTextSize(8);
        deckReaderMode.setButtonTextSize(8);
        playWithComputerMode.setButtonTextSize(8);



        playButton = new BImageButton(BigButtonsX, 100, Assets.playButton[0].getWidth(),
                Assets.playButton[0].getHeight(), Assets.playButton, () -> {
                    // Play State
                    try {
                        try {
                            handler.getGame().getPlayState().setPlay(new Play(handler, signedPlayer, mode));
                            handler.getGame().getPlayState().createManagers();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        int temp = handler.getGame().getPlayState().getPlay().setDefaults();
                        handler.getGame().getPlayState().setSelectedCardX(1001);
                        if (temp == 1){
                            handler.getGame().getPlayState().updateCardViews();
                            handler.getGame().getDisplay().changeDisplaySize(Constants.PLAY_WIDTH , Constants.PLAY_HEIGHT);
                            Game.stopBackSound();
                            handler.getGame().playBackSound("play");
                            StateManager.setCurrentState(handler.getGame().getPlayState());
                        }else {
                            handler.getGame().getMainState().getWarningMessage().setMessage(
                                    "Select a deck first"
                            );
                            handler.getGame().getMainState().getWarningMessage().setBackgroundColor(Constants.WARNING_COLOR);
                            handler.getGame().getMainState().getWarningMessage().setImage(Assets.warning);
                            handler.getGame().getMainState().getWarningMessage().setShow(true);
                            callCollectionsState();

                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                });

        collectionsButton = new BImageButton(BigButtonsX, 175, Assets.collectionsButton[0].getWidth(),
                Assets.collectionsButton[0].getHeight(), Assets.collectionsButton, () -> {
                    // Collections State
                   callCollectionsState();
                });

        shopButton = new BImageButton(BigButtonsX, 250, Assets.shopButton[0].getWidth(),
                Assets.shopButton[0].getHeight(), Assets.shopButton, () -> {
                    // Shop State
                    try {
                        handler.getGame().getShopState().setUpPages(signedPlayer, -1, PageType.SHOP);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    StateManager.setCurrentState(handler.getGame().getShopState());
                });

        settingButton = new BImageButton(BigButtonsX, 325, Assets.settingButton[0].getWidth(),
                Assets.settingButton[0].getHeight(), Assets.settingButton, () -> {
                    // Setting State
                    StateManager.setCurrentState(handler.getGame().getSettingState());
                });

        exitButton = new BImageButton(Constants.STANDARD_STATES_WIDTH / 2 - Assets.logOutButton[0].getWidth() / 2, 450, Assets.exitButton[0].getWidth(),
                Assets.exitButton[0].getHeight(), Assets.exitButton, () -> {
                    //  Exit
                    System.exit(0);
                });

        logOutButton = new BImageButton( Constants.STANDARD_STATES_WIDTH / 2 - Assets.logOutButton[0].getWidth() + 8 + Assets.exitButton[0].getWidth(), 407, Assets.logOutButton[0].getWidth(),
                Assets.logOutButton[0].getHeight(), Assets.logOutButton, () -> {
                    Game.stopBackSound();
                    handler.getGame().getSignInState().clearDate();
                    StateManager.setCurrentState(handler.getGame().getSignInState());
                });

        statusButton = new BImageButton( BigButtonsX, 407, Assets.statusButton[0].getWidth(),
                Assets.statusButton[0].getHeight(), Assets.statusButton, () -> {
                    for (Deck d :
                            signedPlayer.getDecks()) {
                        d.calculate();
                    }
                    Collections.sort(handler.getGame().getStatusState().getDecks(), Collections.reverseOrder());
                    StateManager.setCurrentState(handler.getGame().getStatusState());
                });

        manager.addObject(playButton);
        manager.addObject(collectionsButton);
        manager.addObject(shopButton);
        manager.addObject(settingButton);
        manager.addObject(exitButton);
        manager.addObject(logOutButton);
        manager.addObject(warningMessage);
        manager.addObject(statusButton);
        manager.addObject(defaultMode);
        manager.addObject(playWithComputerMode);
        manager.addObject(deckReaderMode);
    }

    private void callCollectionsState() {
        handler.getGame().getCollectionsState().setCurrentDeckPage(null);
        handler.getGame().getCollectionsState().setSelectedDeck(null);
        try {
            handler.getGame().getCollectionsState().setUpPages(signedPlayer, -1, PageType.COLLECTIONS);
            handler.getGame().getCollectionsState().setUpDecks(handler.getGame().getSignedPlayer());
        } catch (IOException e) {
            e.printStackTrace();
        }
        StateManager.setCurrentState(handler.getGame().getCollectionsState());
    }

    @Override
    public void tick() {
        manager.tick();
    }

    @Override
    public void render(Graphics2D g) {
        g.drawImage(Assets.mainBackground, 0, 0, null);
        drawHeader(g);
        manager.render(g);
        g.setColor(Color.black);
        g.setFont(new Font("Arial", Font.BOLD, 12));
        g.drawString("Current Mode: " + mode, 590, 40);

    }

    private void drawHeader(Graphics2D g) {
        g.drawImage(playerName, 80, 60, null);
        g.drawImage(gemBackground, 545 , 60, null);
        g.setColor(Constants.HEADER_TEXT);
        g.setFont(new Font("Arial", Font.BOLD, 16));
        g.drawString(signedPlayer.getUsername(), 110, 80);
        g.drawString(signedPlayer.getGems() + "", 570, 80);
    }

    public Player getSignedPlayer() {
        return signedPlayer;
    }

    public void setSignedPlayer(Player signedPlayer) {
        this.signedPlayer = signedPlayer;
    }

    public BObjectsManager getManager() {
        return manager;
    }

    public void setManager(BObjectsManager manager) {
        this.manager = manager;
    }

    public BImageButton getPlayButton() {
        return playButton;
    }

    public void setPlayButton(BImageButton playButton) {
        this.playButton = playButton;
    }

    public BImageButton getCollectionsButton() {
        return collectionsButton;
    }

    public void setCollectionsButton(BImageButton collectionsButton) {
        this.collectionsButton = collectionsButton;
    }

    public BImageButton getShopButton() {
        return shopButton;
    }

    public void setShopButton(BImageButton shopButton) {
        this.shopButton = shopButton;
    }

    public BImageButton getSettingButton() {
        return settingButton;
    }

    public void setSettingButton(BImageButton settingButton) {
        this.settingButton = settingButton;
    }

    public BImageButton getExitButton() {
        return exitButton;
    }

    public void setExitButton(BImageButton exitButton) {
        this.exitButton = exitButton;
    }

    public BImageButton getLogOutButton() {
        return logOutButton;
    }

    public void setLogOutButton(BImageButton logOutButton) {
        this.logOutButton = logOutButton;
    }

    public BufferedImage getPlayerName() {
        return playerName;
    }

    public void setPlayerName(BufferedImage playerName) {
        this.playerName = playerName;
    }

    public BufferedImage getGemBackground() {
        return gemBackground;
    }

    public void setGemBackground(BufferedImage gemBackground) {
        this.gemBackground = gemBackground;
    }

    public BWarningMessage getWarningMessage() {
        return warningMessage;
    }

    public void setWarningMessage(BWarningMessage warningMessage) {
        this.warningMessage = warningMessage;
    }
}
