package ir.mrbenyamin.firestone.view.state.states.playstate.handcards;

import ir.mrbenyamin.firestone.card.Minion;
import ir.mrbenyamin.firestone.card.Weapon;
import ir.mrbenyamin.firestone.heroes.Hero;
import ir.mrbenyamin.firestone.play.Play;
import ir.mrbenyamin.firestone.player.Player;
import ir.mrbenyamin.firestone.view.Constants;
import ir.mrbenyamin.firestone.view.Handler;
import ir.mrbenyamin.firestone.view.assets.Assets;
import ir.mrbenyamin.firestone.view.graphicutils.cardview.OnBackCardView;
import ir.mrbenyamin.firestone.view.graphicutils.heroview.HeroView;

public class OpponentHandCardsManager extends HandCardsManager {
    public OpponentHandCardsManager(Handler handler, Play play) {
        super(handler, play);
    }

    @Override
    public void updateCardViews() {
        Player opponentPlayer = play.getOpponentPlayer();
        manager.getObjects().removeAll(manager.getObjects());
        for (int i = 0; i < opponentPlayer.getHandCards().size(); i++) {
            OnBackCardView handCardView = new OnBackCardView(opponentPlayer.getHandCards().get(i),
                    play.getCurrentPlayer(),
                    5 + i * 2 + i * (3 * Constants.CARD_VIEW_WIDTH / 5),
                    35,
                    3 * Constants.CARD_VIEW_WIDTH / 5, 3 * Constants.CARD_VIEW_HEIGHTS / 5,
                    () -> {

                    });
            manager.addObject(handCardView);
        }
        addHero(opponentPlayer.getPlayingDeck().getHero());
    }

    public void addHero(Hero hero) {
        HeroView heroView = new HeroView(hero, 900, 100,
                Assets.getHeroImage(hero.getName()).getWidth()/3,
                Assets.getHeroImage(hero.getName()).getHeight()/3, () -> {
            if (play.getOnFieldSelectedCard() != null){
                play.attackToHero(play.getOnFieldSelectedCard(), hero);
            }
        });
        manager.addObject(heroView);
    }
}
