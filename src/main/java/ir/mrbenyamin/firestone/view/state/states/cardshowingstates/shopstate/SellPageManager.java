package ir.mrbenyamin.firestone.view.state.states.cardshowingstates.shopstate;

import ir.mrbenyamin.firestone.view.Constants;
import ir.mrbenyamin.firestone.view.Handler;

import java.awt.*;

public class SellPageManager {
    private SellPage currentPage;
    private Handler handler;

    SellPageManager(Handler handler) {
        this.handler = handler;
    }

    public void tick(){
        if (currentPage != null){
            currentPage.tick();
        }

    }
    public void render(Graphics2D g){
        g.setColor(Constants.WARNING_FONT_COLOR);
        g.setFont(new Font("Arial", Font.BOLD, 14));
        g.drawString("Your gems: " + handler.getGame().getSignedPlayer().getGems() , 605, 80);
        if (currentPage != null)
        currentPage.render(g);
    }

    public void addPage(SellPage sellPage){
        this.currentPage = sellPage;
    }

    public SellPage getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(SellPage currentPage) {
        this.currentPage = currentPage;
    }

    public Handler getHandler() {
        return handler;
    }

    public void setHandler(Handler handler) {
        this.handler = handler;
    }
}
