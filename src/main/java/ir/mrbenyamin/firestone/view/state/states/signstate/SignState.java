package ir.mrbenyamin.firestone.view.state.states.signstate;

import ir.mrbenyamin.firestone.view.Constants;
import ir.mrbenyamin.firestone.view.state.State;
import ir.mrbenyamin.firestone.view.Handler;
import ir.mrbenyamin.firestone.view.assets.Assets;

import java.awt.*;

public abstract class SignState extends State {
    SignState(Handler handler) {
        super(handler);
    }

    void renderBack(Graphics2D g){
        g.drawImage(Assets.mainLeftImage, 0, 0, null);
        g.drawImage(Assets.signBackGround, Constants.STANDARD_STATES_WIDTH / 2, 0, null);
        g.drawImage(Assets.mainLogo, 3 * Constants.STANDARD_STATES_WIDTH /4 - Assets.mainLogo.getWidth() / 2 , 400, null);
        g.drawImage(Assets.fireStoneLogo, Constants.STANDARD_STATES_WIDTH /4 - Assets.fireStoneLogo.getWidth() / 2 , 20, null);
    }

    public abstract void clearDate();
}
