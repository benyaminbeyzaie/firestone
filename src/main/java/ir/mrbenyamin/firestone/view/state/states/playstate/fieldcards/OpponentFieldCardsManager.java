package ir.mrbenyamin.firestone.view.state.states.playstate.fieldcards;

import ir.mrbenyamin.firestone.play.Play;
import ir.mrbenyamin.firestone.player.Player;
import ir.mrbenyamin.firestone.view.Constants;
import ir.mrbenyamin.firestone.view.Handler;
import ir.mrbenyamin.firestone.view.graphicutils.cardview.HandCardView;

import java.util.ArrayList;

public class OpponentFieldCardsManager extends FieldCardsManager {
    public OpponentFieldCardsManager(Handler handler, Play play) {
        super(handler, play);
    }

    @Override
    public void updateCardViews() {
        Player player = play.getOpponentPlayer();
        manager.getObjects().removeAll(manager.getObjects());
        if (player.getOnFieldMinions()==null){
            player.setOnFieldMinions(new ArrayList<>());
        }
        for (int i = 0; i < player.getOnFieldMinions().size(); i++) {
            int finalI = i;
            HandCardView handCardView = new HandCardView(player.getOnFieldMinions().get(i),
                    player,
                    170 + i * 2 + i * (2 * Constants.CARD_VIEW_WIDTH / 3),
                    270 - 2 * Constants.CARD_VIEW_HEIGHTS / 3,
                    2 * Constants.CARD_VIEW_WIDTH / 3, 2 * Constants.CARD_VIEW_HEIGHTS / 3,
                    () -> {
                if (play.isMustSelectEnemyCard()){
                    executeNeedSelectCardBattlecryOrSpell(player, finalI);
                }
                        // play.actionSpellWithID(play.getSpellActionCallerID(), player.getOnFieldMinions().get(finalI));
                        if (play.getOnFieldSelectedCard() != null){
                            play.attackToMinion(play.getOnFieldSelectedCard(), play.getOpponentPlayer().getOnFieldMinions().get(finalI));
                        }
                    });
            manager.addObject(handCardView);
        }
    }


}
