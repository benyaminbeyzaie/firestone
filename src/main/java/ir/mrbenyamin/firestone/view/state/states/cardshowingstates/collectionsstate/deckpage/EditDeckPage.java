package ir.mrbenyamin.firestone.view.state.states.cardshowingstates.collectionsstate.deckpage;

import ir.mrbenyamin.firestone.card.Card;
import ir.mrbenyamin.firestone.card.Deck;
import ir.mrbenyamin.firestone.heroes.Hero;
import ir.mrbenyamin.firestone.log.MyLogger;
import ir.mrbenyamin.firestone.util.HibernateUtil;
import ir.mrbenyamin.firestone.view.Constants;
import ir.mrbenyamin.firestone.view.Handler;
import ir.mrbenyamin.firestone.view.assets.Assets;
import ir.mrbenyamin.firestone.view.graphicutils.BObject;
import ir.mrbenyamin.firestone.view.graphicutils.BObjectsManager;
import ir.mrbenyamin.firestone.view.graphicutils.ClickListener;
import ir.mrbenyamin.firestone.view.graphicutils.button.BTextButton;
import ir.mrbenyamin.firestone.view.graphicutils.label.BLabel;
import ir.mrbenyamin.firestone.view.graphicutils.textbox.BTextBox;
import ir.mrbenyamin.firestone.view.state.states.MainState;
import org.hibernate.Session;

import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;

public class EditDeckPage {
    private BTextBox deckName;
    private ArrayList<BTextButton> heroesButton;
    private BObjectsManager manager;
    private Handler handler;
    private BTextButton done;
    private BLabel deckNameLabel, selectHeroLabel;
    private Deck selectedDeck;

    EditDeckPage(Handler handler) {
        this.handler = handler;
        selectedDeck = handler.getGame().getCollectionsState().getSelectedDeck();
        heroesButton = new ArrayList<>();
        manager = new BObjectsManager(handler);
        int i = 0;
        deckNameLabel = new BLabel("New name: ", 605, 80, 100, 20, 12);
        deckNameLabel.setStringColor(Constants.WARNING_FONT_COLOR);
        selectHeroLabel = new BLabel("Select Hero: ", 605, 130, 100, 20, 12);
        selectHeroLabel.setStringColor(Constants.WARNING_FONT_COLOR);
        deckName = new BTextBox(605, 90, 100, 20, () -> {
            handler.getGame().getKeyBoardManager().setBObjectManager(manager);
            for (BObject o :
                    manager.getObjects()) {
                if (o instanceof BTextBox){
                    ((BTextBox) o).setUnderType(false);
                }
            }
            deckName.setUnderType(true);
            deckName.setFont(new Font("Arial", Font.PLAIN, 12));
        });
        for (Hero hero:
                handler.getGame().getSignedPlayer().getAvailableHeroes()) {
            heroesButton.add(new BTextButton(hero.getName(), 605, 140 + i * 25, 100, 20, () -> {
                boolean flag = true;
                for (Card card :
                        selectedDeck.getCards()) {
                    if (!card.getHeroClass().equals("Any") && !card.getHeroClass().equals(hero.getName())){
                        flag = false;
                        handler.getGame().getCollectionsState().getPageManager().getMessage().setMessage(
                                card.getName() + " can't fit in " + hero.getName() + "'s deck"
                        );
                        handler.getGame().getCollectionsState().getPageManager().getMessage().setBackgroundColor(Constants.WARNING_COLOR);
                        handler.getGame().getCollectionsState().getPageManager().getMessage().setImage(Assets.warning);
                        handler.getGame().getCollectionsState().getPageManager().getMessage().setShow(true);
                    }
                }
                if (flag){
                    if (selectedDeck == handler.getGame().getSignedPlayer().getCurrentDeck()){
                        selectedDeck.resetStatus();
                        handler.getGame().getSignedPlayer().setCurrentDeck(null);
                    }
                    selectedDeck.setHero(hero);
                    try {
                        MyLogger.writeLog(MainState.LOG_NAME,   selectedDeck.getName() + "'s hero was changed to " + hero.getName(), Level.INFO);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    selectedDeck.resetStatus();
                    if (selectedDeck == handler.getGame().getSignedPlayer().getCurrentDeck()) handler.getGame().getSignedPlayer().setCurrentDeck(null);
                    handler.getGame().getCollectionsState().getPageManager().getMessage().setMessage(
                            "Successfully changed!"
                    );
                    handler.getGame().getCollectionsState().getPageManager().getMessage().setBackgroundColor(Constants.BUTTON_COLOR_3);
                    handler.getGame().getCollectionsState().getPageManager().getMessage().setImage(null);
                    handler.getGame().getCollectionsState().getPageManager().getMessage().setShow(true);
                }
            }));
            i++;
        }

        done = new BTextButton("Back", 605, 450, 100, 20, () -> handler.getGame().getCollectionsState().setEditDeckPage(null));

        BTextButton edit = new BTextButton("Apply", 605, 425, 100, 20, () -> {
            try {
                MyLogger.writeLog(MainState.LOG_NAME,  "Player changed " + handler.getGame().getCollectionsState().getSelectedDeck().getName() + " name to: " + deckName.getTypedString(), Level.INFO);
            } catch (IOException e) {
                e.printStackTrace();
            }
            boolean flag = true;
            for (Deck deck :
                    handler.getGame().getSignedPlayer().getDecks()) {
                if (deck.getName().equals(deckName.getTypedString())) {
                    flag = false;
                    break;
                }
                }
            if (flag){
                handler.getGame().getCollectionsState().getSelectedDeck().setName(deckName.getTypedString());
                handler.getGame().getCollectionsState().setEditDeckPage(null);
            }else {
                handler.getGame().getCollectionsState().getPageManager().getMessage().setMessage(
                        "Choose another name!"
                );
                handler.getGame().getCollectionsState().getPageManager().getMessage().setBackgroundColor(Constants.WARNING_COLOR);
                handler.getGame().getCollectionsState().getPageManager().getMessage().setImage(Assets.warning);
                handler.getGame().getCollectionsState().getPageManager().getMessage().setShow(true);
            }
        });
        manager.addObject(edit);

        BTextButton delete = new BTextButton("Delete", 605, 400, 100, 20, () -> {
            try {
                MyLogger.writeLog(MainState.LOG_NAME,  "Player delete " + selectedDeck.getName() + " from decks", Level.INFO);
            } catch (IOException e) {
                e.printStackTrace();
            }
            handler.getGame().getSignedPlayer().getDecks().remove(selectedDeck);
            if (handler.getGame().getSignedPlayer().getCurrentDeck() == selectedDeck){
                handler.getGame().getSignedPlayer().setCurrentDeck(null);
            }
            handler.getGame().getCollectionsState().setEditDeckPage(null);
            handler.getGame().getCollectionsState().setCurrentDeckPage(null);
            handler.getGame().getCollectionsState().setSelectedDeck(null);
            handler.getGame().getCollectionsState().setUpDecks(handler.getGame().getSignedPlayer());
            handler.getGame().getSignedPlayer().saveSignedPlayerInfo();

        });
        delete.setButtonHoverColor(Constants.BUTTON_COLOR_4);
        manager.addObject(delete);

        BTextButton setAsCurrentDeck = new BTextButton("Set As Current Deck", 605, 375, 100, 20, () -> {
            if (selectedDeck.getCards().size() >= Constants.DECK_SIZE){
                try {
                    MyLogger.writeLog(MainState.LOG_NAME,  "Player changeed current deck to " + selectedDeck.getName(), Level.INFO);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                handler.getGame().getSignedPlayer().setCurrentDeck(selectedDeck);
                handler.getGame().getCollectionsState().getPageManager().getMessage().setMessage(
                        "Your Current deck now is " + selectedDeck.getName()
                );
                handler.getGame().getCollectionsState().getPageManager().getMessage().setBackgroundColor(Constants.BUTTON_COLOR_3);
                handler.getGame().getCollectionsState().getPageManager().getMessage().setImage(null);
                handler.getGame().getCollectionsState().getPageManager().getMessage().setShow(true);
            }else {
                handler.getGame().getCollectionsState().getPageManager().getMessage().setMessage(
                        "You must have at least " + Constants.DECK_SIZE + " cards on your deck!"
                );
                handler.getGame().getCollectionsState().getPageManager().getMessage().setBackgroundColor(Constants.BUTTON_COLOR_1);
                handler.getGame().getCollectionsState().getPageManager().getMessage().setImage(null);
                handler.getGame().getCollectionsState().getPageManager().getMessage().setShow(true);
            }
            handler.getGame().getSignedPlayer().saveSignedPlayerInfo();

        });
        setAsCurrentDeck.setButtonTextSize(10);
        setAsCurrentDeck.setButtonHoverColor(Constants.LABEL_WHITE);
        manager.addObject(setAsCurrentDeck);

        for (BTextButton b :
                heroesButton) {
            manager.addObject(b);
        }
        manager.addObject(deckName);
        manager.addObject(deckNameLabel);
        manager.addObject(selectHeroLabel);
        manager.addObject(done);
    }

    public void tick(){
        manager.tick();
    }
    public void render(Graphics2D g){
        manager.render(g);
    }

    public BTextBox getDeckName() {
        return deckName;
    }

    public void setDeckName(BTextBox deckName) {
        this.deckName = deckName;
    }

    public ArrayList<BTextButton> getHeroesButton() {
        return heroesButton;
    }

    public void setHeroesButton(ArrayList<BTextButton> heroesButton) {
        this.heroesButton = heroesButton;
    }

    public BObjectsManager getManager() {
        return manager;
    }

    public void setManager(BObjectsManager manager) {
        this.manager = manager;
    }

    public Handler getHandler() {
        return handler;
    }

    public void setHandler(Handler handler) {
        this.handler = handler;
    }
}
