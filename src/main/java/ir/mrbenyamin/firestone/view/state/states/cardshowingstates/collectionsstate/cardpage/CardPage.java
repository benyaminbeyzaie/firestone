package ir.mrbenyamin.firestone.view.state.states.cardshowingstates.collectionsstate.cardpage;

import ir.mrbenyamin.firestone.card.Card;
import ir.mrbenyamin.firestone.view.Constants;
import ir.mrbenyamin.firestone.view.Handler;
import ir.mrbenyamin.firestone.view.assets.Assets;
import ir.mrbenyamin.firestone.view.graphicutils.BObjectsManager;
import ir.mrbenyamin.firestone.view.graphicutils.ClickListener;
import ir.mrbenyamin.firestone.view.graphicutils.cardview.CollectionsCardView;
import ir.mrbenyamin.firestone.view.state.StateManager;
import ir.mrbenyamin.firestone.view.state.states.cardshowingstates.collectionsstate.deckpage.CurrentDeckPage;
import ir.mrbenyamin.firestone.view.state.states.cardshowingstates.shopstate.SellPage;

import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;

public class CardPage {
    private String heroName;
    private ArrayList<Card> mustShowCards = new ArrayList<>();
    private int num;
    private ArrayList<Card> allHeroCards;
    private CollectionsCardView[] collectionsCardViews = new CollectionsCardView[6];
    private BObjectsManager manager;
    private Handler handler;
    private String nameFilter;
    private int gemFilter;
    private Have have;
    private PageType pageType;


    CardPage(String heroName, int num, int gemFilter, String nameFilter, Have have, PageType pageType, Handler handler) {
        this.heroName = heroName;
        this.num = num;
        this.handler = handler;
        this.nameFilter = nameFilter;
        this.gemFilter = gemFilter;
        this.have = have;
        this.pageType = pageType;
        manager = new BObjectsManager(handler);

        calculateMustShowCards();
        if (pageType == PageType.COLLECTIONS){
            setUpCollectionsCardViews();
        }
        else if (pageType == PageType.SHOP){
            setUpShopCardViews();
        }
    }

    private void setUpShopCardViews() {
        for (int i = num * 6; i < num * 6 + 6; i++) {
            if (i < mustShowCards.size()){
                if (i % 6 <= 2){
                    int finalI1 = i;
                    collectionsCardViews[i % 6] = new CollectionsCardView(mustShowCards.get(i),handler.getGame().getSignedPlayer(), (i % 3) * Constants.CARD_VIEW_WIDTH + (20 * (i % 6)) + 80, 90, Constants.CARD_VIEW_WIDTH, Constants.CARD_VIEW_HEIGHTS, () -> {
                        if (handler.getGame().getSignedPlayer().getAllAvailableCards().contains(mustShowCards.get(finalI1))){
                            // SetUp sell page
                            handler.getGame().getShopState().getSellPageManager().addPage(new SellPage(mustShowCards.get(finalI1),  handler));
                        }else {
                            // setUp buy page
                            handler.getGame().getShopState().getSellPageManager().addPage(new SellPage(mustShowCards.get(finalI1), handler));
                        }
                    });
                }else{
                    int finalI2 = i;
                    collectionsCardViews[i % 6] = new CollectionsCardView(mustShowCards.get(i),handler.getGame().getSignedPlayer(), (i % 3) * Constants.CARD_VIEW_WIDTH + 20 * (i % 3) + 80, 100 + Constants.CARD_VIEW_HEIGHTS, Constants.CARD_VIEW_WIDTH, Constants.CARD_VIEW_HEIGHTS, () -> {
                        if (handler.getGame().getSignedPlayer().getAllAvailableCards().contains(mustShowCards.get(finalI2))){
                            // SetUp sell page
                            handler.getGame().getShopState().getSellPageManager().addPage(new SellPage(mustShowCards.get(finalI2),  handler));
                        }else {
                            // setUp buy page
                            handler.getGame().getShopState().getSellPageManager().addPage(new SellPage(mustShowCards.get(finalI2), handler));
                        }
                    });
                }
                manager.addObject(collectionsCardViews[i % 6]);
            }
        }
    }

    private void setUpCollectionsCardViews() {
        for (int i = num * 6; i < num * 6 + 6; i++) {
            if (i < mustShowCards.size()){
                if (i % 6 <= 2){
                    int finalI1 = i;
                    collectionsCardViews[i % 6] = new CollectionsCardView(mustShowCards.get(i),handler.getGame().getSignedPlayer(), (i % 3) * Constants.CARD_VIEW_WIDTH + (20 * (i % 6)) + 80, 90, Constants.CARD_VIEW_WIDTH, Constants.CARD_VIEW_HEIGHTS, () -> {
                        if (handler.getGame().getCollectionsState().getSelectedDeck() != null){
                            addCardToDeck(mustShowCards.get(finalI1));
                        }else {
                            // select a deck first!
                            handler.getGame().getCollectionsState().getPageManager().getMessage().setMessage("select a deck first!");
                            handler.getGame().getCollectionsState().getPageManager().getMessage().setImage(Assets.error);
                            handler.getGame().getCollectionsState().getPageManager().getMessage().setBackgroundColor(Constants.WARNING_COLOR);
                            handler.getGame().getCollectionsState().getPageManager().getMessage().setShow(true);
                        }
                    });
                }else{
                    int finalI2 = i;
                    collectionsCardViews[i % 6] = new CollectionsCardView(mustShowCards.get(i),handler.getGame().getSignedPlayer(), (i % 3) * Constants.CARD_VIEW_WIDTH + 20 * (i % 3) + 80, 100 + Constants.CARD_VIEW_HEIGHTS, Constants.CARD_VIEW_WIDTH, Constants.CARD_VIEW_HEIGHTS, () -> {
                        if (handler.getGame().getCollectionsState().getSelectedDeck() != null){
                            addCardToDeck(mustShowCards.get(finalI2));
                        }else {
                            // select a deck first!
                            handler.getGame().getCollectionsState().getPageManager().getMessage().setMessage("select a deck first!");
                            handler.getGame().getCollectionsState().getPageManager().getMessage().setImage(Assets.error);
                            handler.getGame().getCollectionsState().getPageManager().getMessage().setBackgroundColor(Constants.WARNING_COLOR);
                            handler.getGame().getCollectionsState().getPageManager().getMessage().setShow(true);
                        }
                    });
                }
                manager.addObject(collectionsCardViews[i % 6]);
            }
        }
    }

    private void calculateMustShowCards() {
        try {
            allHeroCards = calculateAllHeroCards();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (gemFilter == -1 && nameFilter == null && have == Have.NOT_MATTER){
            mustShowCards.addAll(allHeroCards);
        }
        else if (nameFilter == null && have == Have.NOT_MATTER) {
            for (Card card : allHeroCards) {
                if (card.getManaCost() == gemFilter){
                    mustShowCards.add(card);
                }
            }
        }else if (nameFilter != null && have == Have.NOT_MATTER) {
            for (Card card : allHeroCards) {
                if (card.getName().contains(nameFilter)){
                    mustShowCards.add(card);
                }
            }
        }
        if (have == Have.YES){
            for (Card card : allHeroCards) {
                if (handler.getGame().getSignedPlayer().getAllAvailableCards().contains(card)){
                    mustShowCards.add(card);
                }
            }
        }else if (have == Have.NO){
            for (Card card : allHeroCards) {
                if (!handler.getGame().getSignedPlayer().getAllAvailableCards().contains(card)){
                    mustShowCards.add(card);
                }
            }
        }
    }

    private void addCardToDeck(Card card) {

        int temp = handler.getGame().getSignedPlayer().addCardToDeck(card, handler.getGame().getCollectionsState().getSelectedDeck());
        if (temp == 0){
            handler.getGame().getCollectionsState().getPageManager().getMessage().setMessage("Selected deck is full!");
            handler.getGame().getCollectionsState().getPageManager().getMessage().setImage(Assets.warning);
            handler.getGame().getCollectionsState().getPageManager().getMessage().setBackgroundColor(Constants.WARNING_COLOR);
            handler.getGame().getCollectionsState().getPageManager().getMessage().setShow(true);
        }else if (temp == 1){
            handler.getGame().getCollectionsState().getPageManager().getMessage().setMessage("this card can not added to this hero's deck!");
            handler.getGame().getCollectionsState().getPageManager().getMessage().setImage(Assets.warning);
            handler.getGame().getCollectionsState().getPageManager().getMessage().setBackgroundColor(Constants.WARNING_COLOR);
            handler.getGame().getCollectionsState().getPageManager().getMessage().setShow(true);
        }else if (temp == 2){
            handler.getGame().getCollectionsState().getPageManager().getMessage().setMessage("card added successfully!");
            handler.getGame().getCollectionsState().getPageManager().getMessage().setBackgroundColor(Constants.BUTTON_COLOR_3);
            handler.getGame().getCollectionsState().getPageManager().getMessage().setImage(null);
            handler.getGame().getCollectionsState().getPageManager().getMessage().setShow(true);
            handler.getGame().getCollectionsState().setCurrentDeckPage(new CurrentDeckPage(handler));

        }else if (temp == 3){
            handler.getGame().getCollectionsState().getPageManager().getMessage().setMessage("buy this card first");
            handler.getGame().getCollectionsState().getPageManager().getMessage().setBackgroundColor(Constants.WARNING_COLOR);
            handler.getGame().getCollectionsState().getPageManager().getMessage().setImage(Assets.warning);
            handler.getGame().getCollectionsState().getPageManager().getMessage().setShow(true);
            try {
                handler.getGame().getShopState().setUpPages(handler.getGame().getSignedPlayer(), -1, PageType.SHOP);
            } catch (IOException e) {
                e.printStackTrace();
            }
            StateManager.setCurrentState(handler.getGame().getShopState());
            handler.getGame().getShopState().getSellPageManager().addPage(new SellPage(card,  handler));
        } else if (temp == 4){
            handler.getGame().getCollectionsState().getPageManager().getMessage().setMessage("You already have two number of this!");
            handler.getGame().getCollectionsState().getPageManager().getMessage().setImage(Assets.error);
            handler.getGame().getCollectionsState().getPageManager().getMessage().setBackgroundColor(Constants.WARNING_COLOR);
            handler.getGame().getCollectionsState().getPageManager().getMessage().setShow(true);
        }
    }

    private ArrayList<Card> calculateAllHeroCards() throws IOException {
        ArrayList<Card> arrayList = new ArrayList<>();
        for (Card card :
                Card.getAllCards()) {
            if (card.getHeroClass().equals(this.heroName)) arrayList.add(card);
        }
        return arrayList;
    }

    public void tick(){
        manager.tick();
    }

    public void render(Graphics2D g){
        manager.render(g);
    }

    public String getHeroName() {
        return heroName;
    }

    public void setHeroName(String heroName) {
        this.heroName = heroName;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public ArrayList<Card> getAllHeroCards() {
        return allHeroCards;
    }

    public void setAllHeroCards(ArrayList<Card> allHeroCards) {
        this.allHeroCards = allHeroCards;
    }

    public CollectionsCardView[] getCollectionsCardViews() {
        return collectionsCardViews;
    }

    public void setCollectionsCardViews(CollectionsCardView[] collectionsCardViews) {
        this.collectionsCardViews = collectionsCardViews;
    }

    public BObjectsManager getManager() {
        return manager;
    }

    public void setManager(BObjectsManager manager) {
        this.manager = manager;
    }
}
