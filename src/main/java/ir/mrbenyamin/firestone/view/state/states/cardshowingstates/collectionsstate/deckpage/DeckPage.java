package ir.mrbenyamin.firestone.view.state.states.cardshowingstates.collectionsstate.deckpage;

import ir.mrbenyamin.firestone.view.Constants;
import ir.mrbenyamin.firestone.view.Handler;
import ir.mrbenyamin.firestone.view.assets.Assets;
import ir.mrbenyamin.firestone.view.graphicutils.BObjectsManager;
import ir.mrbenyamin.firestone.view.graphicutils.ClickListener;
import ir.mrbenyamin.firestone.view.graphicutils.button.BTextButton;

import java.awt.*;
import java.util.ArrayList;

public class DeckPage {
    private Handler handler;
    private ArrayList<BTextButton> deckButtons;
    private BObjectsManager manager;
    private BTextButton newDeckButton;

    public DeckPage(Handler handler) {
        this.handler = handler;
        manager = new BObjectsManager(handler);

        newDeckButton = new BTextButton("New deck", 605, 460, 100, 20, () -> {
            if (handler.getGame().getSignedPlayer().getDecks().size() < 15){
                // Deck size limit
                handler.getGame().getCollectionsState().setCreateDeckPage(new CreateDeckPage(handler));
            }else {
                handler.getGame().getCollectionsState().getPageManager().getMessage().setMessage(
                        "You can't create more than 15 decks"
                );
                handler.getGame().getCollectionsState().getPageManager().getMessage().setBackgroundColor(Constants.WARNING_COLOR);
                handler.getGame().getCollectionsState().getPageManager().getMessage().setImage(Assets.warning);
                handler.getGame().getCollectionsState().getPageManager().getMessage().setShow(true);
            }

        });

        manager.addObject(newDeckButton);
    }


    public void tick(){
        manager.tick();
    }

    public void render(Graphics2D g){
        manager.render(g);
    }

    public Handler getHandler() {
        return handler;
    }

    public void setHandler(Handler handler) {
        this.handler = handler;
    }

    public ArrayList<BTextButton> getDeckButtons() {
        return deckButtons;
    }

    public void setDeckButtons(ArrayList<BTextButton> deckButtons) {
        this.deckButtons = deckButtons;
    }


    public BObjectsManager getManager() {
        return manager;
    }

    public void setManager(BObjectsManager manager) {
        this.manager = manager;
    }

    public BTextButton getNewDeckButton() {
        return newDeckButton;
    }

    public void setNewDeckButton(BTextButton newDeckButton) {
        this.newDeckButton = newDeckButton;
    }
}
