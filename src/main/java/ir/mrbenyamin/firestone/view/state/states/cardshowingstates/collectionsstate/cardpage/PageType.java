package ir.mrbenyamin.firestone.view.state.states.cardshowingstates.collectionsstate.cardpage;

public enum PageType {
    SHOP, COLLECTIONS
}
