package ir.mrbenyamin.firestone.view.state;

import ir.mrbenyamin.firestone.card.Card;
import ir.mrbenyamin.firestone.view.Handler;
import ir.mrbenyamin.firestone.view.graphicutils.BObjectsManager;
import ir.mrbenyamin.firestone.view.state.states.cardshowingstates.collectionsstate.cardpage.Have;

import java.awt.*;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public abstract class State {
    protected Handler handler;
    protected BObjectsManager manager;
    private String name;

    public State(Handler handler){
        this.handler = handler;
    }

    public abstract void tick();

    public abstract void render(Graphics2D g);

    public Handler getHandler() {
        return handler;
    }

    public void setHandler(Handler handler) {
        this.handler = handler;
    }

    public BObjectsManager getManager() {
        return manager;
    }

    public void setManager(BObjectsManager manager) {
        this.manager = manager;
    }

    protected Map<String, Integer> calculateEachHeroPagesNumber(Map card) {
        Map<String, Integer> map =  new HashMap<>();
        for(Object key: card.keySet()){
            int allCard = (int) card.get(key);
            map.put((String) key, allCard/6 + 1);
        }
        return map;
    }

    protected Map<String, Integer> calculateEachHeroCards(int gemFilter) throws IOException {
        Map<String, Integer> map = new HashMap<>();
        for (Card card :
                Card.getAllCards()) {
            if (gemFilter == -1){
                if (map.containsKey(card.getHeroClass())){
                    map.replace(card.getHeroClass(), map.get(card.getHeroClass()) + 1);
                }else{
                    map.put(card.getHeroClass(), 1);
                }
            }
            else if (card.getManaCost() == gemFilter){
                if (map.containsKey(card.getHeroClass())){
                    map.replace(card.getHeroClass(), map.get(card.getHeroClass()) + 1);
                }else{
                    map.put(card.getHeroClass(), 1);
                }
            }
        }
        return map;
    }

    protected Map<String, Integer> calculateEachHeroCards(Have have) throws IOException {
        Map<String, Integer> map = new HashMap<>();
        for (Card card :
                Card.getAllCards()) {
            if (have == Have.YES){
                if (handler.getGame().getSignedPlayer().getAllAvailableCards().contains(card)){
                    if (map.containsKey(card.getHeroClass())){
                        map.replace(card.getHeroClass(), map.get(card.getHeroClass()) + 1);
                    }else{
                        map.put(card.getHeroClass(), 1);
                    }
                }
            }else if (have == Have.NO){
                if (!handler.getGame().getSignedPlayer().getAllAvailableCards().contains(card)){
                    if (map.containsKey(card.getHeroClass())){
                        map.replace(card.getHeroClass(), map.get(card.getHeroClass()) + 1);
                    }else{
                        map.put(card.getHeroClass(), 1);
                    }
                }
            }
        }
        return map;
    }

    protected Map<String, Integer> calculateEachHeroCards(String nameFilter) throws IOException {
        Map<String, Integer> map = new HashMap<>();
        for (Card card :
                Card.getAllCards()) {
            if (card.getName().contains(nameFilter)){
                if (map.containsKey(card.getHeroClass())){
                    map.replace(card.getHeroClass(), map.get(card.getHeroClass()) + 1);
                }else{
                    map.put(card.getHeroClass(), 1);
                }
            }
        }
        return map;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
