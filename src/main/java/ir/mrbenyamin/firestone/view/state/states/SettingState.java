package ir.mrbenyamin.firestone.view.state.states;

import ir.mrbenyamin.firestone.player.Player;
import ir.mrbenyamin.firestone.util.HibernateUtil;
import ir.mrbenyamin.firestone.view.Constants;
import ir.mrbenyamin.firestone.view.Game;
import ir.mrbenyamin.firestone.view.Handler;
import ir.mrbenyamin.firestone.view.assets.Assets;
import ir.mrbenyamin.firestone.view.graphicutils.BObjectsManager;
import ir.mrbenyamin.firestone.view.graphicutils.ClickListener;
import ir.mrbenyamin.firestone.view.graphicutils.button.BImageButton;
import ir.mrbenyamin.firestone.view.graphicutils.button.BTextButton;
import ir.mrbenyamin.firestone.view.graphicutils.label.BLabel;
import ir.mrbenyamin.firestone.view.state.State;
import ir.mrbenyamin.firestone.view.state.StateManager;
import org.hibernate.Session;

import java.awt.*;
import java.io.IOException;

public class SettingState extends State {
    private Player signedPlayer;
    private BObjectsManager manager;
    public SettingState(Handler handler) {
        super(handler);
        setName("Setting");
        signedPlayer = handler.getGame().getSignedPlayer();
        manager = new BObjectsManager(handler);
        BImageButton backButton = new BImageButton(10, 10, Assets.back[0].getWidth(), Assets.back[0].getHeight(), Assets.back, () -> {
            StateManager.setCurrentState(handler.getGame().getMainState());
            handler.getGame().getSignedPlayer().saveSignedPlayerInfo();
        });


        BLabel selectCard = new BLabel("select CrardBack: ", 10, 60, 100, 20, 15);
        selectCard.setStringColor(Constants.WARNING_FONT_COLOR);

        BImageButton cardOne = new BImageButton(150, 30, Assets.backCardOne.getWidth()/3, Assets.backCardOne.getHeight()/3, Assets.backCardOne, () -> signedPlayer.setCardBackImageName("one"));

        BImageButton cardTwo = new BImageButton(150 + Assets.backCardOne.getWidth()/3 + 10, 30, Assets.backCardOne.getWidth()/3, Assets.backCardOne.getHeight()/3, Assets.backCardTwo, () -> signedPlayer.setCardBackImageName("two"));

        BImageButton cardThree = new BImageButton(150 + 2 * Assets.backCardOne.getWidth()/3 + 10, 30, Assets.backCardOne.getWidth()/3, Assets.backCardOne.getHeight()/3, Assets.backCardThree, () -> signedPlayer.setCardBackImageName("three"));

        BImageButton cardFour = new BImageButton(150 + 3 * Assets.backCardOne.getWidth()/3 + 10, 30, Assets.backCardOne.getWidth()/3, Assets.backCardOne.getHeight()/3, Assets.backCardFour, () -> signedPlayer.setCardBackImageName("four"));

        BImageButton cardFive = new BImageButton(150 + 4 * Assets.backCardOne.getWidth()/3 + 10, 30, Assets.backCardOne.getWidth()/3, Assets.backCardOne.getHeight()/3, Assets.backCardFive, () -> signedPlayer.setCardBackImageName("five"));

        BLabel selectGameBack = new BLabel("select Game background: ", 10, 200, 100, 20, 15);
        selectGameBack.setStringColor(Constants.WARNING_FONT_COLOR);
        BImageButton backOne = new BImageButton(220, 150, Assets.playBack.getWidth()/7, Assets.playBack.getHeight()/7, Assets.playBack, () -> signedPlayer.setGameBackImageName("one"));
        manager.addObject(backOne);

        BImageButton backTwo = new BImageButton(420, 150, Assets.playBack.getWidth()/7, Assets.playBack.getHeight()/7, Assets.playBack2, () -> signedPlayer.setGameBackImageName("two"));
        manager.addObject(backTwo);

        BLabel soundOnLabel = new BLabel("Sound: ", 10, 270, 100, 20, 15);
        soundOnLabel.setStringColor(Constants.WARNING_FONT_COLOR);

        BTextButton off = new BTextButton("OFF", 80, 255, 40, 20, () -> {
            Game.stopBackSound();
            signedPlayer.setSoundPlay(false);
            handler.getGame().getSignedPlayer().saveSignedPlayerInfo();
        });
        off.setButtonColor(Constants.LABEL_WHITE);
        off.setButtonHoverColor(Constants.BUTTON_COLOR_4);
        BTextButton on = new BTextButton("ON", 125, 255, 40, 20, () -> {
            if (!signedPlayer.isSoundPlay()){
                signedPlayer.setSoundPlay(true);
                handler.getGame().playBackSound("all");
            }
            handler.getGame().getSignedPlayer().saveSignedPlayerInfo();
        });

        BLabel backLabel = new BLabel("Background sound: ", 180, 270, 100, 20, 15);
        backLabel.setStringColor(Constants.WARNING_FONT_COLOR);

        BTextButton oneBack = new BTextButton("one", 345, 255, 40, 20, () -> {
            Game.stopBackSound();
            signedPlayer.setBackMusicName("one");
            handler.getGame().playBackSound("all");
            handler.getGame().getSignedPlayer().saveSignedPlayerInfo();
        });

        BTextButton twoBack = new BTextButton("two", 390, 255, 40, 20, () -> {
            Game.stopBackSound();
            signedPlayer.setBackMusicName("two");
            handler.getGame().playBackSound("all");
            handler.getGame().getSignedPlayer().saveSignedPlayerInfo();
        });

        BLabel playBackLabel = new BLabel("Play background sound: ", 440, 270, 100, 20, 15);
        playBackLabel.setStringColor(Constants.WARNING_FONT_COLOR);

        BTextButton playOneBack = new BTextButton("one", 625, 255, 40, 20, () -> {
            signedPlayer.setGameMusicName("one");
            handler.getGame().getSignedPlayer().saveSignedPlayerInfo();
        });

        BTextButton playTwoBack = new BTextButton("two", 670, 255, 40, 20, () -> {
            signedPlayer.setGameMusicName("two");
            handler.getGame().getSignedPlayer().saveSignedPlayerInfo();
        });


        manager.addObject(selectCard);
        manager.addObject(selectGameBack);
        manager.addObject(soundOnLabel);
        manager.addObject(on);
        manager.addObject(off);
        manager.addObject(backLabel);
        manager.addObject(oneBack);
        manager.addObject(twoBack);
        manager.addObject(playBackLabel);
        manager.addObject(playOneBack);
        manager.addObject(playTwoBack);
        manager.addObject(backButton);

        manager.addObject(cardOne);
        manager.addObject(cardTwo);
        manager.addObject(cardThree);
        manager.addObject(cardFour);
        manager.addObject(cardFive);

    }

    @Override
    public void tick() {
        manager.tick();
    }

    @Override
    public void render(Graphics2D g) {
        manager.render(g);
    }

    public Player getSignedPlayer() {
        return signedPlayer;
    }

    public void setSignedPlayer(Player signedPlayer) {
        this.signedPlayer = signedPlayer;
    }

    @Override
    public BObjectsManager getManager() {
        return manager;
    }

    @Override
    public void setManager(BObjectsManager manager) {
        this.manager = manager;
    }
}
