package ir.mrbenyamin.firestone.view.state.states.cardshowingstates.collectionsstate;

import ir.mrbenyamin.firestone.card.Deck;
import ir.mrbenyamin.firestone.player.Player;
import ir.mrbenyamin.firestone.util.HibernateUtil;
import ir.mrbenyamin.firestone.view.Constants;
import ir.mrbenyamin.firestone.view.Handler;
import ir.mrbenyamin.firestone.view.assets.Assets;
import ir.mrbenyamin.firestone.view.display.MouseManager;
import ir.mrbenyamin.firestone.view.graphicutils.BObjectsManager;
import ir.mrbenyamin.firestone.view.graphicutils.ClickListener;
import ir.mrbenyamin.firestone.view.graphicutils.button.BImageButton;
import ir.mrbenyamin.firestone.view.graphicutils.button.BTextButton;
import ir.mrbenyamin.firestone.view.state.State;
import ir.mrbenyamin.firestone.view.state.StateManager;
import ir.mrbenyamin.firestone.view.state.states.cardshowingstates.collectionsstate.cardpage.CardPageManager;
import ir.mrbenyamin.firestone.view.state.states.cardshowingstates.collectionsstate.cardpage.Have;
import ir.mrbenyamin.firestone.view.state.states.cardshowingstates.collectionsstate.cardpage.PageType;
import ir.mrbenyamin.firestone.view.state.states.cardshowingstates.collectionsstate.deckpage.CreateDeckPage;
import ir.mrbenyamin.firestone.view.state.states.cardshowingstates.collectionsstate.deckpage.CurrentDeckPage;
import ir.mrbenyamin.firestone.view.state.states.cardshowingstates.collectionsstate.deckpage.DeckPage;
import ir.mrbenyamin.firestone.view.state.states.cardshowingstates.collectionsstate.deckpage.EditDeckPage;
import org.hibernate.Session;

import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

public class CollectionsState extends State {
    private CardPageManager pageManager;
    private DeckPage deckPage;
    private Deck selectedDeck;
    private CurrentDeckPage currentDeckPage;
    private CreateDeckPage createDeckPage;
    private BObjectsManager manager;
    private BImageButton backButton;
    private EditDeckPage editDeckPage;
    private Player signedPlayer;


    public CollectionsState(Handler handler) {
        super(handler);
        setName("Collection");
        signedPlayer = handler.getGame().getSignedPlayer();
        deckPage = new DeckPage(handler);
        pageManager = new CardPageManager(handler, PageType.COLLECTIONS, this);
        manager = new BObjectsManager(handler);
        backButton = new BImageButton(10, 10, Assets.back[0].getWidth(), Assets.back[0].getHeight(), Assets.back, () -> {
            StateManager.setCurrentState(handler.getGame().getMainState());
            handler.getGame().getSignedPlayer().saveSignedPlayerInfo();
        });

        manager.addObject(backButton);
    }

    @Override
    public void tick() {
        manager.tick();
        if (MouseManager.mouseX >= 29 && MouseManager.mouseX <= 543 && MouseManager.mouseY >= 56 && MouseManager.mouseY <= 415){
            handler.getGame().getMouseManager().setBObjectManager(pageManager.getCurrentPage().getManager());
        }else if (MouseManager.mouseY > 415 && MouseManager.mouseX <= 543){
            handler.getGame().getMouseManager().setBObjectManager(pageManager.getManager());
        }else if (MouseManager.mouseX >= 550 && MouseManager.mouseY >= 56 && MouseManager.mouseY <= Constants.STANDARD_STATES_HEIGHT){
            if (selectedDeck == null && currentDeckPage == null){
                if (createDeckPage == null){
                    handler.getGame().getMouseManager().setBObjectManager(deckPage.getManager());
                }else {
                    handler.getGame().getMouseManager().setBObjectManager(createDeckPage.getManager());
                }
            }else {
                if (editDeckPage == null){
                    if (currentDeckPage != null)
                    handler.getGame().getMouseManager().setBObjectManager(currentDeckPage.getManager());
                }else {
                    handler.getGame().getMouseManager().setBObjectManager(editDeckPage.getManager());
                }
            }
        }else {
            handler.getGame().getMouseManager().setBObjectManager(manager);
        }
        pageManager.tick();
        deckPage.tick();
        if (createDeckPage != null){
            createDeckPage.tick();
        }

    }

    @Override
    public void render(Graphics2D g) {
        g.drawImage(Assets.collectionsBackground, 0, 0, null);
        g.drawImage(Assets.cardsBorder, 10, 40, null);
        g.drawImage(Assets.decksBorder, 570 , 40, null);
        manager.render(g);
        pageManager.render(g);
        if ((selectedDeck == null || currentDeckPage == null) && createDeckPage == null){
            deckPage.render(g);
        }else {
            if (createDeckPage == null){
                if (editDeckPage == null){
                    currentDeckPage.render(g);
                }else {
                    editDeckPage.render(g);
                }
            }else {
                createDeckPage.render(g);
            }
        }
    }

    public void setUpPages(Player signedPlayer, int gemFilter, PageType type) throws IOException {
        Map<String, Integer> heroAndNumOfCards = calculateEachHeroCards(gemFilter);
        Map<String, Integer> heroAndNumOfPages = calculateEachHeroPagesNumber(heroAndNumOfCards);
        pageManager.addPages(heroAndNumOfPages, gemFilter, type);
    }

    public void setUpPages(Player signedPlayer, String nameFilter, PageType type) throws IOException {
        Map<String, Integer> heroAndNumOfCards = calculateEachHeroCards(nameFilter);
        Map<String, Integer> heroAndNumOfPages = calculateEachHeroPagesNumber(heroAndNumOfCards);
        pageManager.addPages(heroAndNumOfPages, nameFilter, type);
    }

    public void setUpPages(Player signedPlayer, Have have, PageType type) throws IOException {
        Map<String, Integer> heroAndNumOfCards = calculateEachHeroCards(have);
        Map<String, Integer> heroAndNumOfPages = calculateEachHeroPagesNumber(heroAndNumOfCards);
        pageManager.addPages(heroAndNumOfPages, have, type);
    }


    public void setUpDecks(Player signedPlayer){
        deckPage.setManager(new BObjectsManager(handler));
        deckPage.getManager().addObject(deckPage.getNewDeckButton());
        ArrayList<BTextButton> bTextButtons = new ArrayList<>();
        deckPage.setDeckButtons(bTextButtons);
        for (int i = 0; i < signedPlayer.getDecks().size(); i++) {
            int finalI = i;
            BTextButton temp = new BTextButton(handler.getGame().getSignedPlayer().getDecks().get(i).getName(), 600, i * 25 + 70, 110, 20, () -> {
                selectedDeck = (handler.getGame().getSignedPlayer().getDecks().get(finalI));
                currentDeckPage = new CurrentDeckPage(handler);

            });
            deckPage.getManager().addObject(temp);
            deckPage.getDeckButtons().add(temp);
        }
    }



    public CardPageManager getPageManager() {
        return pageManager;
    }

    public void setPageManager(CardPageManager pageManager) {
        this.pageManager = pageManager;
    }

    public DeckPage getDeckPage() {
        return deckPage;
    }

    public void setDeckPage(DeckPage deckPage) {
        this.deckPage = deckPage;
    }

    @Override
    public BObjectsManager getManager() {
        return manager;
    }

    @Override
    public void setManager(BObjectsManager manager) {
        this.manager = manager;
    }

    public BImageButton getBackButton() {
        return backButton;
    }

    public void setBackButton(BImageButton backButton) {
        this.backButton = backButton;
    }

    public Deck getSelectedDeck() {
        return selectedDeck;
    }

    public void setSelectedDeck(Deck selectedDeck) {
        this.selectedDeck = selectedDeck;
    }

    public CurrentDeckPage getCurrentDeckPage() {
        return currentDeckPage;
    }

    public void setCurrentDeckPage(CurrentDeckPage currentDeckPage) {
        this.currentDeckPage = currentDeckPage;
    }

    public CreateDeckPage getCreateDeckPage() {
        return createDeckPage;
    }

    public void setCreateDeckPage(CreateDeckPage createDeckPage) {
        this.createDeckPage = createDeckPage;
    }

    public Player getSignedPlayer() {
        return signedPlayer;
    }

    public void setSignedPlayer(Player signedPlayer) {
        this.signedPlayer = signedPlayer;
    }

    public EditDeckPage getEditDeckPage() {
        return editDeckPage;
    }

    public void setEditDeckPage(EditDeckPage editDeckPage) {
        this.editDeckPage = editDeckPage;
    }
}
