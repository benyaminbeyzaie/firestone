package ir.mrbenyamin.firestone.view.state.states.cardshowingstates.collectionsstate.cardpage;

import ir.mrbenyamin.firestone.view.Constants;
import ir.mrbenyamin.firestone.view.Handler;
import ir.mrbenyamin.firestone.view.assets.Assets;
import ir.mrbenyamin.firestone.view.graphicutils.BObject;
import ir.mrbenyamin.firestone.view.graphicutils.BObjectsManager;
import ir.mrbenyamin.firestone.view.graphicutils.ClickListener;
import ir.mrbenyamin.firestone.view.graphicutils.button.BImageButton;
import ir.mrbenyamin.firestone.view.graphicutils.button.BTextButton;
import ir.mrbenyamin.firestone.view.graphicutils.message.BWarningMessage;
import ir.mrbenyamin.firestone.view.graphicutils.textbox.BTextBox;
import ir.mrbenyamin.firestone.view.state.State;
import ir.mrbenyamin.firestone.view.state.states.cardshowingstates.collectionsstate.CollectionsState;

import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

public class CardPageManager {
    private ArrayList<CardPage> pages = new ArrayList<>();
    private CardPage currentPage;
    private Handler handler;
    private BObjectsManager manager;
    private BImageButton nextPage, previousPage;
    private BImageButton one, two, three, four, five, six, seven, eight, nine, ten;
    private BWarningMessage message;
    private BTextBox searchBox;
    private BImageButton searchImageButton;
    private int currentPageNum = 0;
    private BTextButton showOpenCards, showCloseCards;
    private State state;


    public CardPageManager(Handler handler, PageType type, State state){
        this.handler = handler;
        manager = new BObjectsManager(handler);
        this.state = state;
        message = new BWarningMessage("", Assets.warning, 50, 200, 500, 40, 3000000000L);
        searchBox = new BTextBox(420, 465, 80, 20, () -> {
            handler.getGame().getKeyBoardManager().setBObjectManager(manager);
            for (BObject o :
                    manager.getObjects()) {
                if (o instanceof BTextBox){
                    ((BTextBox) o).setUnderType(false);
                }
            }
            searchBox.setFont(new Font("Arial", Font.PLAIN, 12));
            searchBox.setUnderType(true);
        });
        showOpenCards = new BTextButton("Open Cards", 30, 430, 60, 20, () -> {
            try {
                if (state instanceof CollectionsState){
                    handler.getGame().getCollectionsState().setUpPages(handler.getGame().getSignedPlayer(), Have.YES, type);
                }else{
                    handler.getGame().getShopState().setUpPages(handler.getGame().getSignedPlayer(), Have.YES, type);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        showCloseCards = new BTextButton("Close Cards", 95, 430, 60, 20, () -> {
            try {
                if (state instanceof CollectionsState) {
                    handler.getGame().getCollectionsState().setUpPages(handler.getGame().getSignedPlayer(), Have.NO, type);
                }else {
                    handler.getGame().getShopState().setUpPages(handler.getGame().getSignedPlayer(), Have.NO, type);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        showCloseCards.setButtonTextSize(10);
        showOpenCards.setButtonTextSize(10);
        searchImageButton = new BImageButton(510, 460, 32, 32, Assets.searchButton, () -> {
            try {
                if (state instanceof CollectionsState) {
                    handler.getGame().getCollectionsState().setUpPages(handler.getGame().getSignedPlayer(), searchBox.getTypedString(), type);
                }else {
                    handler.getGame().getShopState().setUpPages(handler.getGame().getSignedPlayer(), searchBox.getTypedString(), type);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        nextPage = new BImageButton(330, 430, Assets.nextPageImageButton[0].getWidth(), Assets.nextPageImageButton[0].getHeight(), Assets.nextPageImageButton, () -> {
            if (pages.size() > 0){
                currentPageNum +=1;
                currentPageNum %= pages.size();
                currentPage = pages.get((currentPageNum) % pages.size());
            }
        });
        previousPage = new BImageButton(170, 430, Assets.previousPageImageButton[0].getWidth(), Assets.previousPageImageButton[0].getHeight(), Assets.previousPageImageButton, () -> {
            if (pages.size() > 0){
                if (currentPageNum == 0){
                    currentPageNum = pages.size() - 1;
                }else{
                    currentPageNum -= 1;
                }
                currentPage = pages.get((currentPageNum) % pages.size());
            }
        });
        one = new BImageButton(30, 455, Assets.redGem[0].getWidth(), Assets.redGem[0].getHeight(), Assets.redGem, () -> {
            try {
                handler.getGame().getCollectionsState().setUpPages(handler.getGame().getSignedPlayer(), 1, type);
                handler.getGame().getShopState().setUpPages(handler.getGame().getSignedPlayer(), 1, type);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        one.setString("1");
        two = new BImageButton(35 + Assets.redGem[0].getWidth(), 455, Assets.redGem[0].getWidth(), Assets.redGem[0].getHeight(), Assets.redGem, () -> {
            try {
                handler.getGame().getCollectionsState().setUpPages(handler.getGame().getSignedPlayer(), 2, type);
                handler.getGame().getShopState().setUpPages(handler.getGame().getSignedPlayer(), 2, type);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        two.setString("2");
        three = new BImageButton(40 + 2 * Assets.redGem[0].getWidth(), 455, Assets.redGem[0].getWidth(), Assets.redGem[0].getHeight(), Assets.redGem, () -> {
            try {
                handler.getGame().getCollectionsState().setUpPages(handler.getGame().getSignedPlayer(), 3, type);
                handler.getGame().getShopState().setUpPages(handler.getGame().getSignedPlayer(), 3, type);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        three.setString("3");
        four = new BImageButton(45 + 3 * Assets.redGem[0].getWidth(), 455, Assets.redGem[0].getWidth(), Assets.redGem[0].getHeight(), Assets.redGem, () -> {
            try {
                handler.getGame().getCollectionsState().setUpPages(handler.getGame().getSignedPlayer(), 4, type);
                handler.getGame().getShopState().setUpPages(handler.getGame().getSignedPlayer(), 4, type);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        four.setString("4");
        five = new BImageButton(50 + 4 * Assets.redGem[0].getWidth(), 455, Assets.redGem[0].getWidth(), Assets.redGem[0].getHeight(), Assets.redGem, () -> {
            try {
                handler.getGame().getCollectionsState().setUpPages(handler.getGame().getSignedPlayer(), 5, type);
                handler.getGame().getShopState().setUpPages(handler.getGame().getSignedPlayer(), 5, type);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        five.setString("5");
        six = new BImageButton(55 + 5 * Assets.redGem[0].getWidth(), 455, Assets.redGem[0].getWidth(), Assets.redGem[0].getHeight(), Assets.redGem, () -> {
            try {
                handler.getGame().getCollectionsState().setUpPages(handler.getGame().getSignedPlayer(), 6, type);
                handler.getGame().getShopState().setUpPages(handler.getGame().getSignedPlayer(), 6, type);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        six.setString("6");
        seven = new BImageButton(60 + 6 * Assets.redGem[0].getWidth(), 455, Assets.redGem[0].getWidth(), Assets.redGem[0].getHeight(), Assets.redGem, () -> {
            try {
                handler.getGame().getCollectionsState().setUpPages(handler.getGame().getSignedPlayer(), 7, type);
                handler.getGame().getShopState().setUpPages(handler.getGame().getSignedPlayer(), 7, type);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        seven.setString("7");
        eight = new BImageButton(65 + 7 * Assets.redGem[0].getWidth(), 455, Assets.redGem[0].getWidth(), Assets.redGem[0].getHeight(), Assets.redGem, () -> {
            try {
                handler.getGame().getCollectionsState().setUpPages(handler.getGame().getSignedPlayer(), 8, type);
                handler.getGame().getShopState().setUpPages(handler.getGame().getSignedPlayer(), 8, type);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        eight.setString("8");
        nine = new BImageButton(70 + 8 * Assets.redGem[0].getWidth(), 455, Assets.redGem[0].getWidth(), Assets.redGem[0].getHeight(), Assets.redGem, () -> {
            try {
                handler.getGame().getCollectionsState().setUpPages(handler.getGame().getSignedPlayer(), 9, type);
                handler.getGame().getShopState().setUpPages(handler.getGame().getSignedPlayer(), 9, type);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        nine.setString("9");
        ten = new BImageButton(75 + 9 * Assets.redGem[0].getWidth(), 455, Assets.redGem[0].getWidth(), Assets.redGem[0].getHeight(), Assets.redGem, () -> {
            try {
                handler.getGame().getCollectionsState().setUpPages(handler.getGame().getSignedPlayer(), 10, type);
                handler.getGame().getShopState().setUpPages(handler.getGame().getSignedPlayer(), 10, type);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        ten.setString("10");

        manager.addObject(nextPage);
        manager.addObject(previousPage);
        manager.addObject(message);
        manager.addObject(one);
        manager.addObject(two);
        manager.addObject(three);
        manager.addObject(four);
        manager.addObject(five);
        manager.addObject(six);
        manager.addObject(seven);
        manager.addObject(eight);
        manager.addObject(nine);
        manager.addObject(ten);
        manager.addObject(searchBox);
        manager.addObject(searchImageButton);
        manager.addObject(showOpenCards);
        manager.addObject(showCloseCards);
    }

    public void tick(){
        currentPage.tick();
        manager.tick();
    }
    public void render(Graphics2D g){
        currentPage.render(g);
        g.setColor(Color.BLACK);
        g.setFont(new Font("Arial", Font.BOLD, 20));
        g.drawString("page " + (currentPageNum  + 1) , 250, 450);
        g.drawString(currentPage.getHeroName(), 250, 80);
        manager.render(g);

    }

    public BObjectsManager getManager() {
        return manager;
    }

    public void setManager(BObjectsManager manager) {
        this.manager = manager;
    }

    public void addPages(Map<String, Integer> heroAndNumOfPages, int gemFilter, PageType type) {
        pages = new ArrayList<>();
        for(Object key: heroAndNumOfPages.keySet()){
            int pageNum = heroAndNumOfPages.get(key);
            for (int i = 0; i < pageNum; i++) {
                pages.add(new CardPage((String) key, i, gemFilter, null, Have.NOT_MATTER, type,  handler));
            }
        }
        if (pages.size() == 0){
            message.setMessage("There isn't any card with " + gemFilter + " mana! Choose another filter to show");
            message.setImage(Assets.warning);
            message.setBackgroundColor(Constants.WARNING_COLOR);
            message.setShow(true);
        }else{
            currentPage = pages.get(0);
        }
    }

    public void addPages(Map<String, Integer> heroAndNumOfPages, Have open, PageType type) {
        pages = new ArrayList<>();
        for(Object key: heroAndNumOfPages.keySet()){
            int pageNum = heroAndNumOfPages.get(key);
            for (int i = 0; i < pageNum; i++) {
                pages.add(new CardPage((String) key, i, -1, null, open, type, handler));
            }
        }
        if (pages.size() == 0){
            message.setMessage("It is empty");
            message.setImage(Assets.warning);
            message.setBackgroundColor(Constants.WARNING_COLOR);
            message.setShow(true);
        }else{
            currentPage = pages.get(0);
        }
    }


    public void addPages(Map<String, Integer> heroAndNumOfPages, String nameFilter, PageType type) {
        pages = new ArrayList<>();
        for(Object key: heroAndNumOfPages.keySet()){
            int pageNum = heroAndNumOfPages.get(key);
            for (int i = 0; i < pageNum; i++) {
                pages.add(new CardPage((String) key, i, -1, nameFilter, Have.NOT_MATTER, type , handler));
            }
        }
        if (pages.size() == 0){
            message.setMessage("There isn't any card with " + nameFilter + " in name! Choose another filter to show");
            message.setImage(Assets.warning);
            message.setBackgroundColor(Constants.WARNING_COLOR);
            message.setShow(true);
        }else{
            currentPage = pages.get(0);
        }
    }

    public ArrayList<CardPage> getPages() {
        return pages;
    }

    public void setPages(ArrayList<CardPage> pages) {
        this.pages = pages;
    }

    public CardPage getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(CardPage currentPage) {
        this.currentPage = currentPage;
    }

    public Handler getHandler() {
        return handler;
    }

    public void setHandler(Handler handler) {
        this.handler = handler;
    }

    public BImageButton getNextPage() {
        return nextPage;
    }

    public void setNextPage(BImageButton nextPage) {
        this.nextPage = nextPage;
    }

    public BImageButton getPreviousPage() {
        return previousPage;
    }

    public void setPreviousPage(BImageButton previousPage) {
        this.previousPage = previousPage;
    }

    public BImageButton getOne() {
        return one;
    }

    public void setOne(BImageButton one) {
        this.one = one;
    }

    public BImageButton getTwo() {
        return two;
    }

    public void setTwo(BImageButton two) {
        this.two = two;
    }

    public BImageButton getThree() {
        return three;
    }

    public void setThree(BImageButton three) {
        this.three = three;
    }

    public BImageButton getFour() {
        return four;
    }

    public void setFour(BImageButton four) {
        this.four = four;
    }

    public BImageButton getFive() {
        return five;
    }

    public void setFive(BImageButton five) {
        this.five = five;
    }

    public BImageButton getSix() {
        return six;
    }

    public void setSix(BImageButton six) {
        this.six = six;
    }

    public BImageButton getSeven() {
        return seven;
    }

    public void setSeven(BImageButton seven) {
        this.seven = seven;
    }

    public BImageButton getEight() {
        return eight;
    }

    public void setEight(BImageButton eight) {
        this.eight = eight;
    }

    public BImageButton getNine() {
        return nine;
    }

    public void setNine(BImageButton nine) {
        this.nine = nine;
    }

    public BImageButton getTen() {
        return ten;
    }

    public void setTen(BImageButton ten) {
        this.ten = ten;
    }

    public BWarningMessage getMessage() {
        return message;
    }

    public void setMessage(BWarningMessage message) {
        this.message = message;
    }

    public int getCurrentPageNum() {
        return currentPageNum;
    }

    public void setCurrentPageNum(int currentPageNum) {
        this.currentPageNum = currentPageNum;
    }
}
