package ir.mrbenyamin.firestone.view.state.states.playstate.fieldcards;

import ir.mrbenyamin.firestone.play.Play;
import ir.mrbenyamin.firestone.player.Player;
import ir.mrbenyamin.firestone.view.Constants;
import ir.mrbenyamin.firestone.view.Handler;
import ir.mrbenyamin.firestone.view.graphicutils.cardview.HandCardView;

import java.util.ArrayList;

public class CurrentFieldCardsManager extends FieldCardsManager {


    public CurrentFieldCardsManager(Handler handler, Play play) {
        super(handler, play);
    }

    @Override
    public void updateCardViews() {
        Player player = play.getCurrentPlayer();
        manager.getObjects().removeAll(manager.getObjects());
        if (player.getOnFieldMinions()==null){
            player.setOnFieldMinions(new ArrayList<>());
        }
        for (int i = 0; i < player.getOnFieldMinions().size(); i++) {
            int finalI = i;
            HandCardView handCardView = new HandCardView(player.getOnFieldMinions().get(i),
                    player,
                    170 + i * 2 + i * (2 * Constants.CARD_VIEW_WIDTH / 3),
                    425 - 10 - 2 * Constants.CARD_VIEW_HEIGHTS / 3,
                    3 * Constants.CARD_VIEW_WIDTH / 5, 3 * Constants.CARD_VIEW_HEIGHTS / 5,
                    () -> {
                if (play.isMustSelectFriendlyCard()){
                    executeNeedSelectCardBattlecryOrSpell(player, finalI);
                }
                        if (play.getSpellActionCallerID() == -1 && play.getBattlecryActionCallerID() == -1) {
                            play.setOnFieldSelectedCard(player.getOnFieldMinions().get(finalI));
                            handler.getGame().getPlayState().setSelectedCardX(170 + finalI * 2 + finalI * (2 * Constants.CARD_VIEW_WIDTH / 3));
                            handler.getGame().getPlayState().setSelectedCardY(425 - 10 - 2 * Constants.CARD_VIEW_HEIGHTS / 3);
                        }
                    });
            manager.addObject(handCardView);
        }

    }

}
