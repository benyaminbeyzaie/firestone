package ir.mrbenyamin.firestone.view.state.states.playstate;

import ir.mrbenyamin.firestone.play.Play;
import ir.mrbenyamin.firestone.player.Player;
import ir.mrbenyamin.firestone.util.HibernateUtil;
import ir.mrbenyamin.firestone.view.Constants;
import ir.mrbenyamin.firestone.view.Game;
import ir.mrbenyamin.firestone.view.Handler;
import ir.mrbenyamin.firestone.view.graphicutils.BObjectsManager;
import ir.mrbenyamin.firestone.view.graphicutils.button.BTextButton;
import ir.mrbenyamin.firestone.view.graphicutils.label.BLabel;
import ir.mrbenyamin.firestone.view.state.State;
import ir.mrbenyamin.firestone.view.state.StateManager;
import org.hibernate.Session;

import java.awt.*;
import java.io.IOException;

public class WinAndLostState extends State {
    public WinAndLostState(Handler handler) {
        super(handler);
        setName("WinAndLostState");
        manager = new BObjectsManager(handler);
        BTextButton ok = new BTextButton("OK", 10, 40, 100, 20, () -> {
            StateManager.setCurrentState(handler.getGame().getMainState());
            handler.getGame().getDisplay().changeDisplaySize(Constants.STANDARD_STATES_WIDTH, Constants.STANDARD_STATES_HEIGHT);
        });
        manager.addObject(ok);
    }

    public void setWinnerLabel(Player winner) throws IOException {
        handler.getGame().getSignedPlayer().saveSignedPlayerInfo();
        handler.getGame().getPlayState().getPlay().getTimer().stop();
        Game.stopBackSound();
        handler.getGame().playBackSound("all");
        BLabel label = new BLabel("Player " + winner.getUsername() + " wins!", 10, 20, 300, 100, 18);
        manager.addObject(label);
    }

    @Override
    public void tick() {
        manager.tick();
    }

    @Override
    public void render(Graphics2D g) {
        g.setColor(Color.black);
        manager.render(g);
    }
}
