package ir.mrbenyamin.firestone.view.state.states.cardshowingstates.shopstate;

import ir.mrbenyamin.firestone.player.Player;
import ir.mrbenyamin.firestone.util.HibernateUtil;
import ir.mrbenyamin.firestone.view.Constants;
import ir.mrbenyamin.firestone.view.Handler;
import ir.mrbenyamin.firestone.view.assets.Assets;
import ir.mrbenyamin.firestone.view.display.MouseManager;
import ir.mrbenyamin.firestone.view.graphicutils.BObjectsManager;
import ir.mrbenyamin.firestone.view.graphicutils.ClickListener;
import ir.mrbenyamin.firestone.view.graphicutils.button.BImageButton;
import ir.mrbenyamin.firestone.view.state.State;
import ir.mrbenyamin.firestone.view.state.StateManager;
import ir.mrbenyamin.firestone.view.state.states.cardshowingstates.collectionsstate.cardpage.CardPageManager;
import ir.mrbenyamin.firestone.view.state.states.cardshowingstates.collectionsstate.cardpage.Have;
import ir.mrbenyamin.firestone.view.state.states.cardshowingstates.collectionsstate.cardpage.PageType;
import org.hibernate.Session;

import java.awt.*;
import java.io.IOException;
import java.util.Map;

public class ShopState extends State {
    private Player signedPlayer;
    private CardPageManager cardPageManager;
    private BObjectsManager manager;
    private BImageButton backButton;
    private SellPageManager sellPageManager;

    public ShopState(Handler handler) {
        super(handler);
        setName("Shop");
        signedPlayer = handler.getGame().getSignedPlayer();

        sellPageManager = new SellPageManager(handler);

        cardPageManager = new CardPageManager(handler, PageType.SHOP, this);
        manager = new BObjectsManager(handler);
        backButton = new BImageButton(10, 10, Assets.back[0].getWidth(), Assets.back[0].getHeight(), Assets.back, () -> {
            StateManager.setCurrentState(handler.getGame().getMainState());
            handler.getGame().getSignedPlayer().saveSignedPlayerInfo();
        });

        manager.addObject(backButton);
    }

    @Override
    public void tick() {

        manager.tick();
        if (MouseManager.mouseX >= 29 && MouseManager.mouseX <= 543 && MouseManager.mouseY >= 56 && MouseManager.mouseY <= 415){
            handler.getGame().getMouseManager().setBObjectManager(cardPageManager.getCurrentPage().getManager());
        }else if (MouseManager.mouseY > 415 && MouseManager.mouseX <= 543){
            handler.getGame().getMouseManager().setBObjectManager(cardPageManager.getManager());
        }else if (MouseManager.mouseX >= 550 && MouseManager.mouseY >= 56 && MouseManager.mouseY <= Constants.STANDARD_STATES_HEIGHT && sellPageManager.getCurrentPage() != null){
            // shop / sell manager
            handler.getGame().getMouseManager().setBObjectManager(sellPageManager.getCurrentPage().getManager());
        }else {
            handler.getGame().getMouseManager().setBObjectManager(manager);
        }
        cardPageManager.tick();
        sellPageManager.tick();

    }

    @Override
    public void render(Graphics2D g) {
        g.drawImage(Assets.collectionsBackground, 0, 0, null);
        g.drawImage(Assets.cardsBorder, 10, 40, null);
        g.drawImage(Assets.decksBorder, 570 , 40, null);
        manager.render(g);
        cardPageManager.render(g);
        sellPageManager.render(g);
    }


    public void setUpPages(Player signedPlayer, int gemFilter, PageType type) throws IOException {
        Map<String, Integer> heroAndNumOfCards = calculateEachHeroCards(gemFilter);
        Map<String, Integer> heroAndNumOfPages = calculateEachHeroPagesNumber(heroAndNumOfCards);
        cardPageManager.addPages(heroAndNumOfPages, gemFilter, type);
    }

    public void setUpPages(Player signedPlayer, String nameFilter, PageType type) throws IOException {
        Map<String, Integer> heroAndNumOfCards = calculateEachHeroCards(nameFilter);
        Map<String, Integer> heroAndNumOfPages = calculateEachHeroPagesNumber(heroAndNumOfCards);
        cardPageManager.addPages(heroAndNumOfPages, nameFilter, type);
    }

    public void setUpPages(Player signedPlayer, Have have, PageType type) throws IOException {
        Map<String, Integer> heroAndNumOfCards = calculateEachHeroCards(have);
        Map<String, Integer> heroAndNumOfPages = calculateEachHeroPagesNumber(heroAndNumOfCards);
        cardPageManager.addPages(heroAndNumOfPages, have, type);
    }





    public Player getSignedPlayer() {
        return signedPlayer;
    }

    public void setSignedPlayer(Player signedPlayer) {
        this.signedPlayer = signedPlayer;
    }

    public CardPageManager getCardPageManager() {
        return cardPageManager;
    }

    public void setCardPageManager(CardPageManager cardPageManager) {
        this.cardPageManager = cardPageManager;
    }

    @Override
    public BObjectsManager getManager() {
        return manager;
    }

    @Override
    public void setManager(BObjectsManager manager) {
        this.manager = manager;
    }

    public BImageButton getBackButton() {
        return backButton;
    }

    public void setBackButton(BImageButton backButton) {
        this.backButton = backButton;
    }

    public SellPageManager getSellPageManager() {
        return sellPageManager;
    }

    public void setSellPageManager(SellPageManager sellPageManager) {
        this.sellPageManager = sellPageManager;
    }
}
