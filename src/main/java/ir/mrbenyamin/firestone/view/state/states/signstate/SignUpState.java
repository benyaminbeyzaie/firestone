package ir.mrbenyamin.firestone.view.state.states.signstate;

import ir.mrbenyamin.firestone.player.sign.SignUp;
import ir.mrbenyamin.firestone.view.assets.Assets;
import ir.mrbenyamin.firestone.view.graphicutils.button.BImageButton;
import ir.mrbenyamin.firestone.view.graphicutils.button.BTextButton;
import ir.mrbenyamin.firestone.view.graphicutils.message.BWarningMessage;
import ir.mrbenyamin.firestone.view.state.State;
import ir.mrbenyamin.firestone.view.Constants;
import ir.mrbenyamin.firestone.view.Handler;
import ir.mrbenyamin.firestone.view.graphicutils.*;
import ir.mrbenyamin.firestone.view.graphicutils.label.BLabel;
import ir.mrbenyamin.firestone.view.graphicutils.textbox.BPasswordTextBox;
import ir.mrbenyamin.firestone.view.graphicutils.textbox.BTextBox;
import ir.mrbenyamin.firestone.view.state.StateManager;

import java.awt.*;
import java.io.IOException;

public class SignUpState extends SignState implements ClickListener {

    private static SignUpState signUpState;
    private SignUp signUp;
    private BObjectsManager manager;
    private BTextBox userNameTextBox;
    private BPasswordTextBox passwordTextBox;
    private BLabel userLabel, passLabel;
    private BTextButton signUpButton;
    private BWarningMessage warning;
    private BImageButton backButton;


    public static SignUpState getInstance(Handler handler) {
        if (signUpState == null) signUpState = new SignUpState(handler);
        return signUpState;
    }

    private SignUpState(Handler handler) {
        super(handler);
        setName("Sign up");
        manager = new BObjectsManager(handler);
        signUp = SignUp.getInstance();

        userLabel = new BLabel("Username: ", 439, 75, 100, 555, 12);
        userNameTextBox = new BTextBox(3 * (Constants.STANDARD_STATES_WIDTH / 4) - 125, 80, 250, 40, () -> {
            for (BObject o :
                    manager.getObjects()) {
                if (o instanceof BTextBox){
                    ((BTextBox) o).setUnderType(false);
                }
            }
            userNameTextBox.setUnderType(true);
        });
        passLabel = new BLabel("Password: ", 439, 145, 100, 555, 12);
        passwordTextBox = new BPasswordTextBox(3 * (Constants.STANDARD_STATES_WIDTH / 4) - 125, 150, 250, 40, () -> {
            for (BObject o :
                    manager.getObjects()) {
                if (o instanceof BTextBox){
                    ((BTextBox) o).setUnderType(false);
                }
            }
            passwordTextBox.setUnderType(true);
        });

        signUpButton = new BTextButton("Sign up", 3 * (Constants.STANDARD_STATES_WIDTH / 4) - 125, 200, 250, 50, () -> callSignUp());
        signUpButton.setButtonTextSize(20);

        backButton = new BImageButton(380, 8, 32, 32, Assets.back, () -> {
            handler.getGame().getSignInState().clearDate();
            StateManager.setCurrentState(handler.getGame().getSignInState());
        });

        warning = new BWarningMessage("", Assets.warning, 3 * (Constants.STANDARD_STATES_WIDTH / 4) - 125, 300, 250, 30, 2000000000l);


        manager.addObject(userNameTextBox);
        manager.addObject(passwordTextBox);
        manager.addObject(userLabel);
        manager.addObject(passLabel);
        manager.addObject(signUpButton);
        manager.addObject(warning);
        manager.addObject(backButton);

    }

    @Override
    public void clearDate() {
        this.getUserNameTextBox().setTypedString("");
        this.getPasswordTextBox().setTypedString("");
        this.getPasswordTextBox().setRenderedPass("");
    }

    private void callSignUp() {
        int temp = -1;
        try {
            temp = signUp.signUp(userNameTextBox.getTypedString(), passwordTextBox.getTypedString());
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (temp == 0) {
            // user is empty
            warning.setMessage("Username is empty!");
            warning.setImage(Assets.warning);
            warning.setBackgroundColor(Constants.WARNING_COLOR);
            warning.setShow(true);
        }else if (temp == 1){
            // pass is empty
            warning.setMessage("Password is empty!");
            warning.setImage(Assets.warning);
            warning.setBackgroundColor(Constants.WARNING_COLOR);
            warning.setShow(true);
        }else if (temp == 2){
            // user exists
            warning.setMessage("This user is already exists!");
            warning.setBackgroundColor(Constants.WARNING_COLOR);
            warning.setImage(Assets.error);
            warning.setShow(true);
        }else if (temp == 3){
            // Success
            warning.setMessage("User " + userNameTextBox.getTypedString() + " created successfully!");
            warning.setImage(null);
            warning.setBackgroundColor(Constants.BUTTON_COLOR_3);
            warning.setShow(true);
        }else if(temp == -1){
            // problem!!
        }
    }

    @Override
    public void tick() {
        manager.tick();
    }


    @Override
    public void render(Graphics2D g) {
        renderBack(g);
        manager.render(g);
    }

    @Override
    public void onClick() {


    }

    public static State getSignUpState() {
        return signUpState;
    }

    public static void setSignUpState(SignUpState signUpState) {
        SignUpState.signUpState = signUpState;
    }

    public SignUp getSignUp() {
        return signUp;
    }

    public void setSignUp(SignUp signUp) {
        this.signUp = signUp;
    }

    public BObjectsManager getManager() {
        return manager;
    }

    public void setManager(BObjectsManager manager) {
        this.manager = manager;
    }

    public BTextBox getUserNameTextBox() {
        return userNameTextBox;
    }

    public void setUserNameTextBox(BTextBox userNameTextBox) {
        this.userNameTextBox = userNameTextBox;
    }

    public BPasswordTextBox getPasswordTextBox() {
        return passwordTextBox;
    }

    public void setPasswordTextBox(BPasswordTextBox passwordTextBox) {
        this.passwordTextBox = passwordTextBox;
    }

    public BLabel getUserLabel() {
        return userLabel;
    }

    public void setUserLabel(BLabel userLabel) {
        this.userLabel = userLabel;
    }

    public BLabel getPassLabel() {
        return passLabel;
    }

    public void setPassLabel(BLabel passLabel) {
        this.passLabel = passLabel;
    }

    public BTextButton getSignUpButton() {
        return signUpButton;
    }

    public void setSignUpButton(BTextButton signUpButton) {
        this.signUpButton = signUpButton;
    }

    public BWarningMessage getWarning() {
        return warning;
    }

    public void setWarning(BWarningMessage warning) {
        this.warning = warning;
    }

    public BImageButton getBackButton() {
        return backButton;
    }

    public void setBackButton(BImageButton backButton) {
        this.backButton = backButton;
    }
}
