package ir.mrbenyamin.firestone.view.assets;

import ir.mrbenyamin.firestone.card.Card;

import javax.accessibility.AccessibleAction;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;

public class Assets {

    public static BufferedImage card, battleBackGround, signBackGround
            , mouseIcon, mainLeftImage, mainLogo, error, warning, fireStoneLogo,
            mainBackground, cardsBorder, decksBorder;
    public static BufferedImage playerNameBack, gemBack, line, playBack, playBack2;
    public static BufferedImage[] back = new BufferedImage[2];
    public static BufferedImage[] playButton, collectionsButton, settingButton, shopButton, exitButton, logOutButton,
    statusButton;
    public static BufferedImage collectionsBackground, decksImageLogo;
    public static BufferedImage[] nextPageImageButton, previousPageImageButton, redGem, searchButton, endTurn;
    public static BufferedImage[] Arcane_Breath, Bananas, Bazaar_Burglary, Corruption, Dreadscale, Friendly_Smith, Goldshire_Footman, Knife_juggler,
    Murloc_Tidecaller, Nightmare, Polymorph, Proud_Defender, Ragnaros_the_Firelord, River_Crocolisk, Sharkbait, Violet_Apprentice,
    Woodchip, Ysera_Awakens, Zarog_Crown, Tolin_Goblet, Aldor_Peacekeeper, Gnomish_Army_Knife, High_Priest_Amet, Madame_Lazul;
    public static BufferedImage[] Poisoned_Dagger, Battle_Axe, Plagued_Knife, Locust;
    public static BufferedImage[] Battle_Axe_OnField, Poisoned_Dagger_OnField, Plagued_Knife_OnField;
    public static BufferedImage[] Book_of_Specters, Curio_Collector, Learn_Draconic, Pharaohs_Blessing, Sathrovarr,
            Security_Rover, Sprint, Strength_in_Numbers, Swarm_of_Locusts, Tomb_Warden;
    public static BufferedImage MageImage, priestImage, warlockImage, paladinImage, rogueImage;
    public static File backgroundMusic1, backgroundMusic2, gameBackgroundMusic1, gameBackgroundMusic2, click;
    public static BufferedImage backCardOne, backCardTwo, backCardThree, backCardFour, backCardFive;

    public static void init(){

        backgroundMusic1 = new File("src\\main\\resources\\ir\\mrbenyamin\\firestone\\sounds\\billizardMusics\\pull-up-a-chair.wav");
        backgroundMusic2 = new File("src\\main\\resources\\ir\\mrbenyamin\\firestone\\sounds\\billizardMusics\\the-arena-awaits.wav");
        gameBackgroundMusic1 = new File("src\\main\\resources\\ir\\mrbenyamin\\firestone\\sounds\\billizardMusics\\heroes-of-warcraft.wav");
        gameBackgroundMusic2 = new File("src\\main\\resources\\ir\\mrbenyamin\\firestone\\sounds\\billizardMusics\\one-cast-chance.wav");

        click = new File("src\\main\\resources\\ir\\mrbenyamin\\firestone\\sounds\\click.wav");
        signBackGround = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\woodBackGround.jpg");
        battleBackGround = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\battleBackGround.jpg");
        card = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\card.png");
        mouseIcon = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\mouseIcon.png");
        mainLeftImage = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\mainLeftImage.png");
        mainLogo = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\mainLogo.png");
        back[0] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\back.png");
        back[1] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\backHover.png");
        warning = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\warning.png");
        error = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\error.png");
        fireStoneLogo = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\FIRESTONE.png");
        mainBackground = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\mainBackground.png");
        playButton = new BufferedImage[2];
        collectionsButton = new BufferedImage[2];
        shopButton = new BufferedImage[2];
        settingButton = new BufferedImage[2];
        exitButton = new BufferedImage[2];
        logOutButton = new BufferedImage[2];
        statusButton = new BufferedImage[2];
        playButton[0] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\playButton.png");
        playButton[1] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\playButtonHover.png");
        collectionsButton[0] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\collectionsButton.png");
        collectionsButton[1] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\collectionsButtonHover.png");
        shopButton[0] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\shopButton.png");
        shopButton[1] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\shopButtonHover.png");
        settingButton[0] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\settingButton.png");
        settingButton[1] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\settingButtonHover.png");
        exitButton[0] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\exitButton.png");
        exitButton [1] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\exitButtonHover.png");
        logOutButton[0] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\logOutButton.png");
        logOutButton [1] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\logOutButtonHover.png");
        statusButton[0] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\statusButton.png");
        statusButton [1] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\statusButtonHover.png");
        playerNameBack = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\playername.png");
        gemBack = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\gems.png");
        playBack = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\playBack.png");
        playBack2 = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\playBack2.png");

        nextPageImageButton = new BufferedImage[2];
        previousPageImageButton = new BufferedImage[2];
        redGem = new BufferedImage[2];
        collectionsBackground = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\collectionsBackground.png");
        decksImageLogo = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\decksImageLogo.png");
        nextPageImageButton[0] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\nextPageImageButton.png");
        previousPageImageButton[0] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\previousPageImageButton.png");
        redGem[0] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\redGem.png");
        nextPageImageButton[1] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\nextPageImageButtonHover.png");
        previousPageImageButton[1] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\previousPageImageButtonHover.png");
        redGem[1] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\redGem.png");
        cardsBorder = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\woodBorderCards.png");
        decksBorder = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\woodBorderDecks.png");

        searchButton = new BufferedImage[2];
        searchButton[0] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\searchImageButton.png");
        searchButton[1] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\searchImageButtonHover.png");

        endTurn = new BufferedImage[2];
        endTurn[0] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\endTurn.png");
        endTurn[1] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\endTurnHover.png");

        // Cards Image
        Arcane_Breath = new BufferedImage[2];
        Arcane_Breath[0] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Arcane Breath.png");
        Arcane_Breath[1] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Arcane Breath-b.png");

        Bananas = new BufferedImage[2];
        Bananas[0] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Bananas.png");
        Bananas[1] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Bananas-b.png");

        Bazaar_Burglary = new BufferedImage[2];
        Bazaar_Burglary[0] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Bazaar Burglary.png");
        Bazaar_Burglary[1] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Bazaar Burglary-b.png");

        Corruption = new BufferedImage[2];
        Corruption[0] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Corruption.png");
        Corruption[1] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Corruption-b.png");

        Dreadscale = new BufferedImage[2];
        Dreadscale[0] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Dreadscale.png");
        Dreadscale[1] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Dreadscale-b.png");

        Friendly_Smith = new BufferedImage[2];
        Friendly_Smith[0] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Friendly Smith.png");
        Friendly_Smith[1] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Friendly Smith-b.png");

        Goldshire_Footman = new BufferedImage[2];
        Goldshire_Footman[0] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Goldshire Footman.png");
        Goldshire_Footman[1] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Goldshire Footman-b.png");

        Knife_juggler = new BufferedImage[2];
        Knife_juggler[0] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Knife juggler.png");
        Knife_juggler[1] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Knife juggler-b.png");

        Murloc_Tidecaller = new BufferedImage[2];
        Murloc_Tidecaller[0] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Murloc Tidecaller.png");
        Murloc_Tidecaller[1] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Murloc Tidecaller-b.png");

        Nightmare = new BufferedImage[2];
        Nightmare[0] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Nightmare.png");
        Nightmare[1] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Nightmare-b.png");

        Polymorph = new BufferedImage[2];
        Polymorph[0] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Polymorph.png");
        Polymorph[1] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Polymorph-b.png");

        Proud_Defender = new BufferedImage[2];
        Proud_Defender[0] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Proud Defender.png");
        Proud_Defender[1] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Proud Defender-b.png");

        Ragnaros_the_Firelord = new BufferedImage[2];
        Ragnaros_the_Firelord[0] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Ragnaros the Firelord.png");
        Ragnaros_the_Firelord[1] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Ragnaros the Firelord-b.png");

        Sharkbait = new BufferedImage[2];
        Sharkbait[0] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Sharkbait.png");
        Sharkbait[1] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Sharkbait-b.png");

        Violet_Apprentice = new BufferedImage[2];
        Violet_Apprentice[0] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Violet Apprentice.png");
        Violet_Apprentice[1] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Violet Apprentice-b.png");

        Woodchip = new BufferedImage[2];
        Woodchip[0] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Woodchip.png");
        Woodchip[1] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Woodchip-b.png");

        Ysera_Awakens = new BufferedImage[2];
        Ysera_Awakens[0] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Ysera Awakens.png");
        Ysera_Awakens[1] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Ysera Awakens-b.png");

        Zarog_Crown = new BufferedImage[2];
        Zarog_Crown[0] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Zarog's Crown.png");
        Zarog_Crown[1] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Zarog's Crown-b.png");

        Tolin_Goblet  = new BufferedImage[2];
        Tolin_Goblet [0] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Tolin's Goblet.png");
        Tolin_Goblet [1] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Tolin's Goblet-b.png");

        River_Crocolisk = new BufferedImage[2];
        River_Crocolisk[0] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\River Crocolisk.png");
        River_Crocolisk[1] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\River Crocolisk-b.png");

        Aldor_Peacekeeper  = new BufferedImage[2];
        Aldor_Peacekeeper [0] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Aldor Peacekeeper.png");
        Aldor_Peacekeeper [1] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Aldor Peacekeeper-b.png");

        Gnomish_Army_Knife  = new BufferedImage[2];
        Gnomish_Army_Knife [0] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Gnomish Army Knife.png");
        Gnomish_Army_Knife [1] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Gnomish Army Knife-b.png");

        High_Priest_Amet  = new BufferedImage[2];
        High_Priest_Amet [0] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\High Priest Amet.png");
        High_Priest_Amet [1] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\High Priest Amet-b.png");

        Madame_Lazul  = new BufferedImage[2];
        Madame_Lazul [0] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Madame Lazul.png");
        Madame_Lazul [1] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Madame Lazul-b.png");

        Book_of_Specters  = new BufferedImage[2];
        Book_of_Specters [0] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Book of Specters.png");
        Book_of_Specters [1] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Book of Specters-b.png");

        Curio_Collector  = new BufferedImage[2];
        Curio_Collector [0] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Curio Collector.png");
        Curio_Collector [1] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Curio Collector-b.png");

        Learn_Draconic  = new BufferedImage[2];
        Learn_Draconic [0] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Learn Draconic.png");
        Learn_Draconic [1] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Learn Draconic-b.png");

        Pharaohs_Blessing  = new BufferedImage[2];
        Pharaohs_Blessing [0] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Pharaoh's Blessing.png");
        Pharaohs_Blessing [1] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Pharaoh's Blessing-b.png");

        Sathrovarr  = new BufferedImage[2];
        Sathrovarr [0] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Sathrovarr.png");
        Sathrovarr [1] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Sathrovarr-b.png");

        Security_Rover  = new BufferedImage[2];
        Security_Rover [0] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Security Rover.png");
        Security_Rover [1] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Security Rover-b.png");

        Sprint  = new BufferedImage[2];
        Sprint [0] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Sprint.png");
        Sprint [1] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Sprint-b.png");

        Strength_in_Numbers  = new BufferedImage[2];
        Strength_in_Numbers [0] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Strength in Numbers.png");
        Strength_in_Numbers [1] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Strength in Numbers-b.png");

        Swarm_of_Locusts  = new BufferedImage[2];
        Swarm_of_Locusts [0] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Swarm of Locusts.png");
        Swarm_of_Locusts [1] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Swarm of Locusts-b.png");

        Locust  = new BufferedImage[2];
        Locust [0] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Locust.png");
        Locust [1] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Locust-b.png");

        Tomb_Warden  = new BufferedImage[2];
        Tomb_Warden [0] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Tomb Warden.png");
        Tomb_Warden [1] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Tomb Warden-b.png");


        Poisoned_Dagger  = new BufferedImage[2];
        Poisoned_Dagger [0] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Poisoned Dagger.png");
        Poisoned_Dagger [1] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Poisoned Dagger-b.png");
        Poisoned_Dagger_OnField = new BufferedImage[2];
        Poisoned_Dagger_OnField[0] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Poisoned Dagger-onField.png");
        Poisoned_Dagger_OnField [1] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Poisoned Dagger-onField.png");

        Plagued_Knife  = new BufferedImage[2];
        Plagued_Knife [0] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Plagued Knife.png");
        Plagued_Knife [1] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Plagued Knife-b.png");
        Plagued_Knife_OnField = new BufferedImage[2];
        Plagued_Knife_OnField[0] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Plagued Knife-onField.png");
        Plagued_Knife_OnField [1] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Plagued Knife-onField.png");



        Battle_Axe  = new BufferedImage[2];
        Battle_Axe [0] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Battle Axe.png");
        Battle_Axe [1] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Battle Axe-b.png");
        Battle_Axe_OnField = new BufferedImage[2];
        Battle_Axe_OnField[0] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Battle Axe-onField.png");
        Battle_Axe_OnField [1] = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cards\\Battle Axe-onField.png");

        line =  ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\line.png");

        // Heroes
        MageImage = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\heroes\\mage.png");
        priestImage = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\heroes\\priest.png");
        paladinImage = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\heroes\\paladin.png");
        warlockImage = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\heroes\\warlock.png");
        rogueImage = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\heroes\\rogue.png");

        // Back Cards
        backCardOne = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cardBack\\1.png");
        backCardTwo = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cardBack\\2.png");
        backCardThree = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cardBack\\3.png");
        backCardFour = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cardBack\\4.png");
        backCardFive = ImageLoader.loadImage("src\\main\\resources\\ir\\mrbenyamin\\firestone\\images\\cardBack\\5.png");


    }

    public static BufferedImage[] getImage(String name) {
        switch (name){
            case "Arcane Breath": return Arcane_Breath;
            case "Bananas": return Bananas;
            case "Violet Apprentice": return Violet_Apprentice;
            case "Woodchip": return Woodchip;
            case "Bazaar Burglary": return Bazaar_Burglary;
            case "Corruption": return Corruption;
            case "Ysera Awakens": return Ysera_Awakens;
            case "Dreadscale": return Dreadscale;
            case "Friendly Smith": return Friendly_Smith;
            case "Goldshire Footman": return Goldshire_Footman;
            case "Knife Juggler": return Knife_juggler;
            case "Murloc Tidecaller": return Murloc_Tidecaller;
            case "Nightmare": return Nightmare;
            case "Polymorph": return Polymorph;
            case "Proud Defender": return Proud_Defender;
            case "Ragnaros the Firelord": return Ragnaros_the_Firelord;
            case "River Crocolisk": return River_Crocolisk;
            case "Sharkbait": return Sharkbait;
            case "Zarog's Crown": return Zarog_Crown;
            case "Tolin's Goblet": return Tolin_Goblet;
            case "Aldor Peacekeeper": return Aldor_Peacekeeper;
            case "Gnomish Army Knife": return Gnomish_Army_Knife;
            case "High Priest Amet": return High_Priest_Amet;
            case "Madame Lazul": return Madame_Lazul;
            case "Poisoned Daggers" : return Poisoned_Dagger;
            case "Plagued Knife" : return Plagued_Knife;
            case "Battle Axe" : return Battle_Axe;
            case "Book of Specters" : return Book_of_Specters;
            case "Curio Collector" : return Curio_Collector;
            case "Learn Draconic" : return Learn_Draconic;
            case "Pharaoh's Blessing" : return Pharaohs_Blessing;
            case "Sathrovarr" : return Sathrovarr;
            case "Security Rover" : return Security_Rover;
            case "Sprint" : return Sprint;
            case "Strength in Numbers" : return Strength_in_Numbers;
            case "Swarm of Locusts" : return Swarm_of_Locusts;
            case "Tomb Warden" : return Tomb_Warden;
            case "Locust" : return Locust;
        }
        return null;
    }

    public static BufferedImage[] getWeaponImage(String name) {
        switch (name){
            case "Battle Axe": return Battle_Axe_OnField;
            case "Poisoned Daggers": return Poisoned_Dagger_OnField;
            case "Plagued Knife" : return Plagued_Knife_OnField;
        }
        return null;
    }

    public static BufferedImage getHeroImage(String name) {
        switch (name){
            case "Mage": return MageImage;
            case "Priest" : return priestImage;
            case "Paladin" : return paladinImage;
            case "Rogue" : return rogueImage;
            case "Warlock" : return warlockImage;
        }
        return null;
    }

    public static BufferedImage getBackCardImage(String name) {
        switch (name){
            case "one": return backCardOne;
            case "two": return backCardTwo;
            case "three": return backCardThree;
            case "four": return backCardFour;
            case "five": return backCardFive;
        }
        return null;
    }

    public static BufferedImage getGameBackgroundImage(String name) {
        switch (name){
            case "one": return playBack;
            case "two": return playBack2;
        }
        return null;
    }

    public static File getPlayBackgroundSound(String name) {
        switch (name){
            case "one": return gameBackgroundMusic1;
            case "two": return gameBackgroundMusic2;
        }
        return null;
    }

    public static File getSound(String name) {
        if (name.equals("one")){
            return backgroundMusic1;
        }else if (name.equals("two")){
            return backgroundMusic2;
        }
        return null;
    }


}
