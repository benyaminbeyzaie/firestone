package ir.mrbenyamin.firestone.view;

import ir.mrbenyamin.firestone.setting.Setting;

import java.awt.*;

public class Constants {
    public static String TITLE;
    public static int STANDARD_STATES_WIDTH;
    public static int STANDARD_STATES_HEIGHT;
    public static int PLAY_WIDTH;
    public static int PLAY_HEIGHT;
    public static String DECK_FILE_ADDRESS = "src\\main\\resources\\ir\\mrbenyamin\\firestone\\deck\\deck.json";
    public static final String CARD_PATH = "src\\main\\java\\ir\\mrbenyamin\\firestone\\card\\cardsjsonfiles\\";

    public final static int CARD_VIEW_WIDTH = 120;
    public final static int CARD_VIEW_HEIGHTS = 160;


    public static  Color BUTTON_COLOR_1;
    public static  Color BUTTON_COLOR_2;
    public static  Color BUTTON_COLOR_3;
    public static  Color BUTTON_COLOR_4;
    public static  Color WARNING_COLOR;
    public static  Color WARNING_FONT_COLOR;
    public static  Color LABEL_WHITE;
    public static  Color HEADER_TEXT;

    public static Font CARDS_TEXT_FONT;
    public static Font COLLECTIONS_CARDS_TEXT_FONT;

    static void init(Setting setting){
        TITLE = setting.getTITLE();
        STANDARD_STATES_WIDTH = setting.getSTANDARD_STATES_WIDTH();
        STANDARD_STATES_HEIGHT = setting.getSTANDARD_STATES_HEIGHT();
        PLAY_WIDTH = setting.getPLAY_WIDTH();
        PLAY_HEIGHT = setting.getPLAY_HEIGHT();

        BUTTON_COLOR_1 = setting.getBUTTON_COLOR_1();
        BUTTON_COLOR_2 = setting.getBUTTON_COLOR_2();
        BUTTON_COLOR_3 = setting.getBUTTON_COLOR_3();
        BUTTON_COLOR_4 = setting.getBUTTON_COLOR_4();
        WARNING_COLOR = setting.getWARNING_COLOR();
        WARNING_FONT_COLOR = setting.getWARNING_FONT_COLOR();
        LABEL_WHITE = setting.getLABEL_WHITE();
        HEADER_TEXT =setting.getHEADER_TEXT();

        CARDS_TEXT_FONT = setting.getCARD_TEXT_FONT();
        COLLECTIONS_CARDS_TEXT_FONT = setting.getCOLLECTIONS_CARD_TEXT_FONT();
    }

    // rendering things
    private static final int FPS = 60;
    static final double TIMER_PER_TICK = 1000000000 / FPS;

    // play things
    public static final int DECK_SIZE = 10;
    public static final int MAX_DECK_CARD_SIZE = 15;

}
