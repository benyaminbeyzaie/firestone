package ir.mrbenyamin.firestone.view;

import ir.mrbenyamin.firestone.player.Player;
import ir.mrbenyamin.firestone.setting.Setting;
import ir.mrbenyamin.firestone.setting.SettingLoader;
import ir.mrbenyamin.firestone.view.state.*;
import ir.mrbenyamin.firestone.view.state.states.cardshowingstates.collectionsstate.CollectionsState;
import ir.mrbenyamin.firestone.view.state.states.cardshowingstates.shopstate.ShopState;
import ir.mrbenyamin.firestone.view.state.states.playstate.PlayState;
import ir.mrbenyamin.firestone.view.state.states.playstate.WinAndLostState;
import ir.mrbenyamin.firestone.view.state.states.signstate.SignInState;
import ir.mrbenyamin.firestone.view.state.states.signstate.SignUpState;
import ir.mrbenyamin.firestone.view.assets.Assets;
import ir.mrbenyamin.firestone.view.display.Display;
import ir.mrbenyamin.firestone.view.display.KeyBoardManager;
import ir.mrbenyamin.firestone.view.display.MouseManager;
import ir.mrbenyamin.firestone.view.state.states.*;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.xml.stream.XMLStreamException;
import java.awt.*;
import java.awt.image.BufferStrategy;
import java.io.File;
import java.io.IOException;

public class Game {
    private static Game game;
    private static File backMusic;
    private boolean running;
    private Player signedPlayer;
    private BufferStrategy bs;
    private Graphics2D graphics2D;
    private Display display;
    private PlayState playState;
    private ShopState shopState;
    private CollectionsState collectionsState;
    private SettingState settingState;
    private MainState mainState;
    private SignUpState signUpState;
    private SignInState signInState;
    private StatusState statusState;
    private WinAndLostState winAndLostState;
    private MouseManager mouseManager;
    private KeyBoardManager keyBoardManager;
    private static Handler handler;
    private static String LOG_NAME;
    private static Clip clip;
    private Setting setting;



    public static Game getInstance() throws IOException, XMLStreamException {
        if (game == null){
            game = new Game();
        }
        return game;
    }

    private Game() throws IOException, XMLStreamException {
        running = true;
        mouseManager = new MouseManager();
        keyBoardManager = new KeyBoardManager();
        setting = SettingLoader.getInstance().loadSetting("src\\main\\resources\\ir\\mrbenyamin\\firestone\\setting\\setting.xml");

        init();
    }

    private void init() throws IOException {
        Assets.init();
        Constants.init(setting);
        // create new handler
        handler = new Handler(this);

        display = Display.getInstance(handler);
        // Add mouse listener
        display.getFrame().addMouseListener(mouseManager);
        display.getFrame().addMouseMotionListener(mouseManager);
        display.getCanvas().addMouseListener(mouseManager);
        display.getCanvas().addMouseMotionListener(mouseManager);

        // Add keyboard listener
        display.getFrame().addKeyListener(keyBoardManager);
        display.getCanvas().addKeyListener(keyBoardManager);

        // setup sign States!
        signUpState = SignUpState.getInstance(handler);
        signInState = SignInState.getInstance(handler);

        // add mouse manager!
        handler.getGame().getMouseManager().setBObjectManager(signInState.getManager());
        handler.getGame().getKeyBoardManager().setBObjectManager(signInState.getManager());

        // set first state
        StateManager.setCurrentState(signInState);
    }

    public void setUpMustSignedStates(){
        if (signedPlayer == null){

        }else {
            mainState = new MainState(handler);
            playState = new PlayState(handler);
            shopState = new ShopState(handler);
            settingState = new SettingState(handler);
            collectionsState = new CollectionsState(handler);
            statusState = new StatusState(handler);
            winAndLostState = new WinAndLostState(handler);
        }
    }

    public void run(){
        double timePerTick = Constants.TIMER_PER_TICK;
        double delta = 0;
        long now;
        long lastTime = System.nanoTime();
        long timer = 0;
        int ticks = 0;

        while(running){
            now = System.nanoTime();
            delta += (now - lastTime) / timePerTick;
            timer += now - lastTime;
            lastTime = now;

            if(delta >= 1){
                tick();
                render();
                ticks++;
                delta--;
            }

            if(timer >= 1000000000){
                System.out.println("Ticks and Frames: " + ticks);
                ticks = 0;
                timer = 0;
            }
        }
    }

    private void tick(){
        if(StateManager.getCurrentState() != null)
            StateManager.getCurrentState().tick();
    }

    private void render(){
        bs = display.getCanvas().getBufferStrategy();
        if(bs == null){
            display.getCanvas().createBufferStrategy(3);
            return;
        }
        if (bs.getDrawGraphics() == null){
            return;
        }
        graphics2D = (Graphics2D) bs.getDrawGraphics();
        // This method is for strings
        graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        // this method works for quality but reduce fps!!
        // graphics2D.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);

        //Clear Screen
        graphics2D.clearRect(0, 0, display.getFrame().getWidth(), display.getFrame().getHeight());

        //Draw Here!
        if(StateManager.getCurrentState() != null)
            StateManager.getCurrentState().render(graphics2D);

        graphics2D.drawImage(Assets.mouseIcon, MouseManager.mouseX - 7, MouseManager.mouseY - 10, 40,40, null);

        //End Drawing!
        bs.show();
        graphics2D.dispose();
    }

    public static void playSound(final File file) {
        try {
            Clip clip = AudioSystem.getClip();
            AudioInputStream inputStream = AudioSystem.getAudioInputStream(file);
            clip.open(inputStream);

            clip.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void playBackSound(String state) {
        if (handler.getGame().getSignedPlayer().isSoundPlay()) {
            try {
                clip = AudioSystem.getClip();
                AudioInputStream inputStream = null;
                if (state.equals("play")){
                    inputStream = AudioSystem.getAudioInputStream(Assets.getPlayBackgroundSound(signedPlayer.getGameMusicName()));
                }else {
                    inputStream = AudioSystem.getAudioInputStream(Assets.getSound(signedPlayer.getBackMusicName()));
                }
                clip.open(inputStream);
                // 50000
                if (!state.equals("play") && signedPlayer.getBackMusicName().equals("one"))
                clip.setLoopPoints(660000, -1);
                else clip.setLoopPoints(0, -1);
                clip.loop(Clip.LOOP_CONTINUOUSLY);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void stopBackSound() {
        try {
            //if (backMusic == null) backMusic = Assets.backgroundMusic;
            // 50000
            if (clip !=null)
            if (clip.isOpen())
            clip.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public MouseManager getMouseManager() {
        return mouseManager;
    }

    public static Game getGame() {
        return game;
    }

    public static void setGame(Game game) {
        Game.game = game;
    }

    public boolean isRunning() {
        return running;
    }

    public void setRunning(boolean running) {
        this.running = running;
    }

    public Player getSignedPlayer() {
        return signedPlayer;
    }

    public void setSignedPlayer(Player signedPlayer) {
        this.signedPlayer = signedPlayer;
    }

    public BufferStrategy getBs() {
        return bs;
    }

    public void setBs(BufferStrategy bs) {
        this.bs = bs;
    }

    public Graphics2D getGraphics2D() {
        return graphics2D;
    }

    public void setGraphics2D(Graphics2D graphics2D) {
        this.graphics2D = graphics2D;
    }

    public Display getDisplay() {
        return display;
    }

    public void setDisplay(Display display) {
        this.display = display;
    }

    public PlayState getPlayState() {
        return playState;
    }

    public void setPlayState(PlayState playState) {
        this.playState = playState;
    }

    public ShopState getShopState() {
        return shopState;
    }

    public void setShopState(ShopState shopState) {
        this.shopState = shopState;
    }

    public CollectionsState getCollectionsState() {
        return collectionsState;
    }

    public void setCollectionsState(CollectionsState collectionsState) {
        this.collectionsState = collectionsState;
    }

    public SettingState getSettingState() {
        return settingState;
    }

    public void setSettingState(SettingState settingState) {
        this.settingState = settingState;
    }

    public MainState getMainState() {
        return mainState;
    }

    public void setMainState(MainState mainState) {
        this.mainState = mainState;
    }

    public SignUpState getSignUpState() {
        return signUpState;
    }

    public void setSignUpState(SignUpState signUpState) {
        this.signUpState = signUpState;
    }

    public SignInState getSignInState() {
        return signInState;
    }

    public void setSignInState(SignInState signInState) {
        this.signInState = signInState;
    }

    public void setMouseManager(MouseManager mouseManager) {
        this.mouseManager = mouseManager;
    }

    public KeyBoardManager getKeyBoardManager() {
        return keyBoardManager;
    }

    public void setKeyBoardManager(KeyBoardManager keyBoardManager) {
        this.keyBoardManager = keyBoardManager;
    }

    public static Handler getHandler() {
        return handler;
    }

    public static void setHandler(Handler handler) {
        Game.handler = handler;
    }

    public StatusState getStatusState() {
        return statusState;
    }

    public void setStatusState(StatusState statusState) {
        this.statusState = statusState;
    }

    public static Clip getClip() {
        return clip;
    }

    public static void setClip(Clip clip) {
        Game.clip = clip;
    }

    public static File getBackMusic() {
        return backMusic;
    }

    public static void setBackMusic(File backMusic) {
        Game.backMusic = backMusic;
    }

    public static String getLogName() {
        return LOG_NAME;
    }

    public static void setLogName(String logName) {
        LOG_NAME = logName;
    }

    public Setting getSetting() {
        return setting;
    }

    public void setSetting(Setting setting) {
        this.setting = setting;
    }

    public WinAndLostState getWinAndLostState() {
        return winAndLostState;
    }

    public void setWinAndLostState(WinAndLostState winAndLostState) {
        this.winAndLostState = winAndLostState;
    }
}
