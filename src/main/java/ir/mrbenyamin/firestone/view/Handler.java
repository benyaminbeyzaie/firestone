package ir.mrbenyamin.firestone.view;

import ir.mrbenyamin.firestone.view.display.KeyBoardManager;
import ir.mrbenyamin.firestone.view.display.MouseManager;

public class Handler {
    private Game game;

    public Handler(Game game) {
        this.game = game;
    }

    public Game getGame() {
        return game;
    }
}
