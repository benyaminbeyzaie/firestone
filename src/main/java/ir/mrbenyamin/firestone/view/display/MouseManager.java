package ir.mrbenyamin.firestone.view.display;

import ir.mrbenyamin.firestone.view.graphicutils.BObjectsManager;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.IOException;

public class MouseManager implements MouseListener, MouseMotionListener {
    public static int mouseX;
    public static int mouseY;
    private static boolean isPressed, isReleased;
    private BObjectsManager manager;

    public void setBObjectManager(BObjectsManager manager){
        this.manager = manager;
    }

    @Override
    public void mousePressed(MouseEvent e) {
        if (e.getButton() == MouseEvent.BUTTON1) isPressed = true;
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        if (e.getButton() == MouseEvent.BUTTON1){
            if (isPressed) isReleased = true;
        }

        if(manager != null) {
            try {
                manager.onMouseRelease(e);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        mouseX = e.getX();
        mouseY = e.getY();

        if(manager != null)
            manager.onMouseMove(e);
    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void mouseDragged(MouseEvent e) {

    }

    public BObjectsManager getManager() {
        return manager;
    }
}
