package ir.mrbenyamin.firestone.view.display;

import ir.mrbenyamin.firestone.view.graphicutils.BObjectsManager;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class KeyBoardManager implements KeyListener {
    private static boolean[] keys = new boolean[256];
    private static char[] keysChar = new char[256];

    private BObjectsManager manager;

    public void setBObjectManager(BObjectsManager manager){
        this.manager = manager;
    }

    @Override
    public void keyTyped(KeyEvent e) {
        if(manager != null)
            manager.onKeyTyped(e);
    }

    @Override
    public void keyPressed(KeyEvent e) {
        keys[e.getKeyCode()] = true;
        keysChar[e.getKeyCode()] = e.getKeyChar();
    }

    @Override
    public void keyReleased(KeyEvent e) {
        keys[e.getKeyCode()] = false;
    }
}
