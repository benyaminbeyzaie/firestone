package ir.mrbenyamin.firestone;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import ir.mrbenyamin.firestone.card.Card;
import ir.mrbenyamin.firestone.card.Minion;
import ir.mrbenyamin.firestone.card.Spell;
import ir.mrbenyamin.firestone.card.Weapon;
import ir.mrbenyamin.firestone.heroes.Hero;
import ir.mrbenyamin.firestone.player.Player;
import ir.mrbenyamin.firestone.security.Security;
import ir.mrbenyamin.firestone.util.HibernateUtil;
import ir.mrbenyamin.firestone.view.Constants;
import org.hibernate.Session;
import org.hibernate.Session;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;


public class TestClient {

    public static void main(String[] args) throws IOException {
        Session session = HibernateUtil.getSessionFactory().openSession();
        String pass = Security.hashPassword("a");
        Player player = new Player("c", pass);
        final DateFormat dff = new SimpleDateFormat("yyMMddhhmmss");
        player.setStringID(dff.format(new Date()));
        player.setDefaults(session);
        Minion minion = Minion.createAMinionFromJson("Woodchip");

        session.beginTransaction();
        session.save(player);
        session.getTransaction().commit();
    }
}
