package ir.mrbenyamin.firestone.log;


import java.io.IOException;
import java.util.MissingResourceException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MyLogger extends Logger {
    protected MyLogger(String name, String resourceBundleName) {
        super(name, resourceBundleName);
    }

    public static Logger getInstance(String signedPlayerUsername) throws IOException {
        Logger logger = Logger.getLogger(GLOBAL_LOGGER_NAME);
        logger.setUseParentHandlers(false);
        FileHandler fileHandler = new FileHandler("src\\main\\java\\ir\\mrbenyamin\\firestone\\log\\logfiles\\" + signedPlayerUsername + ".log", true);
        logger.addHandler(fileHandler);
        MyFormatter formatter = new MyFormatter();
        fileHandler.setFormatter(formatter);
        return logger;
    }

    public static void writeLog(String signedPlayerUsername, String log, Level level) throws IOException {
        Logger logger = Logger.getLogger(GLOBAL_LOGGER_NAME);
        logger.setUseParentHandlers(false);
        FileHandler fileHandler = new FileHandler("src\\main\\java\\ir\\mrbenyamin\\firestone\\log\\logfiles\\" + signedPlayerUsername + ".log", true);
        logger.addHandler(fileHandler);
        MyFormatter formatter = new MyFormatter();
        fileHandler.setFormatter(formatter);
        logger.log(level, log);
        fileHandler.close();
    }

}

