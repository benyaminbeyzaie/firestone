package ir.mrbenyamin.firestone.play.deckreader;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import ir.mrbenyamin.firestone.card.*;
import ir.mrbenyamin.firestone.heroes.Hero;
import ir.mrbenyamin.firestone.play.Play;
import ir.mrbenyamin.firestone.player.Player;
import ir.mrbenyamin.firestone.player.sign.De;
import ir.mrbenyamin.firestone.view.Constants;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class DeckReader {
    private static DeckReader deckReader;

    private DeckReader(){

    }

    public static DeckReader newInstance(){
        if (deckReader == null) deckReader = new DeckReader();
        return deckReader;
    }
    public void readFrom(String deckFileAddress, Player friend, Player enemy) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        File deckFile = new File(deckFileAddress);
        DeckFromJson deckFromJson = objectMapper.readValue(deckFile, DeckFromJson.class);
        Hero heroOne = Hero.createHero("Mage");
        Hero heroTwo = Hero.createHero("Mage");
        ArrayList<Card> friendCards = new ArrayList<>();
        makeDeckCards(friendCards, deckFromJson.getFriendDeck());
        ArrayList<Card> enemyCards = new ArrayList<>();
        makeDeckCards(enemyCards, deckFromJson.getEnemyDeck());

        Deck friendDeck = new Deck("friendDeck", heroOne, friendCards);
        Deck enemyDeck = new Deck("enemyDeck", heroTwo, enemyCards);
        friend.setPlayingDeck(friendDeck);
        enemy.setPlayingDeck(enemyDeck);
    }

    private void makeDeckCards(ArrayList<Card> cards, ArrayList<String> strings) throws IOException {
        for (String s :
                strings) {
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            Card card = objectMapper.readValue(new File(Constants.CARD_PATH + s + ".json"), Card.class);
            if (card.getType().equals("Minion")){
                Minion minion = objectMapper.readValue(new File(Constants.CARD_PATH + s + ".json"), Minion.class);
                cards.add(minion);
            }else if (card.getType().equals("Spell")){
                Spell spell = objectMapper.readValue(new File(Constants.CARD_PATH + s + ".json"), Spell.class);
                cards.add(spell);
            } else if (card.getType().equals("Weapon")){
                Weapon weapon = objectMapper.readValue(new File(Constants.CARD_PATH + s + ".json"), Weapon.class);
                cards.add(weapon);
            }
        }
    }

    public static Player getFriend() {
        return null;
    }

    public static Player getEnemy() {
        return null;
    }
}
