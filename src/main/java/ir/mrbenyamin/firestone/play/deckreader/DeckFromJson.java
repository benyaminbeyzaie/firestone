package ir.mrbenyamin.firestone.play.deckreader;

import java.util.ArrayList;

public class DeckFromJson {
    private ArrayList<String> friendDeck, enemyDeck;

    public DeckFromJson() {
    }

    public ArrayList<String> getFriendDeck() {
        return friendDeck;
    }

    public void setFriendDeck(ArrayList<String> friendDeck) {
        this.friendDeck = friendDeck;
    }

    public ArrayList<String> getEnemyDeck() {
        return enemyDeck;
    }

    public void setEnemyDeck(ArrayList<String> enemyDeck) {
        this.enemyDeck = enemyDeck;
    }
}
