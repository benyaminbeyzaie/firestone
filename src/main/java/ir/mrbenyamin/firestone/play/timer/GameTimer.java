package ir.mrbenyamin.firestone.play.timer;

import ir.mrbenyamin.firestone.play.Play;
import ir.mrbenyamin.firestone.view.Constants;
import ir.mrbenyamin.firestone.view.Handler;

import java.io.IOException;

public class GameTimer extends Thread {
    long timeOfPlayer = 60;
    private Play play;
    private Handler handler;
    public GameTimer(Handler handler, Play play){
        this.handler = handler;
        this.play = play;
    }

    public void run(){
        try {
            startTiming();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void startTiming() throws IOException {
        double timePerTick = 1000000000.0 / 60;
        double delta = 0;
        long now;
        long lastTime = System.nanoTime();
        long timer = 0;

        while(true){
            now = System.nanoTime();
            delta += (now - lastTime) / timePerTick;
            timer += now - lastTime;
            lastTime = now;

            if(delta >= 1){
                delta--;
            }

            if(timer >= 1000000000){
                timeOfPlayer -= 1;
                handler.getGame().getPlayState().setTimeToEnd((int) (timeOfPlayer));
                timer = 0;
            }
            if (timeOfPlayer <= 0){
                handler.getGame().getPlayState().endThings();
                timeOfPlayer = 60;
            }
        }
    }

    public long getTimeOfPlayer() {
        return timeOfPlayer;
    }

    public void setTimeOfPlayer(long timeOfPlayer) {
        this.timeOfPlayer = timeOfPlayer;
    }
}
