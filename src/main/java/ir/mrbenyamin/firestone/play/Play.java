package ir.mrbenyamin.firestone.play;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import ir.mrbenyamin.firestone.card.*;
import ir.mrbenyamin.firestone.heroes.Hero;
import ir.mrbenyamin.firestone.log.MyLogger;
import ir.mrbenyamin.firestone.play.action.*;
import ir.mrbenyamin.firestone.play.action.attack.*;
import ir.mrbenyamin.firestone.play.action.summon.SummonMinion;
import ir.mrbenyamin.firestone.play.action.summon.SummonSpell;
import ir.mrbenyamin.firestone.play.action.summon.SummonWeapon;
import ir.mrbenyamin.firestone.play.deckreader.DeckReader;
import ir.mrbenyamin.firestone.play.timer.GameTimer;
import ir.mrbenyamin.firestone.player.Player;
import ir.mrbenyamin.firestone.view.Constants;
import ir.mrbenyamin.firestone.view.Handler;
import ir.mrbenyamin.firestone.view.assets.Assets;
import ir.mrbenyamin.firestone.view.state.StateManager;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;

public class Play {
    private static final int FIELD_MINIONS_SIZE = 7;
    private Player playerOne;
    private Player playerTwo;
    private Player currentPlayer;
    private Player opponentPlayer;
    private ArrayList<Action> actions;
    private static int turn = 0;
    private static boolean isPlaying;
    private Card onHandSelectedCard;
    private Card onFieldSelectedCard;
    private GameTimer timer;
    private Handler handler;
    private int mode;
    private int spellActionCallerID = -1;
    private int battlecryActionCallerID = -1;
    private Card nightmareCard = null;
    private Card corruptionCard = null;
    private Player turnForCorruption = null;
    private boolean isSomeBodyWins = false;

    // spell actions
    private boolean mustSelectEnemyCard = false;
    private boolean mustSelectFriendlyCard = false;

    public Play(Handler handler, Player playerOne, int mode) throws IOException {
        this.handler = handler;
        this.playerOne = playerOne;
        this.mode = mode;
        if (mode == 0) {
            this.playerTwo = handler.getGame().getPlayState().getImaginaryPlayer();
            setRandomCurrentPlayer();
        }
        if (mode == 1) {
            // deck reader mode
            this.playerTwo = handler.getGame().getPlayState().getImaginaryPlayer();
            DeckReader deckReader = DeckReader.newInstance();
            deckReader.readFrom(Constants.DECK_FILE_ADDRESS, playerOne, playerTwo);
            currentPlayer = playerOne;
            opponentPlayer = playerTwo;
        }

        isPlaying = true;
        actions = new ArrayList<>();
        onHandSelectedCard = null;
        onFieldSelectedCard = null;
    }

    private void setRandomCurrentPlayer() {
        Random random = new Random();
        int rand = random.nextInt(2);
        if (rand == 0) {
            currentPlayer = playerOne;
            opponentPlayer = playerTwo;
        } else if (rand == 1) {
            currentPlayer = playerTwo;
            opponentPlayer = playerOne;
        }
    }

    private Deck getCopyOfDeck(Deck currentDeck) throws IOException {
        ArrayList<Card> cards = new ArrayList<>();
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        for (Card card :
                currentDeck.getCards()) {
            File file = new File(Card.CARDS_PATH + "\\" + card.getName() + ".json");
            if (card.getType().equals("Minion")) {
                Minion newCard = objectMapper.readValue(file, Minion.class);
                cards.add(newCard);
            } else if (card.getType().equals("Spell")) {
                Spell newCard = objectMapper.readValue(file, Spell.class);
                cards.add(newCard);
            } else if (card.getType().equals("Weapon")) {
                Weapon weapon = objectMapper.readValue(file, Weapon.class);
                cards.add(weapon);
            }
        }
        File file = new File(Hero.HERO_FILE_PATH + currentDeck.getHero().getName() + ".json");
        Hero hero = objectMapper.readValue(file, Hero.class);
        Deck copy = new Deck(currentDeck.getName(), hero, cards);
        return copy;
    }

    private void changeTurn() {
        if (currentPlayer == playerOne) {
            currentPlayer = playerTwo;
            opponentPlayer = playerOne;
        } else if (currentPlayer == playerTwo) {
            currentPlayer = playerOne;
            opponentPlayer = playerTwo;
        }
    }

    private void checkForLost() {
        if (!isSomeBodyWins){
            if (playerOne.getPlayingDeck().getHero().getBlood() <= 0) {
                wins(playerTwo);
                if (mode == 0){
                    playerTwo.getCurrentDeck().setWinsCount(
                            playerTwo.getCurrentDeck().getWinsCount() + 1
                    );
                }
                isSomeBodyWins = true;
            }else if (playerTwo.getPlayingDeck().getHero().getBlood() <= 0){
                wins(playerOne);
                if (mode == 0){
                    playerOne.getCurrentDeck().setWinsCount(
                            playerOne.getCurrentDeck().getWinsCount() + 1
                    );
                }
                isSomeBodyWins = true;
            }
            if (isSomeBodyWins && mode == 0){
                playerTwo.getCurrentDeck().setAllGamesCount(
                        playerTwo.getCurrentDeck().getAllGamesCount() + 1
                );
                playerOne.getCurrentDeck().setAllGamesCount(
                        playerOne.getCurrentDeck().getAllGamesCount() + 1
                );
            }
            handler.getGame().getSignedPlayer().saveSignedPlayerInfo();
        }
    }

    private void wins(Player winner) {
        try {
            handler.getGame().getWinAndLostState().setWinnerLabel(winner);
        } catch (IOException e) {
            e.printStackTrace();
        }
        handler.getGame().getDisplay().getFrame().setSize(300, 100);
        StateManager.setCurrentState(handler.getGame().getWinAndLostState());
    }

    public int summonAction(Card selectedCard) {
        if (isMustSelectEnemyCard() || isMustSelectFriendlyCard()) return 5; // spell or battlecry is here
        if (currentPlayer.getInfoPassive() == null) return 4;
        if (selectedCard == null) {
            // show select a card first message
            return 0; // select a cart
        } else {
            Action tempAction = null;
            if (selectedCard instanceof Spell) {
                tempAction = new SummonSpell(currentPlayer, this, selectedCard);
            } else if (selectedCard instanceof Minion) {
                if (currentPlayer.getOnFieldMinions().size() >= 7) {
                    return 3;// field is full
                } else {
                    tempAction = new SummonMinion(currentPlayer, this, selectedCard);
                }
            } else if (selectedCard instanceof Weapon){
                tempAction = new SummonWeapon(currentPlayer, this, selectedCard);
            }
            assert tempAction != null;
            tempAction.act();
            if (tempAction.isDone()) {
                actions.add(tempAction);
                checkForLost();
                try {
                    MyLogger.writeLog(getCurrentPlayer().getUsername() + "-" + getCurrentPlayer().getStringID(), tempAction.getPlayer().getUsername() + " summoned " + tempAction.getCardToPlay().getName(), Level.INFO);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if  (mode == 0)
                updateCardUseCountsMap(currentPlayer.getCurrentDeck(), tempAction);
                return 1; // done
            } else {
                return 2; // mana is not enough
            }
        }
    }

    public int setDefaults() throws IOException {
        timer = new GameTimer(handler, this);
        timer.setTimeOfPlayer(60);
        timer.start();
        actions = new ArrayList<>();
        onHandSelectedCard = null;
        onFieldSelectedCard = null;

        this.getCurrentPlayer().setMaxMana(1);
        this.getCurrentPlayer().setCurrentMana(1);
        this.getOpponentPlayer().setMaxMana(0);
        this.getOpponentPlayer().setCurrentMana(0);
        this.getPlayerOne().setInfoPassive(null);
        this.getPlayerTwo().setInfoPassive(null);
        this.getPlayerOne().setActiveWeapon(null);
        this.getPlayerTwo().setActiveWeapon(null);
        playerOne.setChangedCardFinalI(new ArrayList<>());
        playerTwo.setChangedCardFinalI(new ArrayList<>());

        // Copy current deck!
        if (mode == 0) {
            if (this.getPlayerOne().getCurrentDeck() == null) return 0;
            this.playerTwo = handler.getGame().getPlayState().getImaginaryPlayer();
            setRandomCurrentPlayer();
            getPlayerOne().setPlayingDeck(getCopyOfDeck(getPlayerOne().getCurrentDeck()));
            getPlayerTwo().setPlayingDeck(getCopyOfDeck(getPlayerTwo().getCurrentDeck()));
        }
        if (mode == 1) {
            // deck reader mode
            this.playerTwo = handler.getGame().getPlayState().getImaginaryPlayer();
            DeckReader deckReader = DeckReader.newInstance();
            deckReader.readFrom(Constants.DECK_FILE_ADDRESS, playerOne, playerTwo);
            currentPlayer = playerOne;
            opponentPlayer = playerTwo;
        }
        // Delete previous hands
        getPlayerOne().getHandCards().removeAll(this.getPlayerOne().getHandCards());
        getPlayerTwo().getHandCards().removeAll(this.getPlayerTwo().getHandCards());
        // delete all previous on field cards
        if (getPlayerOne().getOnFieldMinions() != null) {
            getPlayerOne().getOnFieldMinions().removeAll(getPlayerOne().getOnFieldMinions());
        }
        if (getPlayerTwo().getOnFieldMinions() != null) {
            getPlayerTwo().getOnFieldMinions().removeAll(getPlayerTwo().getOnFieldMinions());
        }

        if (mode == 0 || mode == 2) {
            addThreeRandomCards(currentPlayer);
            addThreeRandomCards(opponentPlayer);
        } else {
            addThreeFirstCards(currentPlayer);
            addThreeFirstCards(opponentPlayer);
        }
        return 1; // success
    }

    private void updateCardUseCountsMap(Deck deck, Action action) {
        if (deck.getUseCountsOfCards().containsKey(action.getCardToPlay().getName())) {
            deck.getUseCountsOfCards().replace(action.getCardToPlay().getName(), deck.getUseCountsOfCards().get(action.getCardToPlay().getName()) + 1);
        } else {
            deck.getUseCountsOfCards().put(action.getCardToPlay().getName(), 1);
        }
    }

    private void increaseOneMaxMana(Player player) {
        if (player.getMaxMana() < 10) {
            player.setMaxMana(player.getMaxMana() + 1);
        }
    }


    private void increaseCurrentMana(Player player) {
        player.setCurrentMana(player.getMaxMana());
    }

    public int addACardFromDeckToHand() {
        if (currentPlayer.getPlayingDeck().getCards().size() > 0) {
            if (mode == 1) {
                if (currentPlayer.getHandCards().size() >= 12) {
                    return 1; // hand is full
                } else {
                    for (Minion m :
                            currentPlayer.getOnFieldMinions()) {
                        if (m.getName().equals("Curio Collector")) {
                            if (currentPlayer.getPlayingDeck().getCards().get(0).getType().equals("Minion")){
                                ((Minion) currentPlayer.getPlayingDeck().getCards().get(0)).setHealth(
                                        ((Minion) currentPlayer.getPlayingDeck().getCards().get(0)).getHealth() + 1
                                );
                                ((Minion) currentPlayer.getPlayingDeck().getCards().get(0)).setAttack(
                                        ((Minion) currentPlayer.getPlayingDeck().getCards().get(0)).getAttack() + 1
                                );
                            }
                        }
                    }
                    currentPlayer.getHandCards().add(currentPlayer.getPlayingDeck().getCards().get(0));
                    currentPlayer.getPlayingDeck().getCards().remove(0);
                    return 2; // successful
                }
            } else {
                Random random = new Random();
                if (currentPlayer.getHandCards().size() >= 12) {
                    return 1; // hand is full
                } else {
                    int rand = random.nextInt(currentPlayer.getPlayingDeck().getCards().size());
                    currentPlayer.getHandCards().add(currentPlayer.getPlayingDeck().getCards().get(rand));
                    currentPlayer.getPlayingDeck().getCards().remove(rand);
                    return 2; // successful
                }
            }
        } else {
            currentPlayer.getPlayingDeck().getHero().setBlood(
                    currentPlayer.getPlayingDeck().getHero().getBlood() - 2
            );
            return 0; // don't have any card
        }


    }

    private void addThreeRandomCards(Player player) {
        Random random = new Random();
        for (int i = 0; i < 3; i++) {
            if (player.getPlayingDeck().getCards().size() > 0) {
                int rand = random.nextInt(player.getPlayingDeck().getCards().size());
                player.getHandCards().add(player.getPlayingDeck().getCards().get(rand));
                player.getPlayingDeck().getCards().remove(rand);
            }
        }
    }

    private void addThreeFirstCards(Player player) {
        for (int i = 0; i < 3; i++) {
            if (player.getPlayingDeck().getCards().size() > 0) {
                player.getHandCards().add(player.getPlayingDeck().getCards().get(0));
                player.getPlayingDeck().getCards().remove(0);
            }
        }
    }

    public int endTurn() {
        if (currentPlayer.getInfoPassive() == null && timer.getTimeOfPlayer() <= 0) {
            currentPlayer.setInfoPassive(InfoPassiveType.TWICE_DRAW);
        }
        if (currentPlayer.getInfoPassive() != null) {
            try {
                MyLogger.writeLog(getCurrentPlayer().getUsername() + "-" + getCurrentPlayer().getStringID(), currentPlayer.getUsername() + " pressed end turn button", Level.INFO);
            } catch (IOException e) {
                e.printStackTrace();
            }
            checkForRagnarostheFirelord();
            checkForTurnBaseSpecialPower();
            changeTurn();
            increaseOneMaxMana(currentPlayer);
            increaseCurrentMana(currentPlayer);
            increaseOnFieldTimeOfCards();
            decreaseAttackInOnce();
            checkForNightmareCard();
            checkForCorruptionCard();
            spellActionCallerID = -1;
            battlecryActionCallerID = -1;
            timer.setTimeOfPlayer(60);
            checkForLost();
            return 0; // success
        } else {
            return 1; // select an info passive
        }
    }

    private void checkForTurnBaseSpecialPower() {
        if (currentPlayer.getPlayingDeck().getHero().getName().equals("Paladin")){
            if (currentPlayer.getOnFieldMinions().size() > 0){
                Random random = new Random();
                int rand = random.nextInt(currentPlayer.getOnFieldMinions().size());
                currentPlayer.getOnFieldMinions().get(rand).setAttack(
                        currentPlayer.getOnFieldMinions().get(rand).getAttack() + 1
                );
                currentPlayer.getOnFieldMinions().get(rand).setHealth(
                        currentPlayer.getOnFieldMinions().get(rand).getHealth() + 1
                );
            }
        }
    }

    private void checkForRagnarostheFirelord() {
        for (Minion minion :
                currentPlayer.getOnFieldMinions()) {
            if (minion.getName().equals("Ragnaros the Firelord")){
                if (opponentPlayer.getOnFieldMinions().size() != 0){
                    Random random = new Random();
                    int rand = random.nextInt(opponentPlayer.getOnFieldMinions().size());
                    decreaseHealthOfMinion(opponentPlayer.getOnFieldMinions().get(rand), 8);
                }
            }
        }
        checkForDeadMinions();
    }

    private void checkForNightmareCard() {
        if (nightmareCard != null) {
            currentPlayer.getOnFieldMinions().remove(nightmareCard);
            nightmareCard = null;
        }
    }

    private void checkForCorruptionCard() {
        if (corruptionCard != null && turnForCorruption == currentPlayer) {
            for (Minion m :
                    opponentPlayer.getOnFieldMinions()) {
                if (m == corruptionCard) {
                    opponentPlayer.getOnFieldMinions().remove(corruptionCard);
                    break;
                }
            }

        }
    }

    private void increaseOnFieldTimeOfCards() {
        for (Minion minion : playerOne.getOnFieldMinions()) {
            minion.setTimeOnField(minion.getTimeOnField() + 1);
        }
        for (Minion minion : playerTwo.getOnFieldMinions()) {
            minion.setTimeOnField(minion.getTimeOnField() + 1);
        }
        if (playerOne.getActiveWeapon() != null) playerOne.getActiveWeapon().setTimeOnField(
                playerOne.getActiveWeapon().getTimeOnField() + 1
        );
        if (playerTwo.getActiveWeapon() != null) playerTwo.getActiveWeapon().setTimeOnField(
                playerTwo.getActiveWeapon().getTimeOnField() + 1
        );
    }

    private void decreaseAttackInOnce() {
        for (Minion minion : playerOne.getOnFieldMinions()) {
            minion.setAttackInOnce(0);
        }
        for (Minion minion : playerTwo.getOnFieldMinions()) {
            minion.setAttackInOnce(0);
        }
        if (playerOne.getActiveWeapon() != null) playerOne.getActiveWeapon().setAttackInOnce(0);
        if (playerTwo.getActiveWeapon() != null) playerTwo.getActiveWeapon().setAttackInOnce(0);
    }


    public Player getPlayerOne() {
        return playerOne;
    }

    public void setPlayerOne(Player playerOne) {
        this.playerOne = playerOne;
    }

    public Player getPlayerTwo() {
        return playerTwo;
    }

    public void setPlayerTwo(Player playerTwo) {
        this.playerTwo = playerTwo;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public void setCurrentPlayer(Player currentPlayer) {
        this.currentPlayer = currentPlayer;
    }

    public ArrayList<Action> getActions() {
        return actions;
    }

    public void setActions(ArrayList<Action> actions) {
        this.actions = actions;
    }

    public static int getTurn() {
        return turn;
    }

    public static void setTurn(int turn) {
        Play.turn = turn;
    }

    public static boolean isIsPlaying() {
        return isPlaying;
    }

    public static void setIsPlaying(boolean isPlaying) {
        Play.isPlaying = isPlaying;
    }

    public Card getOnHandSelectedCard() {
        return onHandSelectedCard;
    }

    public void setOnHandSelectedCard(Card onHandSelectedCard) {
        this.onHandSelectedCard = onHandSelectedCard;
    }

    public Player getOpponentPlayer() {
        return opponentPlayer;
    }

    public void setOpponentPlayer(Player opponentPlayer) {
        this.opponentPlayer = opponentPlayer;
    }

    public Card getOnFieldSelectedCard() {
        return onFieldSelectedCard;
    }

    public void setOnFieldSelectedCard(Card onFieldSelectedCard) {
        this.onFieldSelectedCard = onFieldSelectedCard;
    }

    private void checkForDeadMinions() {
        checkPlayerDeadMinions(playerOne);
        checkPlayerDeadMinions(playerTwo);
        checkForDeadWeapon(playerOne);
        checkForDeadWeapon(playerTwo);
        handler.getGame().getPlayState().updateCardViews();
        checkForLost();
    }

    private void checkForDeadWeapon(Player player) {
        if (player.getActiveWeapon() != null){
            if (player.getActiveWeapon().getDurability() <= 0) player.setActiveWeapon(null);
        }
        checkForLost();
    }

    private void checkPlayerDeadMinions(Player player) {
        for (int i = 0; i < player.getOnFieldMinions().size(); i++) {
            if (player.getOnFieldMinions().get(i).getHealth() <= 0) {
                player.getOnFieldMinions().remove(player.getOnFieldMinions().get(i));
                if (player.getInfoPassive() == InfoPassiveType.WARRIORS) {
                    player.getPlayingDeck().getHero().setBlood(player.getPlayingDeck().getHero().getBlood() + 2);
                }
            }
        }
    }

    public GameTimer getTimer() {
        return timer;
    }

    public void setTimer(GameTimer timer) {
        this.timer = timer;
    }

    public Handler getHandler() {
        return handler;
    }

    public void setHandler(Handler handler) {
        this.handler = handler;
    }

    public int getSpellActionCallerID() {
        return spellActionCallerID;
    }

    public void setSpellActionCallerID(int spellActionCallerID) {
        this.spellActionCallerID = spellActionCallerID;
    }

    public void battlecry(Card card) throws IOException {
        if (card.getName().equals("Aldor Peacekeeper")) {
            battlecryActionCallerID = 0;
            if (opponentPlayer.getOnFieldMinions().size() > 0) {
                mustSelectEnemyCard = true;
                showSelectEnemyCard();
            }
        }
        if (card.getName().equals("Madame Lazul")) {
            if (opponentPlayer.getHandCards().size() > 0) {
                battlecryActionCallerID = 1;
                battlecryWithCallerID(battlecryActionCallerID, null);
            }
        }
        if (card.getName().equals("Murloc Tidecaller")) {
            battlecryActionCallerID = 2;
            battlecryWithCallerID(battlecryActionCallerID, null);
        }
        if (card.getName().equals("Tomb Warden")) {
            battlecryActionCallerID = 3;
            battlecryWithCallerID(battlecryActionCallerID, null);
        } if (card.getName().equals("Battle Axe")){
            battlecryActionCallerID = -1;
        }
        if (card.getName().equals("Sathrovarr")){
            battlecryActionCallerID = 5;
            if (currentPlayer.getOnFieldMinions().size() > 0) {
                mustSelectFriendlyCard = true;
                showSelectFriendlyCard();
            }
        }
    }

    public void battlecryWithCallerID(int battlecryActionCallerID, Minion target) throws IOException {
        if (battlecryActionCallerID == 0) {
            // Aldor Peacekeeper
            target.setAttack(1);
        }
        if (battlecryActionCallerID == 1) {
            // Madame Lazul
            Random random = new Random();
            int i = random.nextInt(opponentPlayer.getHandCards().size());
            Card card = Card.createCardWithName(opponentPlayer.getHandCards().get(i).getName());
            if (card.getType().equals("Spell")) {
                Spell spell = Spell.createSpellFromJson(card.getName());
                currentPlayer.getHandCards().add(spell);
            } else if (card.getType().equals("Minion")) {
                Minion minion = Minion.createAMinionFromJson(card.getName());
                currentPlayer.getHandCards().add(minion);
            }
        }
        if (battlecryActionCallerID == 2) {
            // Murloc
            for (int i = 0; i < currentPlayer.getOnFieldMinions().size() - 1; i++) {
                if (currentPlayer.getOnFieldMinions().get(i).getName().equals("Murloc Tidecaller")) {
                    currentPlayer.getOnFieldMinions().get(i).setAttack(
                            currentPlayer.getOnFieldMinions().get(i).getAttack() + 1
                    );
                }
            }
        }
        if (battlecryActionCallerID == 3) {
            // Tomb Warden
            if (currentPlayer.getOnFieldMinions().size() <= 12) {
                Minion minion = Minion.createAMinionFromJson("Tomb Warden");
                currentPlayer.getOnFieldMinions().add(minion);
            }
        }
        if (battlecryActionCallerID == 4){
            //
        }
        if (battlecryActionCallerID == 5) {
            // Sathrovarr
            Minion minion = Minion.createAMinionFromJson(target.getName());
            currentPlayer.getHandCards().add(minion);
            Minion minion1 = Minion.createAMinionFromJson(target.getName());
            currentPlayer.getPlayingDeck().getCards().add(minion1);
            Minion minion2 = Minion.createAMinionFromJson(target.getName());
            currentPlayer.getOnFieldMinions().add(minion2);
        }
        checkForLost();
        handler.getGame().getPlayState().updateCardViews();
        this.battlecryActionCallerID = -1;
    }


    public void spellAction(Card onHandSelectedCard) throws IOException {
        if (onHandSelectedCard.getName().equals("Polymorph")) {
            spellActionCallerID = 0;
            if (opponentPlayer.getOnFieldMinions().size() > 0) {
                showSelectEnemyCard();
                mustSelectEnemyCard = true;
            }
        }
        if (onHandSelectedCard.getName().equals("Sprint")) {
            spellActionCallerID = 1;
            actionSpellWithID(spellActionCallerID, null);
        }
        if (onHandSelectedCard.getName().equals("Swarm of Locusts")) {
            spellActionCallerID = 2;
            actionSpellWithID(spellActionCallerID, null);
        }
        if (onHandSelectedCard.getName().equals("Pharaoh's Blessing")) {
            spellActionCallerID = 3;
            if (currentPlayer.getOnFieldMinions().size() > 0) {
                mustSelectFriendlyCard = true;
                showSelectFriendlyCard();
            }
        }
        if (onHandSelectedCard.getName().equals("Book of Specters")) {
            spellActionCallerID = 4;
            actionSpellWithID(spellActionCallerID, null);
        }
        if (onHandSelectedCard.getName().equals("Bananas")) {
            spellActionCallerID = 5;
            if (currentPlayer.getOnFieldMinions().size() > 0) {
                mustSelectFriendlyCard = true;
                showSelectFriendlyCard();
            }
        }
        if (onHandSelectedCard.getName().equals("Nightmare")) {
            spellActionCallerID = 6;
        }
        if (onHandSelectedCard.getName().equals("Ysera Awakens")) {
            spellActionCallerID = 7;
            actionSpellWithID(spellActionCallerID, null);
        }
        if (onHandSelectedCard.getName().equals("Tolin's Goblet")) {
            spellActionCallerID = 8;
            if (currentPlayer.getOnFieldMinions().size() > 0) {
                mustSelectFriendlyCard = true;
            }
        }
        if (onHandSelectedCard.getName().equals("Arcane Breath")) {
            spellActionCallerID = 9;
            if (opponentPlayer.getOnFieldMinions().size() > 0) {
                mustSelectEnemyCard = true;
                showSelectEnemyCard();
            }
        }
        if (onHandSelectedCard.getName().equals("Corruption")) {
            spellActionCallerID = 10;
            if (opponentPlayer.getOnFieldMinions().size() > 0) {
                mustSelectEnemyCard = true;
                showSelectEnemyCard();
            }
        }
        if (onHandSelectedCard.getName().equals("Friendly Smith")){
            spellActionCallerID = 11;
            actionSpellWithID(spellActionCallerID, null);
        }
        if (onHandSelectedCard.getName().equals("Zarog's Crown")){
            spellActionCallerID = 12;
            actionSpellWithID(spellActionCallerID, null);
        }
        if (onHandSelectedCard.getName().equals("Gnomish Army Knife")){
            spellActionCallerID = 13;
            if (currentPlayer.getOnFieldMinions().size() > 0) {
                mustSelectFriendlyCard = true;
                showSelectFriendlyCard();
            }
        }
    }

    private void showSelectEnemyCard() {
        handler.getGame().getPlayState().getWarningMessage().setMessage("Select enemy card!");
        handler.getGame().getPlayState().getWarningMessage().setImage(Assets.error);
        handler.getGame().getPlayState().getWarningMessage().setBackgroundColor(Constants.WARNING_COLOR);
        handler.getGame().getPlayState().getWarningMessage().setShow(true);
    }

    private void showSelectFriendlyCard() {
        handler.getGame().getPlayState().getWarningMessage().setMessage("Select friendly card!");
        handler.getGame().getPlayState().getWarningMessage().setImage(Assets.error);
        handler.getGame().getPlayState().getWarningMessage().setBackgroundColor(Constants.WARNING_COLOR);
        handler.getGame().getPlayState().getWarningMessage().setShow(true);
    }

    public void actionSpellWithID(int spellActionCallerID, Minion target) throws IOException {
        boolean isDone = false;
        if (spellActionCallerID == 0) {
            // Polymorph
            target.setAttack(1);
            target.setHealth(1);
            isDone = true;
        }
        if (spellActionCallerID == 1) {
            // Sprint
            for (int i = 0; i < 4; i++) {
                addACardFromDeckToHand();
            }
            isDone = true;
        }
        if (spellActionCallerID == 2) {
            // swarm of locusts
            for (int i = 0; i < 6; i++) {
                if (currentPlayer.getOnFieldMinions().size() < FIELD_MINIONS_SIZE) {
                    Minion oneoneMinionWithRush = Minion.createAMinionFromJson("Locust");
                    currentPlayer.getOnFieldMinions().add(oneoneMinionWithRush);
                }
            }
            isDone = true;
        }
        if (spellActionCallerID == 3) {
            // Pharaoh's Blessing
            for (Minion m :
                    currentPlayer.getOnFieldMinions()) {
                if (target == m) {
                    int plus = 4;
                    if (currentPlayer.getPlayingDeck().getHero().getName().equals("Priest")){
                        plus = 8;
                    }
                    target.setHealth(target.getHealth() + plus);
                    target.setAttack(target.getAttack() + 4);
                    target.setTaut(true);
                    target.setShield(true);
                    isDone = true;
                    break;
                }
            }
        }
        if (spellActionCallerID == 4) {
            // Book of Specters
            int o = Math.min(currentPlayer.getPlayingDeck().getCards().size(), 3);
            for (int i = 0; i < o; i++) {
                if (currentPlayer.getPlayingDeck().getCards().get(i).getType().equals("Minion")) {
                    currentPlayer.getHandCards().add(currentPlayer.getPlayingDeck().getCards().get(i));
                }
                currentPlayer.getPlayingDeck().getCards().remove(i);
            }
            isDone = true;
        }
        if (spellActionCallerID == 5) {
            // Bananas
            for (Minion m :
                    currentPlayer.getOnFieldMinions()) {
                if (target == m) {
                    int plus = 1;
                    if (currentPlayer.getPlayingDeck().getHero().getName().equals("Priest")){
                        plus = 2;
                    }
                    target.setAttack(target.getAttack() + 1);
                    target.setHealth(target.getHealth() + plus);
                    isDone = true;
                    break;
                }
            }
        }
        if (spellActionCallerID == 6) {
            // Nightmare
            for (Minion m :
                    currentPlayer.getOnFieldMinions()) {
                if (target == m) {
                    int plus = 5;
                    if (currentPlayer.getPlayingDeck().getHero().getName().equals("Priest")){
                        plus = 10;
                    }
                    nightmareCard = target;
                    target.setAttack(target.getAttack() + 5);
                    target.setHealth(target.getHealth() + plus);
                    isDone = true;
                    break;
                }
            }
        }
        if (spellActionCallerID == 7) {
            // Ysera Awakens
            for (Minion minion :
                    currentPlayer.getOnFieldMinions()) {
                if (!minion.getName().equals("Ysera")){
                    decreaseHealthOfMinion(minion, 5);
                }
            }
            for (Minion minion :
                    opponentPlayer.getOnFieldMinions()) {
                if (!minion.getName().equals("Ysera")) {
                    decreaseHealthOfMinion(minion, 5);
                }
            }
            isDone = true;
        }
        if (spellActionCallerID == 8) {
            // Tolin's Goblet
            for (Minion minion :
                    currentPlayer.getOnFieldMinions()) {
                if (minion == target) {
                    int plus = 1;
                    if (currentPlayer.getPlayingDeck().getHero().getName().equals("Priest")){
                        plus = 2;
                    }
                    target.setHealth(target.getHealth() + plus);
                    target.setAttack(target.getAttack() + 1);
                    isDone = true;
                    break;
                }
            }
        }
        if (spellActionCallerID == 9) {
            decreaseHealthOfMinion(target, 2);
        }
        if (spellActionCallerID == 10) {
            for (Minion m :
                    opponentPlayer.getOnFieldMinions()) {
                if (m == target) {
                    corruptionCard = target;
                    if (currentPlayer == playerOne) {
                        turnForCorruption = playerOne;
                    } else {
                        turnForCorruption = playerTwo;
                    }
                    break;
                }
            }
            isDone = true;
        }
        if (spellActionCallerID == 11){
            // Friendly Smith
            Random random = new Random();
            ArrayList<Weapon> weapons = new ArrayList<>();
            for (Card card:
                    Card.getAllCards()){
                if (card.getType().equals("Weapon")){
                    weapons.add((Weapon) card);
                }
            }
            int rand = random.nextInt(weapons.size());
            Weapon weapon = weapons.get(rand);
            Weapon weaponToAdd = Weapon.createWeaponWithName(weapon.getName());
            weaponToAdd.setDurability(weaponToAdd.getDurability() + 2);
            weaponToAdd.setAttack(weaponToAdd.getAttack() + 2);
            currentPlayer.getHandCards().add(weaponToAdd);
            isDone = true;
        }
        if (spellActionCallerID == 12){
            Random random = new Random();
            ArrayList<Minion> legends = new ArrayList<>();
            for (Card card:
                    Card.getAllCards()){
                if (card.getRarity().equals("Legendary") && card.getType().equals("Minion")){
                    legends.add((Minion) card);
                }
            }
            if (legends.size() > 0){
                int rand = random.nextInt(legends.size());
                Minion m = Minion.createAMinionFromJson(legends.get(rand).getName());
                int rand2 = random.nextInt(legends.size());
                Minion x = Minion.createAMinionFromJson(legends.get(rand2).getName());

                currentPlayer.getOnFieldMinions().add(m);
                currentPlayer.getOnFieldMinions().add(x);
            }
            isDone = true;
        }
        if (spellActionCallerID == 13) {
            // Gnomish Army Knife
            target.setTaut(true);
            target.setCharge(true);
            target.setShield(true);
            target.setWindfury(true);
            target.setLifesteal(true);
            target.setPoisonous(true);
            isDone = true;
        }
        if (isDone) {
            this.spellActionCallerID = -1;
        }
        handler.getGame().getPlayState().updateCardViews();
        checkForDeadMinions();
        checkForLost();
    }

    public void checkForTurnBaseActionCards() throws IOException {
        boolean dreadscale = false;
        for (Minion minion :
                currentPlayer.getOnFieldMinions()) {
            if (minion.getName().equals("Dreadscale")) {
                // dreadscale
                dreadscale = true;
                break;
            }
        }
        if (dreadscale) {
            actionDreadscale();
            checkForDeadMinions();
        }
        if (opponentPlayer.getInfoPassive() == InfoPassiveType.NURSE) {
            if (opponentPlayer.getOnFieldMinions().size() > 0) {
                ArrayList<Minion> damaged = findDamagedMinions(opponentPlayer);
                if (damaged.size() > 0){
                    Random random = new Random();
                    int rand = random.nextInt(damaged.size());
                    try {
                        Minion minion = Minion.createAMinionFromJson(damaged.get(rand).getName());
                        damaged.get(rand).setHealth(minion.getHealth());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        checkForLost();
    }

    private ArrayList<Minion> findDamagedMinions(Player player) throws IOException {
        ArrayList<Minion> minions = new ArrayList<>();
        for (Minion minion :
                player.getOnFieldMinions()) {
            Minion tempMinion = Minion.createAMinionFromJson(minion.getName());
            if (minion.getHealth() < tempMinion.getHealth()) minions.add(minion);
        }
        return minions;
    }

    private void actionDreadscale() {
        for (Minion minion :
                opponentPlayer.getOnFieldMinions()) {
            minion.setHealth(minion.getHealth() - 1);
        }
    }

    public int getMode() {
        return mode;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    public int getBattlecryActionCallerID() {
        return battlecryActionCallerID;
    }

    public void setBattlecryActionCallerID(int battlecryActionCallerID) {
        this.battlecryActionCallerID = battlecryActionCallerID;
    }


    public void attackToMinion(Card attacker, Minion target) {
        AttackToMinion attack = null;
        if (attacker instanceof Minion){
            attack = new AttackWithMinionToMinion(currentPlayer, this, attacker, target);
        }
        if (attacker instanceof Weapon){
            attack = new AttackWithWeaponToMinion(currentPlayer, this, attacker, target);
        }
        assert attack != null;
        int temp = attack.act();
        if (attack.isDone()){
            actions.add(attack);
            try {
                MyLogger.writeLog(getCurrentPlayer().getUsername() + "-" + getCurrentPlayer().getStringID(), attack.getAttacker().getName() + " attacked " + attack.getTarget().getName(), Level.INFO);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        checkForDeadMinions();
        setOnFieldSelectedCard(null);
        checkForDeadWeapon(currentPlayer);
        checkForLost();
        handler.getGame().getPlayState().setSelectedCardX(1001);
    }

    public void attackToHero(Card attacker, Hero target){
        AttackToHero attackToHero = null;
        if (attacker instanceof Minion){
            attackToHero = new AttackWithMinionToHero(currentPlayer, this, attacker, target);
        }
        if (attacker instanceof Weapon){
            attackToHero = new AttackWithWeaponToHero(currentPlayer, this, attacker, target);
        }
        int temp = attackToHero.act();
        if (attackToHero.isDone()){
            actions.add(attackToHero);
            try {
                MyLogger.writeLog(getCurrentPlayer().getUsername() + "-" + getCurrentPlayer().getStringID(), attackToHero.getCardToPlay().getName() + " attacked player hero's", Level.INFO);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        checkForDeadMinions();
        checkForDeadWeapon(currentPlayer);
        setOnFieldSelectedCard(null);
        checkForLost();
        handler.getGame().getPlayState().setSelectedCardX(1001);
    }

    public void decreaseHealthOfMinion(Minion target, int attack) {
        if (target.isShield()) target.setShield(false);
        else {
            target.setHealth(target.getHealth() - attack);
            //attacker.setHealth(attacker.getHealth() - target.getAttack());
            handleSecurityRover(target);
            //handleSecurityRover(attacker);
        }
    }

    public void decreaseHealthOfMinion(Minion target, Weapon attacker) {
        if (target.isShield()) target.setShield(false);
        else {
            target.setHealth(target.getHealth() - attacker.getAttack());
            //attacker.setHealth(attacker.getHealth() - target.getAttack());
            handleSecurityRover(target);
            //handleSecurityRover(attacker);
        }
    }

    private void handleSecurityRover(Minion m){
        if (m.getName().equals("Security Rover")){
            Player player;
            boolean opponentHas = false;
            for (Minion minion :
                    opponentPlayer.getOnFieldMinions()) {
                if (minion == m) {
                    opponentHas = true;
                    break;
                }
            }
            if (opponentHas) player = opponentPlayer;
            else player = currentPlayer;
            try {
                Minion minion = Minion.createAMinionFromJson("River Crocolisk");
                minion.setTaut(true);
                player.getOnFieldMinions().add(minion);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public boolean isMustSelectEnemyCard() {
        return mustSelectEnemyCard;
    }

    public void setMustSelectEnemyCard(boolean mustSelectEnemyCard) {
        this.mustSelectEnemyCard = mustSelectEnemyCard;
    }

    public boolean isMustSelectFriendlyCard() {
        return mustSelectFriendlyCard;
    }

    public void setMustSelectFriendlyCard(boolean mustSelectFriendlyCard) {
        this.mustSelectFriendlyCard = mustSelectFriendlyCard;
    }
}
