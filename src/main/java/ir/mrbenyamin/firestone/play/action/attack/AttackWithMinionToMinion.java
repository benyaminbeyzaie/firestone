package ir.mrbenyamin.firestone.play.action.attack;

import ir.mrbenyamin.firestone.card.Card;
import ir.mrbenyamin.firestone.card.Minion;
import ir.mrbenyamin.firestone.play.Play;
import ir.mrbenyamin.firestone.player.Player;

public class AttackWithMinionToMinion extends AttackToMinion {
    private Minion target;
    private Minion attacker;
    private Play play;
    public AttackWithMinionToMinion(Player player, Play play, Card attacker, Card target) {
        super(player, play, attacker, target);
        this.attacker = (Minion) attacker;
        this.target = (Minion) target;
        this.play = play;
    }

    @Override
    public int act() {
        if (attacker.canAttack()){
            if (play.isMustSelectEnemyCard()) return 4;
            if (play.isMustSelectFriendlyCard()) return 5;
            if (attacker.getAttackInOnce() > 0){
                if (attacker.getAttackInOnce() == 1 && attacker.isWindfury()){
                    return goToAttack();
                }
                return 1;// attacked already!
            }else return goToAttack();
        }else return 6; // can't Attack
    }

    private int goToAttack() {
        if (attacker.getTimeOnField() < 2) {
            if (attacker.isRush() || attacker.isCharge()){
                if (thereIsATaut()){
                    if (target.isTaut()){
                        successfulAct();
                        return 0;
                    }
                    else {
                        return 3;// taut
                    }
                }else {
                    successfulAct();
                    return 0;
                }
            }else{
                return 2; // can't attack this turn
            }
        }
        else if (thereIsATaut()){
            if (target.isTaut()){
                successfulAct();
                return 0; // success!
            }else {
                return 3; // taut
            }
        } else{
            successfulAct();
            return 0; // success!
        }
    }

    private void successfulAct() {
        if (attacker.isLifesteal()){
            play.getCurrentPlayer().getPlayingDeck().getHero().setBlood(
                    play.getCurrentPlayer().getPlayingDeck().getHero().getBlood() + attacker.getAttack()
            );
        }
        if (attacker.isPoisonous()){
            target.setHealth(0);
        }else {
            play.decreaseHealthOfMinion(target, attacker.getAttack() + (checkForProudDefender(play, attacker)? 2: 0));
            play.decreaseHealthOfMinion(attacker, target.getAttack() + (checkForProudDefender(play, attacker)? 2: 0));
        }
        (attacker).setAttackInOnce((attacker).getAttackInOnce() + 1);
        setDone(true);
    }
}
