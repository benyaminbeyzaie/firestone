package ir.mrbenyamin.firestone.play.action.attack;

import ir.mrbenyamin.firestone.card.Card;
import ir.mrbenyamin.firestone.card.Minion;
import ir.mrbenyamin.firestone.play.Play;
import ir.mrbenyamin.firestone.play.action.Action;
import ir.mrbenyamin.firestone.player.Player;

public abstract class AttackToMinion extends Action {
    private Card attacker;
    private Card target;
    private Play play;
    AttackToMinion(Player player, Play play, Card attacker, Card target) {
        super(player, play, attacker);
        this.attacker = attacker;
        this.target = target;
        this.play = play;
    }

    boolean thereIsATaut(){
        boolean thereIsATaut = false;
        for (Minion minion :
                play.getOpponentPlayer().getOnFieldMinions()) {
            if (minion.isTaut()) {
                thereIsATaut = true;
                break;
            }
        }
        return thereIsATaut;
    }

    public Card getAttacker() {
        return attacker;
    }

    public void setAttacker(Card attacker) {
        this.attacker = attacker;
    }

    public Card getTarget() {
        return target;
    }

    public void setTarget(Card target) {
        this.target = target;
    }
}
