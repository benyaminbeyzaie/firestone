package ir.mrbenyamin.firestone.play.action.attack;

import ir.mrbenyamin.firestone.card.Card;
import ir.mrbenyamin.firestone.card.Weapon;
import ir.mrbenyamin.firestone.heroes.Hero;
import ir.mrbenyamin.firestone.play.Play;
import ir.mrbenyamin.firestone.player.Player;

public class AttackWithWeaponToHero extends AttackToHero {
    private Weapon attacker;
    private Play play;
    public AttackWithWeaponToHero(Player player, Play play, Card attacker, Hero hero) {
        super(player, play, attacker, hero);
        this.attacker = (Weapon) attacker;
        this.play = play;
    }

    @Override
    public int act() {
        if (play.isMustSelectEnemyCard()) return 4;
        if (play.isMustSelectFriendlyCard()) return 5;
        if (attacker.getAttackInOnce() > 0){
            return 1; // already attacked!
        }
        else if (attacker.getTimeOnField() < 2){
            return 2; // can't attack
        }
        else if (!thereIsATaut()){
            hero.setBlood(hero.getBlood() - attacker.getAttack());
            attacker.setDurability(attacker.getDurability() - 1);
            attacker.setAttackInOnce(attacker.getAttackInOnce() + 1);
            setDone(true);
        }
        return 0;
    }
}
