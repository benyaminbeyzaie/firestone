package ir.mrbenyamin.firestone.play.action;

import ir.mrbenyamin.firestone.card.Card;
import ir.mrbenyamin.firestone.card.Minion;
import ir.mrbenyamin.firestone.play.Play;
import ir.mrbenyamin.firestone.player.Player;

public abstract class Action {
    Player player;
    Play play;
    Card cardToPlay;
    boolean isDone = false;

    protected Action(Player player, Play play, Card cardToPlay) {
        this.player = player;
        this.cardToPlay = cardToPlay;
        this.play = play;
    }

    protected void checkForRogueSpecialPower(Player player, Card card){
        if (player.getPlayingDeck().getHero().getName().equals("Rogue")){
            if (!(card.getHeroClass().equals("Rogue") || card.getHeroClass().equals("Any"))){
                int mana = Math.max(card.getManaCost() - 2, 0);
                card.setManaCost(mana);
            }
        }
    }

    protected boolean checkForProudDefender(Play play, Minion minion){
        if (play.getCurrentPlayer().getOnFieldMinions().size() == 1){
            return minion.getName().equals("Proud Defender");
        }
        return false;
    }
    public abstract int act();

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public Card getCardToPlay() {
        return cardToPlay;
    }

    public void setCardToPlay(Card cardToPlay) {
        this.cardToPlay = cardToPlay;
    }

    public boolean isDone() {
        return isDone;
    }

    public void setDone(boolean done) {
        isDone = done;
    }
}
