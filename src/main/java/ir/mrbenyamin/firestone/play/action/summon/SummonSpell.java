package ir.mrbenyamin.firestone.play.action.summon;

import ir.mrbenyamin.firestone.card.Card;
import ir.mrbenyamin.firestone.card.Spell;
import ir.mrbenyamin.firestone.play.Play;
import ir.mrbenyamin.firestone.play.action.Action;
import ir.mrbenyamin.firestone.player.Player;

import java.util.ArrayList;

public class SummonSpell extends Action {

    private Play play;
    private Player player;
    private Card cardToPlay;


    public SummonSpell(Player player, Play play, Card cardToPlay) {
        super(player, play, cardToPlay);
        this.play = play;
        this.player = player;
        this.cardToPlay = cardToPlay;
    }

    @Override
    public int act() {
        if (player.getInfoPassive() != null){
            // Special Powers check
            checkForMageSpecialPower(player, cardToPlay);
            checkForRogueSpecialPower(player, cardToPlay);

            if (player.getCurrentMana() >= cardToPlay.getManaCost()){
                if (player.getHandCards().contains(cardToPlay)) {
                    if (player.getPlayedSpells() == null){
                        player.setPlayedSpells(new ArrayList<>());
                    }
                    player.getPlayedSpells().add((Spell) cardToPlay);
                    player.getHandCards().removeIf(card -> card == cardToPlay);
                    player.setCurrentMana(player.getCurrentMana() - cardToPlay.getManaCost());
                    setDone(true);
                }
            }
            return 0; // success
        }
        return 1; // select info passive
    }

    private void checkForMageSpecialPower(Player player, Card cardToPlay) {
        if (player.getPlayingDeck().getHero().getName().equals("Mage")){
            int mana = Math.max(cardToPlay.getManaCost() - 2, 0);
            cardToPlay.setManaCost(mana);
        }
    }
}
