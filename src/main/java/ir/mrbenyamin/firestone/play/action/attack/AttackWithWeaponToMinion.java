package ir.mrbenyamin.firestone.play.action.attack;

import ir.mrbenyamin.firestone.card.Card;
import ir.mrbenyamin.firestone.card.Minion;
import ir.mrbenyamin.firestone.card.Weapon;
import ir.mrbenyamin.firestone.play.Play;
import ir.mrbenyamin.firestone.player.Player;

public class AttackWithWeaponToMinion extends AttackToMinion {
    private Weapon attacker;
    private Minion target;
    private Player player;
    private Play play;
    public AttackWithWeaponToMinion(Player player, Play play, Card attacker, Card target) {
        super(player, play, attacker, target);
        this.attacker = (Weapon) attacker;
        this.target = (Minion) target;
        this.player = player;
        this.play = play;
    }

    @Override
    public int act() {
        if (play.isMustSelectEnemyCard()) return 4;
        if (play.isMustSelectFriendlyCard()) return 5;
        if (attacker.getAttackInOnce() > 0){
            return 1; // already attacked!
        }
        else if (attacker.getTimeOnField() < 2){
            return 2; // can't attack
        }
        else {
            if (thereIsATaut()){
                if (target.isTaut()){
                    successfulAct();
                    return 0;
                }else {
                    return 3; // taut
                }
            }else {
                successfulAct();
                return 0;
            }
        }
    }

    private void successfulAct() {
        play.decreaseHealthOfMinion(target, attacker);
        attacker.setDurability(attacker.getDurability() - 1);
        attacker.setAttackInOnce(attacker.getAttackInOnce() + 1);
        player.getPlayingDeck().getHero().setBlood(
                player.getPlayingDeck().getHero().getBlood() - target.getAttack()
        );
        setDone(true);
    }
}
