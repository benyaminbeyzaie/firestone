package ir.mrbenyamin.firestone.play.action.attack;

import ir.mrbenyamin.firestone.card.Card;
import ir.mrbenyamin.firestone.card.Minion;
import ir.mrbenyamin.firestone.heroes.Hero;
import ir.mrbenyamin.firestone.play.Play;
import ir.mrbenyamin.firestone.play.action.Action;
import ir.mrbenyamin.firestone.player.Player;

public abstract class AttackToHero extends Action {
    Hero hero;
    private Play play;
    AttackToHero(Player player, Play play, Card cardToPlay, Hero hero) {
        super(player, play, cardToPlay);
        this.hero = hero;
        this.play = play;
    }

    boolean thereIsATaut(){
        boolean thereIsATaut = false;
        for (Minion minion :
                play.getOpponentPlayer().getOnFieldMinions()) {
            if (minion.isTaut()) {
                thereIsATaut = true;
                break;
            }
        }
        return thereIsATaut;
    }
}
