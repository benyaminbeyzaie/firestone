package ir.mrbenyamin.firestone.play.action.attack;

import ir.mrbenyamin.firestone.card.Card;
import ir.mrbenyamin.firestone.card.Minion;
import ir.mrbenyamin.firestone.heroes.Hero;
import ir.mrbenyamin.firestone.play.Play;
import ir.mrbenyamin.firestone.player.Player;

public class AttackWithMinionToHero extends AttackToHero {
    private Minion attacker;
    private Play play;
    public AttackWithMinionToHero(Player player, Play play, Card attacker, Hero hero) {
        super(player, play, attacker, hero);
        this.attacker = (Minion) attacker;
        this.play = play;
    }

    @Override
    public int act() {
        if (attacker.canAttack()){
            if (play.isMustSelectEnemyCard()) return 4;
            if (play.isMustSelectFriendlyCard()) return 5;
            if (attacker.getAttackInOnce() > 0){
                if (attacker.getAttackInOnce() == 1 && attacker.isWindfury()){
                    if (attacker.getTimeOnField() < 2) {
                        if (attacker.isRush() && !thereIsATaut()){
                            return successfulAct();
                        }else {
                            return 2; // can't attack this turn
                        }
                    }
                    else if (!thereIsATaut()){
                        return successfulAct();
                    }else {
                        return 3; // taut
                    }
                }
                return 1;// attacked already!
            }else if (attacker.getTimeOnField() < 2) {
                if (attacker.isRush() && !thereIsATaut()){
                    return successfulAct();
                }else {
                    return 2; // can't attack this turn
                }
            }
            else if (!thereIsATaut()){
                return successfulAct();
            }else {
                return 3; // taut
            }
        }else {
            return 6;// can't Attack
        }
    }

    private int successfulAct() {
        if (attacker.isLifesteal()){
            play.getCurrentPlayer().getPlayingDeck().getHero().setBlood(
                    play.getCurrentPlayer().getPlayingDeck().getHero().getBlood() + attacker.getAttack()
            );
        }
        hero.setBlood(hero.getBlood() - attacker.getAttack() - (checkForProudDefender(play, attacker)? 2 : 0));
        attacker.setAttackInOnce(attacker.getAttackInOnce() + 1);
        setDone(true);
        return 0;
    }
}
