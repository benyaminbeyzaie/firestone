package ir.mrbenyamin.firestone.play.action.summon;

import ir.mrbenyamin.firestone.card.Card;
import ir.mrbenyamin.firestone.card.Minion;
import ir.mrbenyamin.firestone.play.Play;
import ir.mrbenyamin.firestone.play.action.Action;
import ir.mrbenyamin.firestone.player.Player;

import java.util.ArrayList;

public class SummonMinion extends Action {
    private Player player;
    private Card cardToPlay;
    public SummonMinion(Player player, Play play, Card cardToPlay) {
        super(player, play, cardToPlay);
        this.player = player;
        this.cardToPlay = cardToPlay;
    }

    @Override
    public int act() {
        checkForRogueSpecialPower(player, cardToPlay);

        if (player.getInfoPassive() != null){
            if (player.getCurrentMana() >= cardToPlay.getManaCost()){
                if (player.getHandCards().contains(cardToPlay)){
                    if (player.getOnFieldMinions() == null){
                        player.setOnFieldMinions(new ArrayList<>());
                    }
                    player.getOnFieldMinions().add((Minion) cardToPlay);
                    for (int t = 0; t < player.getHandCards().size(); t++) {
                        if (player.getHandCards().get(t) == cardToPlay){
                            player.getHandCards().remove(t);
                            break;
                        }
                    }
                    player.setCurrentMana(player.getCurrentMana() - cardToPlay.getManaCost());
                    setDone(true);
                }
            }
            return 0;
        }
        return 1;
    }
}
