package ir.mrbenyamin.firestone.play;

public enum InfoPassiveType {
    TWICE_DRAW(0){

    }, OFF_CARDS(1){

    }, MANA_JUMP(2){

    }, WARRIORS(3){

    }, NURSE(4){

    };

    private int infoIndex;

    public static InfoPassiveType getInfoPassive(int infoIndex) {
        for (InfoPassiveType l : InfoPassiveType.values()) {
            if (l.infoIndex == infoIndex) return l;
        }
        throw new IllegalArgumentException("Leg not found. Amputated?");
    }

    InfoPassiveType(int i) {
        this.infoIndex = i;
    }
}
