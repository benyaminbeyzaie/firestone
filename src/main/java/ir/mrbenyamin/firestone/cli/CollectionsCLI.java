package ir.mrbenyamin.firestone.cli;

import ir.mrbenyamin.firestone.log.MyLogger;
import ir.mrbenyamin.firestone.player.Player;

import java.io.IOException;
import java.util.Scanner;
import java.util.logging.Level;

public class CollectionsCLI extends CLI {

    //Singleton design pattern
    private static CollectionsCLI collectionsCLI = null;
    private CollectionsCLI(){}
    public static CollectionsCLI getInstance(){
        if (collectionsCLI == null){
            collectionsCLI = new CollectionsCLI();
        }
        return collectionsCLI;
    }

    void collectionManager(Player signedPlayer) throws IOException {
        System.out.println("\u001B[32mCollections");
        Scanner scanner = new Scanner(System.in);
        //boolean running = true;
        while (true){
            String nextLine = scanner.nextLine();
            if (nextLine.length() > 8 && nextLine.substring(0,6).equals("select")){
                //signedPlayer.selectHero(nextLine.substring(7));
                signedPlayer.saveSignedPlayerInfo();
            }else if(nextLine.length() > 4 && nextLine.substring(0,3).equals("add")){
                //signedPlayer.addCardToBattleDeck(nextLine.substring(4));
                signedPlayer.saveSignedPlayerInfo();
            }else if (nextLine.length() > 8 && nextLine.substring(0,6).equals("remove")){
                //signedPlayer.removeCardFromDeck(nextLine.substring(7));
                signedPlayer.saveSignedPlayerInfo();
            } else {
                switch (nextLine){
                    case "--help":
                        showHelp();
                        break;
                    case "ls -a -heroes":
                        MyLogger.writeLog(signedPlayer.getUsername() + "-" + signedPlayer.getStringID(),  "list all heroes", Level.INFO);
                        //signedPlayer.showAllAvailableHeroes();
                        break;
                    case "ls -m -heroes":
                        MyLogger.writeLog(signedPlayer.getUsername() + "-" + signedPlayer.getStringID(),  "list current heroes", Level.INFO);
                        //signedPlayer.showCurrentHero();
                        break;
                    case "ls -a -cards":
                        MyLogger.writeLog(signedPlayer.getUsername() + "-" + signedPlayer.getStringID(),  "show all available cards", Level.INFO);
                        //signedPlayer.showAllAvailableCards();
                        break;
                    case "ls -m -cards":
                        MyLogger.writeLog(signedPlayer.getUsername() + "-" + signedPlayer.getStringID(),  "show battle deck", Level.INFO);
                        //signedPlayer.showBattleDeckCards();
                        break;
                    case "ls -n -cards":
                        MyLogger.writeLog(signedPlayer.getUsername() + "-" + signedPlayer.getStringID(),  "show all cards that player can add to deck", Level.INFO);
                        //signedPlayer.showAllCardsThatPlayerCanAddToDeck();
                        break;
                    case "main":
                        MyLogger.writeLog(signedPlayer.getUsername() + "-" + signedPlayer.getStringID(),  "back to main", Level.INFO);
                        PlayerCLI playerCLI = PlayerCLI.getInstance();
                        playerCLI.signedPlayerCommands(signedPlayer);
                        break;
                    default:
                        System.out.println("Sorry that command is not defined! try --help to see all defined commands");
                }
            }
        }
    }


}

