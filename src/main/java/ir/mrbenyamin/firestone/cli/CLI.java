package ir.mrbenyamin.firestone.cli;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import ir.mrbenyamin.firestone.cli.help.Help;
import ir.mrbenyamin.firestone.player.sign.SignIn;
import ir.mrbenyamin.firestone.player.sign.SignUp;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class CLI {
    private static final String JSON_HELP_FILE_PATH = "src\\main\\java\\ir\\mrbenyamin\\firestone\\cli\\help\\helpjsonfiles\\";
    //Singleton design pattern!
    private static CLI cli = null;
    // CLI Constructor is not private because it is a super class
    public static CLI getInstance(){
        if (cli == null){
            cli = new CLI();
        }
        return cli;
    }

    public void run() throws IOException {
        PlayerCLI playerCLI = PlayerCLI.getInstance();
        Scanner command = new Scanner(System.in);
        //boolean running = true;
        System.out.println("Already have an account? (Y/N) ");
        System.out.println("use --help command in order to get help");
        while (true) {
            switch (command.nextLine()) {
                case "Y":
                    //SignIn.getInstance().signIn();
                    break;
                case "N":
                    //SignUp.getInstance().signUp();
                    break;
                case "--help":
                    showHelp();
                    break;
                case "exit":
                    System.exit(0);
                    break;
                default:
                    System.out.println("Sorry that command is not defined! try --help to see all defined commands");
            }
        }
    }
    public void showHelp() throws IOException {
        File helpFile = new File(JSON_HELP_FILE_PATH + this.getClass().getSimpleName() + ".json");
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        Help help = objectMapper.readValue(helpFile, Help.class);
        for (String st :
                help.getHelpText()) {
            System.out.println(st);
        }
    }
}

