package ir.mrbenyamin.firestone.cli;


import ir.mrbenyamin.firestone.log.MyLogger;
import ir.mrbenyamin.firestone.player.Player;
import ir.mrbenyamin.firestone.shop.Shop;
import ir.mrbenyamin.firestone.util.HibernateUtil;
import org.hibernate.Session;

import java.io.IOException;
import java.util.Scanner;
import java.util.logging.Level;

public class ShopCLI extends CLI{

    //Singleton design pattern
    private static ShopCLI shopCLI = null;
    private ShopCLI(){}
    public static ShopCLI getInstance(){
        if (shopCLI == null){
            shopCLI = new ShopCLI();
        }
        return shopCLI;
    }

    public void shopManager(Player signedPlayer) throws IOException {
        System.out.println("\u001B[32mShop");
        Scanner scanner = new Scanner(System.in);
        Shop shop = new Shop();
        while (true){
            String nextLine = scanner.nextLine();
            if (nextLine.length() > 4 && nextLine.substring(0,3).equals("buy")){
                //buy
                //System.out.println("Buying...");
                shop.buy(signedPlayer, nextLine.substring(4));
                Session session = HibernateUtil.getSessionFactory().openSession();
                session.update(signedPlayer);
                session.close();
                HibernateUtil.getSessionFactory().close();
            }else if(nextLine.length() > 5 && nextLine.substring(0,4).equals("sell")){
                //sell
                //System.out.println("Selling...");
                shop.sell(signedPlayer, nextLine.substring(5));
                Session session = HibernateUtil.getSessionFactory().openSession();
                session.update(signedPlayer);
                session.close();
                HibernateUtil.getSessionFactory().close();
            }else {
                switch (nextLine){
                    case "--help":
                        showHelp();
                        break;
                    case "wallet":
                        MyLogger.writeLog(signedPlayer.getUsername() + "-" + signedPlayer.getStringID() , "wallet", Level.INFO);
                        System.out.println("You have " + signedPlayer.getGems() + " gems!");
                        break;
                    case "ls -s":
                        MyLogger.writeLog(signedPlayer.getUsername() + "-" + signedPlayer.getStringID() , "show all salable cards", Level.INFO);
                        //shop.showAllSalableCards(signedPlayer);
                        break;
                    case "ls -b":
                        MyLogger.writeLog(signedPlayer.getUsername() + "-" + signedPlayer.getStringID() , "show all buyable :) cards", Level.INFO);
                        //shop.showAllowableCardsToBuy(signedPlayer);
                        break;
                    case "main":
                        MyLogger.writeLog(signedPlayer.getUsername() + "-" + signedPlayer.getStringID(),  "back to main", Level.INFO);
                        PlayerCLI playerCLI = PlayerCLI.getInstance();
                        playerCLI.signedPlayerCommands(signedPlayer);
                        break;
                    default:
                        System.out.println("Sorry that command is not defined! try --help to see all defined commands");
                }
            }
        }
    }
}

