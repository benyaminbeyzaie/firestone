package ir.mrbenyamin.firestone.cli;

import ir.mrbenyamin.firestone.log.MyLogger;
import ir.mrbenyamin.firestone.player.Player;

import java.io.IOException;
import java.util.Scanner;
import java.util.logging.Level;

public class PlayerCLI extends CLI{

    //Singleton design pattern
    private static PlayerCLI playerCLI = null;
    public PlayerCLI(){

    }
    public static PlayerCLI getInstance(){
        if (playerCLI == null){
            playerCLI = new PlayerCLI();
        }
        return playerCLI;
    }



    public void signedPlayerCommands(Player signedPlayer) throws IOException {
        System.out.println("\u001B[32mMain menu");
        Scanner scanner = new Scanner(System.in);
        //boolean running = true;
        while (true) {
            switch (scanner.nextLine()) {
                case "--help":
                    showHelp();
                    break;
                case "collections":
                    MyLogger.writeLog(signedPlayer.getUsername() + "-" + signedPlayer.getStringID(),  "move to collection", Level.INFO);
                    CollectionsCLI collectionsCLI = CollectionsCLI.getInstance();
                    collectionsCLI.collectionManager(signedPlayer);
                    break;
                case "shop":
                    MyLogger.writeLog(signedPlayer.getUsername() + "-" + signedPlayer.getStringID(),  "move to shop", Level.INFO);
                    ShopCLI shopCLI = ShopCLI.getInstance();
                    shopCLI.shopManager(signedPlayer);
                    break;
                case "logout":
                    MyLogger.writeLog(signedPlayer.getUsername() + "-" + signedPlayer.getStringID(),  "log out!", Level.INFO);
                    CLI.getInstance().run();
                    break;
                case "delete":
                    //signedPlayer.deletePlayer();
                    break;
                case "exit" :
                    System.exit(0);
                default:
                    System.out.println("Sorry that command is not defined! try --help to see all defined commands");
            }
        }
    }


}

