package ir.mrbenyamin.firestone.heroes;


import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import ir.mrbenyamin.firestone.card.Card;
import ir.mrbenyamin.firestone.card.Minion;
import ir.mrbenyamin.firestone.card.Spell;

import javax.persistence.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Hero {
    @Id
    private String name;
    @Column
    private int blood;
    @Column
    private String CARDS_PATH;
    @ManyToMany
    @JoinTable
    private List<Card> classCards = new ArrayList<>();
    transient public static final String HERO_FILE_PATH = "src\\main\\java\\ir\\mrbenyamin\\firestone\\heroes\\heroesjsonfiles\\";

    public static Hero createHero(String name) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        File file = new File( HERO_FILE_PATH + name + ".json");
        Hero hero = objectMapper.readValue(file, Hero.class);
        return hero;
    }

    public String getCARDS_PATH() {
        return CARDS_PATH;
    }

    public void setCARDS_PATH(String CARDS_PATH) {
        this.CARDS_PATH = CARDS_PATH;
    }

    public int getBlood() {
        return blood;
    }

    public void setBlood(int blood) {
        this.blood = blood;
    }

    public List<Card> getClassCards() {
        return classCards;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}

