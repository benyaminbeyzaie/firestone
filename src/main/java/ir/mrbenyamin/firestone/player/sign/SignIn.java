package ir.mrbenyamin.firestone.player.sign;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import ir.mrbenyamin.firestone.card.Card;
import ir.mrbenyamin.firestone.log.MyLogger;
import ir.mrbenyamin.firestone.player.Player;
import ir.mrbenyamin.firestone.util.HibernateUtil;
import org.hibernate.Session;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;

public class SignIn {
    //Singleton design pattern
    private static SignIn signIn = null;
    private Player signedPlayer;
    private SignIn(){

    }
    public static SignIn getInstance(){
        if (signIn == null){
            signIn = new SignIn();
        }
        return signIn;
    }

    public static String PLAYER_FILE_PATH = "src\\main\\java\\ir\\mrbenyamin\\firestone\\player\\playersjsonfiles\\";


    //overload sign in method due to use in sign up method!
    public int signIn(String userIn, String passIn) throws IOException {
        if (userIn.equals("")) return 0;
        else if (passIn.equals("")) return 1;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Player check = session.get(Player.class, userIn);
        if (check != null) {
//            ObjectMapper objectMapper = new ObjectMapper();
//            SimpleModule simpleModule = new SimpleModule();
//            simpleModule.addKeyDeserializer(Card.class, new De());
//            objectMapper.registerModule(simpleModule);
//            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
//            Player player = objectMapper.readValue(file, Player.class);
            if (check.checkPassword(passIn)) {
                signedPlayer = check;
                MyLogger.writeLog(signedPlayer.getUsername() + "-" + signedPlayer.getStringID(), signedPlayer.getUsername() + " sign in successfully!", Level.INFO);
                session.close();
                return 4;
            } else {
                session.close();
                return 3;
            }
        } else {
            session.close();
            return 2;
        }
    }

    public Player getSignedPlayer() {
        return signedPlayer;
    }

    public void setSignedPlayer(Player signedPlayer) {
        this.signedPlayer = signedPlayer;
    }
}

