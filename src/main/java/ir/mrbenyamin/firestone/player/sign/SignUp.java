package ir.mrbenyamin.firestone.player.sign;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import ir.mrbenyamin.firestone.log.MyLogger;
import ir.mrbenyamin.firestone.play.Play;
import ir.mrbenyamin.firestone.player.Player;
import ir.mrbenyamin.firestone.security.Security;
import ir.mrbenyamin.firestone.util.HibernateUtil;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.LineIterator;
import org.hibernate.Session;

public class SignUp {
    //Singleton Design pattern
    private static SignUp signUp = null;
    private Player player;
    private String tmpPass;
    private SignUp(){

    }
    public static SignUp getInstance(){
        if (signUp == null){
            signUp = new SignUp();
        }
        return signUp;
    }

    public int signUp(String user, String pass) throws IOException {
        if (user == null || user.equals("")) return 0; // user is empty;
        if (pass == null || pass.equals("")) return 1; // pass is empty;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Player check = session.get(Player.class, user);
        if (check == null){
            tmpPass = pass;
            // Hashing the password!
            pass = Security.hashPassword(pass);
            player = new Player(user, pass);

            final DateFormat dff = new SimpleDateFormat("yyMMddhhmmss");
            player.setStringID(dff.format(new Date()));
            player.setDefaults(session);
            session.beginTransaction();
            session.save(player);
            session.getTransaction().commit();
            session.close();
            createLogFile();
            MyLogger.writeLog(player.getUsername() + "-" + player.getStringID(), player.getUsername() + " signed up successfully!" , Level.INFO);
            return 3; // User created successfully
        }else {
            return 2; // User exist
        }
    }

    private void createLogFile() throws IOException {

        DateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
        File logFile = new File("src\\main\\java\\ir\\mrbenyamin\\firestone\\log\\logfiles\\" + player.getUsername() + "-" +player.getStringID() + ".log");
        // Not sure if it helps or not but too scared to delete!
        logFile.createNewFile();
        LineIterator li = FileUtils.lineIterator(logFile);
        BufferedWriter w = new BufferedWriter(new FileWriter(logFile));
        try {
            w.write("//this user crated at: " + df.format(new Date()) + "\n//PASSWORD: " + tmpPass + "\n");
        } finally {
            // never ends in this block!
            IOUtils.closeQuietly(w);
            LineIterator.closeQuietly(li);
        }
    }
}

