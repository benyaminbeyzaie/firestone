package ir.mrbenyamin.firestone.player.sign;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.KeyDeserializer;
import ir.mrbenyamin.firestone.card.Card;

import java.io.IOException;

public class De extends KeyDeserializer
{
    @Override
    public Object deserializeKey(final String key, final DeserializationContext ctxt ) throws IOException, JsonProcessingException
    {

        return Card.getCardOfName(key);
    }
}
