package ir.mrbenyamin.firestone.player;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import ir.mrbenyamin.firestone.card.*;
import ir.mrbenyamin.firestone.heroes.Hero;
import ir.mrbenyamin.firestone.log.MyLogger;
import ir.mrbenyamin.firestone.play.InfoPassiveType;
import ir.mrbenyamin.firestone.security.Security;

import ir.mrbenyamin.firestone.util.HibernateUtil;
import ir.mrbenyamin.firestone.view.Constants;
import ir.mrbenyamin.firestone.view.state.states.MainState;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.LineIterator;
import org.hibernate.Session;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

@Entity
@Table(name = "player")
public class Player {
    @Id
    private String username;
    @Column
    private String password;
    @Column
    private String stringID;
    @Column
    private int gems;

    @ManyToOne
    @Cascade(value = org.hibernate.annotations.CascadeType.ALL)
    private Deck currentDeck;
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable
    private List<Card> allAvailableCards = new ArrayList<>();
    @ManyToMany
    @JoinTable
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Hero> availableHeroes = new ArrayList<>();
    @OneToMany
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinTable
    @Cascade(value = org.hibernate.annotations.CascadeType.ALL)
    private List<Deck> decks = new ArrayList<>();
    private final static int MIN_CARD_SIZE = 2;

    // Play State things
    private final static int MAX_HAND_CARDS_SIZE = 12;
    private final static int MAX_ON_FIELD_CARDS_SIZE = 7;
    transient private Deck playingDeck;
    transient private ArrayList<Integer> changedCardFinalI = new ArrayList<>();

    transient private ArrayList<Card> handCards = new ArrayList<>();
    transient private ArrayList<Minion> onFieldMinions;
    transient private ArrayList<Spell> playedSpells;
    private String cardBackImageName;
    private int currentMana;
    private int maxMana;
    private InfoPassiveType infoPassive;
    transient private Weapon activeWeapon;

    // Sound
    private boolean soundPlay;
    private String backMusicName;
    private String gameMusicName;
    private String gameBackImageName;
    //private int volume;

    public Player() {
        onFieldMinions = new ArrayList<>();
        playedSpells = new ArrayList<>();
    }


    public Player(String username, String password) {
        this.username = username;
        this.password = password;
        onFieldMinions = new ArrayList<>();
        playedSpells = new ArrayList<>();
    }

    public void setDefaults(Session session) throws IOException {
        Hero mage = session.get(Hero.class, "Mage");
        Hero priest = session.get(Hero.class, "Priest");
        availableHeroes.add(mage);
        availableHeroes.add(priest);
        setDefaultAllAvailableCards(session);
        soundPlay = true;
        backMusicName = "one";
        gameMusicName = "one";
        gameBackImageName = "one";
        cardBackImageName = "one";
    }

    public synchronized void saveSignedPlayerInfo() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.update(this);
        session.getTransaction().commit();
        session.close();
        //HibernateUtil.getSessionFactory().close();
    }

    private void setDefaultAllAvailableCards(Session session) throws IOException {
        setGems(50);


        String CARD_PATH = Constants.CARD_PATH;
//        deckFiles.add(new File(CARD_PATH + "Bazaar Burglary.json"));
//        deckFiles.add(new File(CARD_PATH + "Friendly Smith.json"));
//        deckFiles.add(new File(CARD_PATH + "Corruption.json"));
//        deckFiles.add(new File(CARD_PATH + "Dreadscale.json"));
//        deckFiles.add(new File(CARD_PATH + "Polymorph.json"));
//        deckFiles.add(new File(CARD_PATH + "Arcane Breath.json"));
//        deckFiles.add(new File(CARD_PATH + "Goldshire Footman.json"));
//        deckFiles.add(new File(CARD_PATH + "Knife Juggler.json"));
//        deckFiles.add(new File(CARD_PATH + "River Crocolisk.json"));


        Minion minion = session.get(Minion.class, "Woodchip");
        allAvailableCards.add(minion);

        ArrayList<Card> defaultDeckCardList = new ArrayList<>();
        defaultDeckCardList.add(session.get(Minion.class, "Woodchip"));

        Hero hero = session.get(Hero.class, "Mage");

        Deck defaultDeck = new Deck("Default1", hero, defaultDeckCardList);
        decks.add(defaultDeck);


    }

    public int addCardToDeck(Card card, Deck deck){
        if (deck.getCards().size() + 1 <= Constants.MAX_DECK_CARD_SIZE){
            if (card.getHeroClass().equals(deck.getHero().getName()) || card.getHeroClass().equals("Any")){
                if (this.getAllAvailableCards().contains(card)){
                    int temp = 0;
                    for (Card ca :
                            deck.getCards()) {
                        if (ca.equals(card)) temp++;
                    }
                    if (temp >= 2){
                        return 4; // already have 2
                    }else{
                        deck.getCards().add(card);
                        if (deck == this.getCurrentDeck())
                        {
                            this.setCurrentDeck(null);
                        }
                        try {
                            MyLogger.writeLog(MainState.LOG_NAME,  "Player add " + card.getName() + " to " + deck.getName(), Level.INFO);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        return 2; // Success
                    }
                }else {
                    return 3; // buy card first!
                }
            }else{
                return 1; // card is not in hero class
            }
        }else {
            return 0; // deck is full
        }
    }

    public List<Hero> getAvailableHeroes() {
        return availableHeroes;
    }

    public void setAvailableHeroes(ArrayList<Hero> availableHeroes) {
        this.availableHeroes = availableHeroes;
    }

    public String getStringID() {
        return stringID;
    }

    public void setStringID(String ID) {
        this.stringID = ID;
    }

    public int getGems() {
        return gems;
    }

    public void setGems(int gems) {
        this.gems = gems;
    }

    public List<Card> getAllAvailableCards() {
        return allAvailableCards;
    }

    public void setAllAvailableCards(ArrayList<Card> allAvailableCards) {
        this.allAvailableCards = allAvailableCards;
    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean checkPassword(String pass) {
        return Security.hashPassword(pass).equals(this.getPassword());
    }

//    public Hero getCurrentHero() {
//        return currentHero;
//    }
//
//    public void setCurrentHero(Hero currentHero) {
//        this.currentHero = currentHero;
//    }

    public static int getMinCardSize() {
        return MIN_CARD_SIZE;
    }


    public int deletePlayer(String pass) throws IOException {
        if (Security.hashPassword(pass).equals(this.getPassword())){
            File file = new File("src\\main\\java\\ir\\mrbenyamin\\firestone\\player\\playersjsonfiles\\" + this.getUsername() + ".json");
            if (file.delete()){
                DateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
                File logFile = new File("src\\main\\java\\ir\\mrbenyamin\\firestone\\log\\logfiles\\" + this.getUsername() + "-" + this.getStringID() + ".log");
                LineIterator li = FileUtils.lineIterator(logFile);
                File tempFile = File.createTempFile("src\\main\\java\\ir\\mrbenyamin\\firestone\\log\\logfiles\\prependPrefix", ".tmp");
                BufferedWriter w = new BufferedWriter(new FileWriter(tempFile));
                try {
                    w.write("//this user deleted at: " + df.format(new Date()) + "\n");
                    while (li.hasNext()) {
                        w.write(li.next());
                        w.write("\n");
                    }
                } finally {
                    IOUtils.closeQuietly(w);
                    LineIterator.closeQuietly(li);
                }
                FileUtils.deleteQuietly(logFile);
                FileUtils.moveFile(tempFile, logFile);

                MyLogger.writeLog(this.getUsername() + "-" + this.getStringID(), this.getUsername() + " Deleted!", Level.INFO);
                return 0; // success
            }else{
                return 1; // problem
            }
        }else {
           return 2;// pass is wrong!
        }
    }


    @Override
    public String toString() {
        return "Player{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", ID='" + stringID + '\'' +
                '}';
    }

    public List<Deck> getDecks() {
        return decks;
    }

    public void setDecks(ArrayList<Deck> decks) {
        this.decks = decks;
    }

    public static int getMaxHandCardsSize() {
        return MAX_HAND_CARDS_SIZE;
    }

    public static int getMaxOnFieldCardsSize() {
        return MAX_ON_FIELD_CARDS_SIZE;
    }

    public ArrayList<Card> getHandCards() {
        return handCards;
    }

    public void setHandCards(ArrayList<Card> handCards) {
        this.handCards = handCards;
    }

    public Deck getCurrentDeck() {
        return currentDeck;
    }

    public void setCurrentDeck(Deck currentDeck) {
        this.currentDeck = currentDeck;
    }

    public ArrayList<Minion> getOnFieldMinions() {
        return onFieldMinions;
    }

    public void setOnFieldMinions(ArrayList<Minion> onFieldMinions) {
        this.onFieldMinions = onFieldMinions;
    }

    public ArrayList<Spell> getPlayedSpells() {
        return playedSpells;
    }

    public void setPlayedSpells(ArrayList<Spell> playedSpells) {
        this.playedSpells = playedSpells;
    }

    public String getCardBackImageName() {
        return cardBackImageName;
    }

    public void setCardBackImageName(String cardBackImageName) {
        this.cardBackImageName = cardBackImageName;
        try {
            MyLogger.writeLog(this.getUsername() + "-" + this.getStringID(),  this.getUsername() + " select " + cardBackImageName + " for card backs.", Level.INFO);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public int getCurrentMana() {
        return currentMana;
    }

    public void setCurrentMana(int currentMana) {
        this.currentMana = currentMana;
    }

    public int getMaxMana() {
        return maxMana;
    }

    public void setMaxMana(int maxMana) {
        this.maxMana = maxMana;
    }

    public Deck getPlayingDeck() {
        return playingDeck;
    }

    public void setPlayingDeck(Deck playingDeck) {
        this.playingDeck = playingDeck;
    }

    public boolean isSoundPlay() {
        return soundPlay;
    }

    public void setSoundPlay(boolean soundPlay) {
        this.soundPlay = soundPlay;
        try {
            MyLogger.writeLog(this.getUsername() + "-" + this.getStringID(),  this.getUsername() + " set sound to: " + soundPlay, Level.INFO);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getBackMusicName() {
        return backMusicName;
    }

    public void setBackMusicName(String backMusicName) {
        this.backMusicName = backMusicName;
        try {
            MyLogger.writeLog(this.getUsername() + "-" + this.getStringID(),  this.getUsername() + " select " + backMusicName + " for background music.", Level.INFO);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getGameMusicName() {
        return gameMusicName;
    }

    public void setGameMusicName(String gameMusicName) {
        this.gameMusicName = gameMusicName;
        try {
            MyLogger.writeLog(this.getUsername() + "-" + this.getStringID(),  this.getUsername() + " select " + gameBackImageName + " for game background music.", Level.INFO);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

//    public int getVolume() {
//        return volume;
//    }
//
//    public void setVolume(int volume) {
//        this.volume = volume;
//    }

    public String getGameBackImageName() {
        return gameBackImageName;
    }

    public void setGameBackImageName(String gameBackImageName) {
        this.gameBackImageName = gameBackImageName;
        try {
            MyLogger.writeLog(this.getUsername() + "-" + this.getStringID(),  this.getUsername() + " select " + gameBackImageName + " for background.", Level.INFO);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public InfoPassiveType getInfoPassive() {
        return infoPassive;
    }

    public void setInfoPassive(InfoPassiveType infoPassive) {
        this.infoPassive = infoPassive;
    }

    public ArrayList<Integer> getChangedCardFinalI() {
        return changedCardFinalI;
    }

    public void setChangedCardFinalI(ArrayList<Integer> changedCardFinalI) {
        this.changedCardFinalI = changedCardFinalI;
    }

    public Weapon getActiveWeapon() {
        return activeWeapon;
    }

    public void setActiveWeapon(Weapon activeWeapon) {
        this.activeWeapon = activeWeapon;
    }
}

